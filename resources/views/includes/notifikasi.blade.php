@if (count($errors) > 0)
<div class="alert alert-danger alert-dismissible fade show mt-3" role="alert" style="margin-top: 100px">
    @foreach ($errors->all() as $error)
        {{ $error }}
        <br />
    @endforeach
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="close"></button>
</div>

@endif
@if (Session::has('success'))
    <div class="alert alert-info alert-dismissible fade show" role="alert" style="margin-top: 100px">
        {{ Session::get('success') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif
@if (Session::has('error'))
    <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert" style="margin-top: 100px">
        {{ Session::get('error') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif
