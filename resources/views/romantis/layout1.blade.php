<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Sukamiskin | Romantis</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{asset('vendor/aos/aos.css')}}" rel="stylesheet">
  <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
  <link href="{{asset('vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendor/remixicon/remixicon.css')}}" rel="stylesheet">
  <link href="{{asset('vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">


    <link rel="stylesheet" type="text/css" href="{{ asset('css/trix.css') }}">
    <script type="text/javascript" src="{{ asset('js/trix.js') }}"></script>

    <style>
    trix-toolbar [data-trix-button-group="file-tools"] {
        display: none;
    }

    .instagram-media {
            background:#FFF; border:0; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 1px 0 rgba(0,0,0,0.15); padding:0;
        }
</style>
  <!-- Template Main CSS File -->
  <link href="{{asset('css/romantis/style.css')}}" rel="stylesheet">
  <link href="{{asset('css/styles.css')}}" rel="stylesheet" />


  <!-- =======================================================
  * Template Name: Arsha
  * Updated: Jul 05 2023 with Bootstrap v5.3.0
  * Template URL: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body class="d-flex flex-column h-100">

  <header id="header" class="fixed-top header-inner-pages">
  <div class="container d-flex align-items-center" style="position: relative">

    <h1 class="logo me-auto"><a href="{{route('romantis')}}" style="text-decoration:none">Romantis</a></h1>
    <!-- Uncomment below if you prefer to use an image logo -->
    <a href="{{route('romantis')}}" class="logo me-auto"><img src="{{asset('img/romantis/logo.png')}}" alt="" class="img-fluid"></a>

    <nav id="navbar" class="navbar">
      <ul>
        <li><a class="nav-link scrollto active" href="{{route('romantis')}}#hero">Beranda</a></li>
        <li><a class="nav-link scrollto" href="{{route('romantis.profil.kelurahan')}}">Profil</a></li>
        {{-- <li class="dropdown"><a href="#" style="text-decoration:none"><span>Profil</span> <i class="bi bi-chevron-down"></i></a>
          <ul>
            <li><a href="#" style="text-decoration:none">Struktur Pemerintahan</a></li>
            <li class="dropdown"><a href="#" style="text-decoration:none"><span>Monografi</span> <i class="bi bi-chevron-right"></i></a>
              <ul>
                <li><a href="#" style="text-decoration:none">Geografis</a></li>
                <li><a href="#" style="text-decoration:none">Kependudukan</a></li>
                <li><a href="#" style="text-decoration:none">Peta Wilayah</a></li>
              </ul>
            </li>
            <li class="dropdown"><a href="#" style="text-decoration:none"><span>Sarana Prasarana</span> <i class="bi bi-chevron-right"></i></a>
              <ul>
                <li><a href="#" style="text-decoration:none">Kesehatan</a></li>
                <li><a href="#" style="text-decoration:none">Pendidikan</a></li>
                <li><a href="#" style="text-decoration:none">Keagamaan</a></li>
                <li><a href="#" style="text-decoration:none">Perekonomian</a></li>
              </ul>
            </li>
            <li><a href="#" style="text-decoration:none">Sejarah</a></li>
            <li><a href="#" style="text-decoration:none">Prestasi</a></li>
          </ul>
        </li> --}}
        <li><a class="nav-link scrollto" href="{{route('romantis')}}#services">Layanan</a></li>
        <li><a class="nav-link scrollto" href="{{route('romantis')}}#portfolio">Giat</a></li>
        <li><a class="nav-link   scrollto" href="{{route('romantis')}}#why-us">Pemberdayaan</a></li>
        <li><a class="getstarted scrollto" href="{{route('romantis')}}#contact" style="text-decoration:none">Hubungi Kami</a></li>
      </ul>
      <i class="bi bi-list mobile-nav-toggle"></i>
    </nav><!-- .navbar -->

  </div>
</header><!-- End Header -->

  <main id="main">
    @include('includes.notifikasi')
    @yield('content')

  </main>
  <!-- End #main -->

  <!-- ======= Footer ======= -->
    {{-- <footer id="footer"> --}}
        <footer class="py-2 mt-auto" id="footer">
            <div class="container footer-bottom clearfix">
                <div class="copyright">
                     &copy; Copyright <strong><span>kel. sukamiskin</span></strong>. 2023
                </div>
                <div class="credits">
                    <!-- All the links in the footer should remain intact. -->
                    <!-- You can delete the links only if you purchased the pro version. -->
                    <!-- Licensing information: https://bootstrapmade.com/license/ -->
                    <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/ -->
                    Designed by <a href="#">dummieling@gmail.com</a>
                </div>
            </div>
        </footer>
        <!-- End Footer -->

    <div id="preloader"></div>
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous">
    </script> <script src="{{ asset('js/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/scripts.js') }}"></script>
    <script src="{{asset('vendor/aos/aos.js')}}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('vendor/glightbox/js/glightbox.min.js')}}"></script>
    <script src="{{asset('vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('vendor/swiper/swiper-bundle.min.js')}}"></script>
    <script src="{{asset('vendor/waypoints/noframework.waypoints.js')}}"></script>
    <script src="{{asset('vendor/php-email-form/validate.js')}}"></script>
    @yield('scripts')

    <!-- Template Main JS File -->
    <script src="{{asset('js/romantis/main.js')}}"></script>

</body>

</html>
