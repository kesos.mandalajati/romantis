

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Surat Keterangan</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <style type="text/css">
        @page {
            size: f4;
            /* size: 33.02cm 21cm; */
             margin: 10mm 25mm 20mm 25mm;
             font-family: 'Times New Roman', Times, serif;
        }

        #judul {
            text-align: center;
        }

        body {
            width: 100%;
            height: 100%;
            margin: 0%;
            font: 15pt "Tahoma";
            line-height: 25px;
            text-align: justify;
            font-family: 'Times New Roman', Times, serif;
        }

        @media print {

            .export {
                display: none;
                font-family: 'Times New Roman', Times, serif;
            }

        }

    </style>
</head>

<body>
    <table align="center" width="800px">
        <table>
            <tr>
                <td colspan="2" width="700px" colspan="2" style="text-transform: uppercase; text_align: start; font-size: 12px; line-height:15px">
                    LAMPIRAN I<br> KEPUTUSAN DIREKTUR JENDRAL BIMBINGAN MASYARAKAT ISLAM<br>NOMOR 713 TAHUN 2018<br>TENTANG<br>PENETAPAN FORMULIR DAN LAPORAN PENCATATAN PERKAWINAN ATAU RUJUK
                </td>
                <td width="100px" style="text-transform: uppercase; font-size: 12px; text-align: end; line-height:15px">
                    <br><br><br><br>Model N1
                </td>
            </tr>

            <tr>
                <td colspan="3" height="20"></td>
            </tr>
            <tr>
                <td colspan="3"><center>FORMULIR SURAT PENGANTAR PERKAWINAN</center></td>
            </tr>
            <tr >
                <td width="100px">KANTOR DESA/KELURAHAN</td>
                <td width="10px">:</td>
                <td width="690px" style="text-transform: uppercase; text_align: start; line-height:15px">{{ $suket->relasiSuketKeKelurahan->nama_kelurahan }}</td>
            </tr>
            <tr>
                <td>KECAMATAN</td>
                <td>:</td>
                <td style="text-transform: uppercase; text_align: start; line-height:15px">{{ $suket->relasiSuketKeKecamatan->nama_kecamatan }}</td>
            </tr>
            <tr>
                <td>KABUPATEN/KOTA</td>
                <td>:</td>
                <td style="text-transform: uppercase; text_align: start; line-height:15px">{{ $suket->relasiSuketKeKotakab->nama_kota_kab }}</td>
            </tr>
            <tr>
                <td colspan="3" height="20"></td>
            </tr>
            <tr>
                <td colspan="3">
                    <font size="4" style="text-transform: uppercase;"><center>
                        <font size="5" style="text-transform: uppercase;"><u><strong>{{ $suket->jenis_suket }}</strong></u></font><br>
                    Nomor : {{ $suket->no_reg_kel }}</center>
                    </font><br>
                </td>
            </tr>
        </table>
    </table>
    <table align="center" width="100%; padding-top: 20px">
        <tr style="text-indent: 50px">
            <td colspan="3" height="20"></td>
        </tr>
        <tr style="text-align: start">
            <td colspan="3">Yang bertanda tangan di bawah ini, menjelaskan dengan sesungguhnya bahwa :</td>
        </tr>
    </table>
    <table style="line-height: 5px; width: 100%">
        <table>
            <tr>
                <td width="400px" style="vertical-align: top; text-indent: 50px">1. Nama</td>
                <td style="vertical-align: top" width="10px">:</td>
                <td style="text-transform: uppercase; vertical-align: top" width="390">{{ $suket->relasiSuketKeWarga->nama }} <span style="{{ $suket->relasiSuketKeWarga->status_kematian != null ? 'color:red' : ''  }}">{{ $suket->relasiSuketKeWarga->status_kematian != null ? '(Alm)' : '' }}</span></td>
            </tr>
            <tr>
                <td width="400px" style="vertical-align: top; text-indent: 50px">2. NIK</td>
                <td style="vertical-align: top" width="10px">:</td>
                <td style="text-transform: uppercase; vertical-align: top" width="390">{{ $suket->relasiSuketKeWarga->nik }}</td>
            </tr>

            <tr>
                <td width="400px" style="vertical-align: top; text-indent: 50px">3. Agama</td>
                <td style="vertical-align: top" width="10px">:</td>
                <td style="text-transform: uppercase; vertical-align: top" width="390">{{ $suket->relasiSuketKeWarga->relasiWargaKeAgama->nama_agama }}</td>
            </tr>
            <tr>
                <td width="400px" style="vertical-align: top; text-indent: 50px">4. Jenis Kelamin</td>
                <td style="vertical-align: top" width="10px">:</td>
                <td style="text-transform: uppercase; vertical-align: top" width="390">{{ $suket->relasiSuketKeWarga->jenis_kelamin }}</td>
            </tr>
            <tr>
                <td width="400px" style="vertical-align: top; text-indent: 50px">5. Tempat Dan Tanggal Lahir</td>
                <td style="vertical-align: top" width="10px">:</td>
                <td style="text-transform: uppercase; vertical-align: top" width="390">{{ $suket->relasiSuketKeWarga->tempat_lahir }}, {{ date('d-m-Y', strtotime($suket->relasiSuketKeWarga->tanggal_lahir)) }}</td>
            </tr>
            <tr>
                <td width="400px" style="vertical-align: top; text-indent: 50px">6. Kewarganegaraan</td>
                <td style="vertical-align: top" width="10px">:</td>
                <td style="text-transform: uppercase; vertical-align: top" width="390">{{ $suket->relasiSuketKeWarga->kewarganegaraan }}</td>
            </tr>
            <tr>
                <td width="400px" style="vertical-align: top; text-indent: 50px">7. Pekerjaan</td>
                <td style="vertical-align: top" width="10px">:</td>
                <td style="text-transform: uppercase; vertical-align: top" width="390">{{ $suket->relasiSuketKeWarga->relasiWargaKePekerjaan->nama_pekerjaan }}</td>
            </tr>
            <tr>
                <td width="400px" style="vertical-align: top; text-indent: 50px">8. Alamat</td>
                <td style="vertical-align: top; width: 10px">:</td>
                <td style="text-transform: uppercase; vertical-align: top; text-align: justify" width="800">{{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeAlamat->nama_alamat }} RT. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeErte->nama_erte }} RW. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeErwe->nama_erwe }} Kel. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeKelurahan->nama_kelurahan }} Kec. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeKecamatan->nama_kecamatan }} {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeKotakab->nama_kota_kab }}</td>
            </tr>
            <tr>
                <td width="400px" style="vertical-align: top; text-indent: 50px">9.  Status Perkawinan</td>
                <td style="vertical-align: top" width="10px">:</td>
                <td style="text-transform: uppercase; vertical-align: top" width="390">{{ $suket->relasiSuketKeWarga->status_perkawinan }}</td>
            </tr>
            <tr>
                <td style="vertical-align: top; text-indent: 80px">a. Laki-Laki</td>
                <td style="vertical-align: top;" width="10px">:</td>
                <td style="vertical-align: top;" width="10px">
                    @if($suket->relasiSuketKeWarga->jenis_kelamin == "LAKI-LAKI")
                        @if($suket->relasiSuketKeWarga->status_perkawinan == "BELUM KAWIN" )
                            JEJAKA
                        @elseif($suket->relasiSuketKeWarga->status_perkawinan == "CERAI HIDUP" )
                            DUDA
                        @elseif($suket->relasiSuketKeWarga->status_perkawinan == "CERAI MATI" )
                            DUDA
                        @endif
                    {{-- @else
                        @if($suket->status_perkawinan_pasangan == "BELUM KAWIN" )
                            JEJAKA
                        @elseif($suket->relasiSuketKeWarga->status_perkawinan == "CERAI HIDUP" )
                            DUDA
                        @elseif($suket->relasiSuketKeWarga->status_perkawinan == "CERAI MATI" )
                            DUDA
                        @endif --}}
                    @endif
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top; text-indent: 80px">b. Perempuan</td>
                <td style="vertical-align: top;" width="10px">:</td>
                <td style="vertical-align: top;" width="10px">
                    @if($suket->relasiSuketKeWarga->jenis_kelamin == "PEREMPUAN")
                        @if($suket->relasiSuketKeWarga->status_perkawinan == "BELUM KAWIN" )
                            PERAWAN
                        @elseif($suket->relasiSuketKeWarga->status_perkawinan == "CERAI HIDUP" )
                            JANDA
                        @elseif($suket->relasiSuketKeWarga->status_perkawinan == "CERAI MATI" )
                            JANDA
                        @endif
                    {{-- @else
                        @if($suket->status_perkawinan_pasangan == "BELUM KAWIN" )
                            PERAWAN
                        @elseif($suket->relasiSuketKeWarga->status_perkawinan == "CERAI HIDUP" )
                            JANDA
                        @elseif($suket->relasiSuketKeWarga->status_perkawinan == "CERAI MATI" )
                            JANDA
                        @endif --}}
                    @endif
                </td>
            </tr>
            <tr>
                <td width="400px" style="vertical-align: top; text-indent: 50px">10.  Nama Istri/Suami terdahulu</td>
                <td style="vertical-align: top" width="10px">:</td>
                <td style="text-transform: uppercase; vertical-align: top" width="390">{{ $suket->nama_pasangan }} </span></td>
            </tr>

            <tr>
                <td colspan="3">
                    adalah benar anak dari perkawinan seorang laki-laki dengan identitas sebagai berikut :
                </td>
            </tr>
            <tr>
                <td width="400px" style="vertical-align: top; text-indent: 50px">Nama Lengkap dan Alias</td>
                <td style="vertical-align: top;" width="10px">:</td>
                <td style="text-transform: uppercase; vertical-align: top" width="390">{{ $suket->nama_ayah }} </span></td>
            </tr>
            <tr>
                <td width="400px" style="vertical-align: top; text-indent: 50px">NIK</td>
                <td style="vertical-align: top;" width="10px">:</td>
                <td style="text-transform: uppercase; vertical-align: top" width="390">{{ $suket->nik_ayah }}</td>
            </tr>
            <tr>
                <td width="400px" style="vertical-align: top; text-indent: 50px">Tempat Dan Tanggal Lahir</td>
                <td style="vertical-align:" width="10px">:</td>
                <td style="text-transform: uppercase; vertical-align: top" width="390">{{ $suket->tempat_lahir_ayah }}, {{ date('d-m-Y', strtotime($suket->ttl_ayah)) }}</td>
            </tr>
            <tr>
                <td style="vertical-align: top; text-indent: 50px;">Pekerjaan</td>
                <td style="vertical-align: top; width="10px"">:</td>
                <td style="text-transform: uppercase; vertical-align: top" width="390">{{ $suket->pekerjaan_ayah }}</td>
            </tr>
            <tr>
                <td style="vertical-align: top; text-indent: 50px;">Agama</td>
                <td style="vertical-align: top; width="10px"">:</td>
                <td style="text-transform: uppercase; vertical-align: top" width="390">{{ $suket->agama_ayah }}</td>
            </tr>
            <tr>
                <td style="vertical-align: top; text-indent: 50px;">Kewarganegaraan</td>
                <td style="vertical-align: top; width="10px"">:</td>
                <td style="text-transform: uppercase; vertical-align: top" width="390">{{ $suket->kewarganegaraan_ayah }}</td>
            </tr>
            <tr>
                <td width="400px" style="vertical-align: top; text-indent: 50px">Alamat</td>
                <td style="vertical-align: top;" width="10px">:</td>
                <td style="text-transform: uppercase; vertical-align: top" width="390">{{ $suket->alamat_ayah }}</td>
            </tr>
            <tr>
                <td colspan="3">
                    dengan seorang perempuan, dengan identitas sebagai berikut :
                </td>
            </tr>

            <tr>
                <td width="400px" style="vertical-align: top; text-indent: 50px">Nama</td>
                <td style="vertical-align: top;">:</td>
                <td style="text-transform: uppercase; vertical-align: top" width="390">{{ $suket->nama_ibu }} </span></td>
            </tr>
            <tr>
                <td width="400px" style="vertical-align: top; text-indent: 50px">NIK</td>
                <td style="vertical-align: top;">:</td>
                <td style="text-transform: uppercase; vertical-align: top" width="390">{{ $suket->nik_ibu }}</td>
            </tr>
            <tr>
                <td width="400px" style="vertical-align: top; text-indent: 50px">Tempat Dan Tanggal Lahir</td>
                <td style="vertical-align: top;">:</td>
                <td style="text-transform: uppercase; vertical-align: top" width="390">{{ $suket->tempat_lahir_ibu }}, {{ date('d-m-Y', strtotime($suket->ttl_ibu)) }}</td>
            </tr>
            <tr>
                <td style="vertical-align: top; text-indent: 50px;">Pekerjaan</td>
                <td style="vertical-align: top;">:</td>
                <td style="text-transform: uppercase; vertical-align: top" width="390">{{ $suket->pekerjaan_ibu }}</td>
            </tr>
            <tr>
                <td style="vertical-align: top; text-indent: 50px;">Agama</td>
                <td style="vertical-align: top; width="10px"">:</td>
                <td style="text-transform: uppercase; vertical-align: top" width="390">{{ $suket->agama_ibu }}</td>
            </tr>
            <tr>
                <td style="vertical-align: top; text-indent: 50px;">Kewarganegaraan</td>
                <td style="vertical-align: top; width="10px"">:</td>
                <td style="text-transform: uppercase; vertical-align: top" width="390">{{ $suket->kewarganegaraan_ibu }}</td>
            </tr>
            <tr>
                <td width="400px" style="vertical-align: top; text-indent: 50px">Alamat</td>
                <td style="vertical-align: top;">:</td>
                <td style="text-transform: uppercase; vertical-align: top" width="390">{{ $suket->alamat_ibu }}</td>
            </tr>
        </table>
    </table>
    <table align="center" width="100%; padding-top: 20px">
        <tr>
            <td>
                Demikian Surat Pengantar ini dibuat dengan mengingat sumpah jabatan dan untuk dipergunakan sebagaimana mestinya.
            </td>
        </tr>
        <tr>
            <td height="30"></td>
        </tr>
    </table>


    <table style="width: 100%">
        <tr><center>
            <td width="50%" height="4px" style="vertical-align: top"><center>
                </center>
            </td>
            <td width="50%" height="4px" style="vertical-align: top"><center>
                {{-- {{ $suket->relasiSuketKeKotakab->nama_kota_kab }}, {{ date('d-m-Y', strtotime($suket->tgl_reg_kel)) }}</center> --}}
            </td></center>
        </tr>
        <tr><center>
            <td width="50%" height="4px" style="vertical-align: top"><center>
                </center>
            </td>
            <td width="50%" height="4px" style="vertical-align: top"><center>
                {{-- @if($suket->relasiSuketKePejabat->jabatan != "Lurah")
                    a.n. LURAH <span style="text-transform: uppercase">{{ $suket->relasiSuketKeKelurahan->nama_kelurahan }}
                        <br>
                <span style="text-transform: uppercase">{{ $suket->relasiSuketKePejabat->jabatan }}</span>
                @elseif($suket->relasiSuketKePejabat->jabatan == "Lurah")
                    LURAH <span style="text-transform: uppercase"> {{ $suket->relasiSuketKeKelurahan->nama_kelurahan }}

                @endif --}}
                </center>
            </td></center>
        </tr>
        <tr>
            <td height="90"></td>
        </tr>
        <tr>
            <td width="50%"><center></center>
            </td>
            <td width="50%"><center>
                {{-- <span style="text-transform: uppercase"><strong>{{ $suket->relasiSuketKePejabat->name }}</strong></span></center> --}}
            </td>
        </tr>
        <tr>
            <td></td>
            <td width="50%" line-height="1"><center><strong>
                {{-- NIP. {{ $suket->relasiSuketKePejabat->nip }}</strong></center> --}}
            </td>
        </tr>
    </table>
</body>

</html>
