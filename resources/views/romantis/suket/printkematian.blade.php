

<!DOCTYPE html>
<html lang="en">

<head>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Surat Keterangan</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
            integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
        <style type="text/css">
            @page {
                size: f4;
                /* size: 33.02cm 21cm; */
                margin: 10mm 25mm 20mm 25mm;
                font-family: 'Times New Roman', Times, serif;
            }

            #judul {
                text-align: center;
                font-family: 'Times New Roman', Times, serif;
            }

            body {
                width: 100%;
                height: 100%;
                margin: 0%;
                font: 16pt "Tahoma";
                line-height: 35px;
                text-align: justify;
                font-family: 'Times New Roman', Times, serif;
            }

            @media print {

                .export {
                    display: none;
                    font-family: 'Times New Roman', Times, serif;
                }

            }

        </style>

</head>

<body>
    <table align="center" width="100%">
        <tr>
            <td><img src="{{ asset('img/logo bandung.png') }}" height="150" width="150"></td>
            <td><center>
                {{-- <font size="4" style="text-transform: uppercase; font-size: 25px"><strong>PEMERINTAH {{ $suket->relasiSuketKeKotakab->nama_kota_kab }}</strong></font><br>
                <font size="5" style="text-transform: uppercase;font-size: 35px"><strong>KECAMATAN {{ $suket->relasiSuketKeKecamatan->nama_kecamatan }}</strong></font><br>
                <font size="6" style="text-transform: uppercase;font-size: 45px"><strong>KELURAHAN {{ $suket->relasiSuketKeKelurahan->nama_kelurahan }}</strong></font><br>
                Alamat : {{ Auth::user()->alamat_sekretariat }} Telp.{{ Auth::user()->no_tlp }} --}}
            </center>
            </td>
        </tr>
        <tr>
            <td colspan="2"><hr style="border: 0; border-top: 4px double black"></td>
        </tr>
        <tr>
            <td colspan="2"><center>
                <font size="5"><strong>SURAT KETERANGAN</strong></font><br>
                Nomor : {{ $suket->no_reg_kel }}
            </center></td>
        </tr>
        <tr>
            <td colspan="2" height="50"></td>
        </tr>
        <tr style="text-indent: 50px; padding-top: 20px; text-align: justify">
            <td colspan="2">Telah menghadap kepada Lurah <span style="text-transform: capitalize"> {{ $suket->relasiSuketKeKelurahan->nama_kelurahan }} Kecamatan {{ $suket->relasiSuketKeKecamatan->nama_kecamatan }} {{ $suket->relasiSuketKeKotakab->nama_kota_kab }}</span>, seorang penduduk :</td>
        </tr>
    </table>
    <table style="line-height: 5px; width: 100%">
        <table>
            <tr>
                <td width="35%" style="text-indent: 50px">Nama</td>
                <td style="vertical-align: top; width: 30px">:</td>
                <td style="text-transform: uppercase; vertical-align: top">{{ $suket->relasiSuketKeWarga->nama }}</td>
            </tr>
            <tr>
                <td style="vertical-align: top; text-indent: 50px">NIK</td>
                <td style="vertical-align: top; width: 30px">:</td>
                <td style="text-transform: uppercase; vertical-align: top">{{ $suket->relasiSuketKeWarga->nik }}</td>
            </tr>
            <tr>
                <td style="vertical-align: top; text-indent: 50px">No KK</td>
                <td style="vertical-align: top; width: 30px">:</td>
                <td style="text-transform: uppercase; vertical-align: top">{{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->no_kk }}</td>
            </tr>
            <tr>
                <td style="vertical-align: top; text-indent: 50px">Jenis Kelamin</td>
                <td style="vertical-align: top; width: 30px">:</td>
                <td style="text-transform: uppercase; vertical-align: top">{{ $suket->relasiSuketKeWarga->jenis_kelamin }}</td>
            </tr>
            <tr>
                <td style="vertical-align: top; text-indent: 50px">Tempat Dan Tanggal Lahir</td>
                <td style="vertical-align: top; width: 30px">:</td>
                <td style="text-transform: uppercase; vertical-align: top">{{ $suket->relasiSuketKeWarga->tempat_lahir }}, {{ date('d-m-Y', strtotime($suket->relasiSuketKeWarga->tanggal_lahir)) }}</td>
            </tr>
            <tr>
                <td style="vertical-align: top; text-indent: 50px">Kewarganegaraan</td>
                <td style="vertical-align: top; width: 30px">:</td>
                <td style="text-transform: uppercase; vertical-align: top">{{ $suket->relasiSuketKeWarga->kewarganegaraan }}</td>
            </tr>
            <tr>
                <td style="vertical-align: top; text-indent: 50px">Status Perkawinan</td>
                <td style="vertical-align: top; width: 30px">:</td>
                <td style="text-transform: uppercase; vertical-align: top">{{ $suket->relasiSuketKeWarga->status_perkawinan }}</td>
            </tr>
            <tr>
                <td style="vertical-align: top; text-indent: 50px">Pekerjaan</td>
                <td style="vertical-align: top; width: 30px">:</td>
                <td style="text-transform: uppercase; vertical-align: top">{{ $suket->relasiSuketKeWarga->relasiWargaKepekerjaan->nama_pekerjaan }}</td>
            </tr>
            <tr>
                <td style="vertical-align: top; text-indent: 50px">Agama</td>
                <td style="vertical-align: top; width: 30px">:</td>
                <td style="text-transform: uppercase; vertical-align: top">{{ $suket->relasiSuketKeWarga->relasiWargaKeAgama->nama_agama }}</td>
            </tr>
            <tr>
                <td style="vertical-align: top; text-indent: 50px">Alamat</td>
                <td style="vertical-align: top; width: 30px">:</td>
                <td style="text-transform: uppercase; vertical-align: top">{{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeAlamat->nama_alamat }} RT. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeErte->nama_erte }} RW. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeErwe->nama_erwe }} Kel. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeKelurahan->nama_kelurahan }} Kec. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeKecamatan->nama_kecamatan }} {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeKotakab->nama_kota_kab }}</td>
            </tr>
            <tr>
                <td style="vertical-align: top; text-indent: 50px">Nama Pemohon</td>
                <td style="vertical-align: top; width: 30px">:</td>
                <td style="text-transform: uppercase; vertical-align: top">{{ $suket->pemohon }}</td>
            </tr>
            <tr>
                <td style="vertical-align: top; text-indent: 50px">Hubungan Dengan Almarhum</td>
                <td style="vertical-align: top; width: 30px">:</td>
                <td style="text-transform: uppercase; vertical-align: top">{{ $suket->hubungan }}</td>
            </tr>

        </table>
        <table align="center" width="100%; padding-top: 20px">
            <tr style="text-indent: 50px">
                <td>
                    Berdasarkan Surat Pengantar dari Ketua RT. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeErte->nama_erte }} RW. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeErwe->nama_erwe }} Nomor @if($suket->no_reg_rt){{ $suket->no_reg_rt }}@else {{ $suket->no_reg_rw }}@endif Tanggal @if($suket->tgl_reg_rt){{ date('d-m-Y', strtotime($suket->tgl_reg_rt)) }} @else {{ date('d-m-Y', strtotime($suket->tgl_reg_rw)) }}@endif dan pengakuan dari Pemohon, bahwa penduduk yang bernama {{ $suket->relasiSuketKeWarga->nama }} benar telah meninggal dunia pada tanggal {{ date('d-m-Y', strtotime($suket->relasiSuketKeWarga->status_kematian)) }}.
                </td>
            </tr>
            <tr style="text-indent: 50px">
                <td>
                    Surat Keterangan ini diperlukan untuk melengkapi persyaratan <strong>{{  $suket->maksud_suket  }}</strong>.
                </td>
            </tr>
            <tr style="text-indent: 50px">
                <td>
                    Surat Keterangan ini berlaku untuk <strong>satu kali pengurusan</strong>.
                </td>
            </tr>
             <tr style="text-indent: 50px">
                <td>
                    Surat Keterangan dinyatakan tidak berlaku apabila terjadi pelanggaran Peraturan Perundang-undangan dan Peraturan Daerah Kota Bandung serta apabila terdapat kekeliruan/kesalahan dalam pembuatannya, Pemohon bersedia mempertanggungjawabkan secara hukum tanpa melibatkan pihak manapun.
                </td>
            </tr>
             <tr style="text-indent: 50px">
                <td>
                    Demikian Surat Keterangan ini dibuat untuk dapat dipergunakan sebagaimana mestinya.
                </td>
            </tr>
            <tr>
                <td height="30"></td>
            </tr>
        </table>
        <table style="width: 100%">
            <tr><center>
                <td width="50%" height="10px" style="vertical-align: top"><center>
                    </center>
                </td>
                <td width="50%" height="10px" style="vertical-align: top"><center>
                    {{ $suket->relasiSuketKeKotakab->nama_kota_kab }}, {{ date('d-m-Y', strtotime($suket->tgl_reg_kel)) }}</center>
                </td></center>
            </tr>
            <tr><center>
                <td width="50%" height="10px" style="vertical-align: top"><center>
                    PEMOHON,</center>
                </td>
                <td width="50%" height="10px" style="vertical-align: top"><center>
                    {{-- @if($suket->relasiSuketKePejabat->jabatan != "Lurah")
                        a.n. LURAH <span style="text-transform: uppercase">{{ $suket->relasiSuketKeKelurahan->nama_kelurahan }}<br>
                        {{ $suket->relasiSuketKePejabat->jabatan }}
                    @else
                        LURAH <span style="text-transform: uppercase">{{ $suket->relasiSuketKeKelurahan->nama_kelurahan }}
                    @endif --}}
                </center>
                </td></center>
            </tr>
            <tr>
                <td height="90"></td>
            </tr>
            <tr>
                <td width="50%"><center>
                    <span style="text-transform: uppercase"><strong> {{  $suket->pemohon }} </strong></span></center>
                </td>
                <td width="50%"><center>
                    {{-- <span style="text-transform: uppercase"><strong>{{ $suket->relasiSuketKePejabat->name }}</strong></span></center> --}}
                </td>
            </tr>
            <tr>
                <td></td>
                <td width="50%" line-height="1"><center><strong>
                    {{-- NIP. {{ $suket->relasiSuketKePejabat->nip }}</strong></center> --}}
                </td>
            </tr>
        </table>
    </table>
</body>

</html>
