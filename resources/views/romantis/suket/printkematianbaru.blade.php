

<!DOCTYPE html>
<html lang="en">

<head>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Surat Keterangan</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
            integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
        <style type="text/css">
            @page {
                size: 33cm 21.50cm;
                /* margin: 10mm 10mm 10mm 10mm; */
                font-family: 'Times New Roman', Times, serif;
            }

            #judul {
                text-align: center;
                font-family: 'Times New Roman', Times, serif;
            }

            body {
                width: 1248px;
                height: 750px;
                margin: 0%;
                font: 12pt "Tahoma";
                text-align: justify;
                font-family: 'Times New Roman', Times, serif;
            }

            @media print {

                .export {
                    display: none;
                    font-family: 'Times New Roman', Times, serif;
                }

            }

        </style>

</head>

<body style="text-transform: uppercase; font-size: 14px">
    <table align="left" width="400px" height="750px" cellpadding="7" style="border: 4px double black; ">
        <tr><center>
            <td><center><img src="{{ asset('img/logo bandung.png') }}" height="60" width="50"></center></td>
            <td colspan="2" height="60px"><center>
                <strong><u >{{ $suket->jenis_suket }}</u></strong><br>
                {{-- Nomor : {{ $suket->no_reg_kel }} --}}
            </center>
            </td></center>
        </tr>
        <tr>
            <td colspan="3" height="70px"><center>
                <strong style="text-transform: uppercase;">KELURAHAN {{ $suket->relasiSuketKeKelurahan->nama_kelurahan }}<br> KECAMATAN {{ $suket->relasiSuketKeKecamatan->nama_kecamatan }} <br> {{ $suket->relasiSuketKeKotakab->nama_kota_kab }}</strong>
            </center></td>
        </tr>
        <tr >
            <td colspan="3" style="text-indent: 20px">Yang bertanda tangan di bawah ini menerangkan bahwa:</td>
        </tr>
        <tr>
            <td style="vertical-align: top;" width="37%" height="130px">
                Nama <br>
                NIK <br>
                Kelamin<br>
                Lahir<br>
                No. KK <br>
                Alamat
            </td>
            <td style="vertical-align: top;" width="5%">:<br>:<br>:<br>:<br>:<br>:</td>
            <td style="text-transform: uppercase; vertical-align: top" width="58%">
                {{  $suket->relasiSuketKeWarga->nama }}<br>
                {{  $suket->relasiSuketKeWarga->nik }}<br>
                {{  $suket->relasiSuketKeWarga->jenis_kelamin }}<br>
                {{  $suket->relasiSuketKeWarga->tanggal_lahir }}<br>
                {{  $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->no_kk }}<br>
                {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeAlamat->nama_alamat }} RT. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeErte->nama_erte }} RW. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeErwe->nama_erwe }} Kel. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeKelurahan->nama_kelurahan }} Kec. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeKecamatan->nama_kecamatan }} {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeKotakab->nama_kota_kab }}
            </td>
        </tr>
        <tr >
            <td colspan="3" style="text-indent: 20px">Telah meninggal dunia, pada:</td>
        </tr>
        <tr>
            <td style="vertical-align: top;" width="37%" height="70px">
                {{-- Hari<br> --}}
                Tanggal<br>
                Di<br>
                Sebab Kematian
            </td>
            <td style="vertical-align: top;" width="5%">:<br>:<br>:
            </td>
            <td style="text-transform: uppercase; vertical-align: top" width="58%">
                {{-- @if( date('l', strtotime($suket->relasiSuketKeWarga->status_kematian)) == "Sunday") Minggu @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->status_kematian)) == "Monday") Senin @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->status_kematian)) == "Tuesday") Selasa @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->status_kematian)) == "wednesday") Rabu @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->status_kematian)) == "Thrusday") Kamis @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->status_kematian)) == "Friday") Jumat @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->status_kematian)) == "Saturday") Sabtu @endif<br> --}}
                {{ date('d-m-Y', strtotime($suket->relasiSuketKeWarga->status_kematian)) }}<br>
                @if ($suket->tempat){{ $suket->tempat }}@else Rumah Kediaman @endif<br>
                {{  $suket->sebab }}
            </td>
        </tr>
        <tr >
            <td colspan="3" style="text-indent: 20px">Surat Keterangan ini dibuat atas dasar yang sebenarnya.</td>
        </tr>
        <tr >
            <td height="50px"></td>
            <td colspan="2"  align="end" style="vertical-align: top"><center>
                {{-- {{ $suket->relasiSuketKeKotakab->nama_kota_kab }}, {{ date('d-m-Y', strtotime($suket->tgl_reg_kel)) }}<br>
                @if($suket->relasiSuketKePejabat->jabatan != "Lurah")
                    a.n. LURAH <span style="text-transform: uppercase">{{ $suket->relasiSuketKeKelurahan->nama_kelurahan }}<br>
                    {{ $suket->relasiSuketKePejabat->jabatan }}
                @else
                    LURAH <span style="text-transform: uppercase">{{ $suket->relasiSuketKeKelurahan->nama_kelurahan }}
                @endif</center> --}}
            </td>
        </tr>
        <tr>
            <td colspan="3"  height="70"></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2"  align="end"><center><strong><span style="text-transform: uppercase">
                {{-- {{ $suket->relasiSuketKePejabat->name }}<br>
                NIP. {{ $suket->relasiSuketKePejabat->nip }}</strong></center> --}}
            </td>
        </tr>
    </table>

    <table align="left" width="20px" height="750px" cellpadding="7" style="border: 4px double black;">
    </table>

    <table align="left" width="400px" height="750px" cellpadding="7"  style="border: 4px double black; ">
        <tr><center>
            <td><center><img src="{{ asset('img/logo bandung.png') }}" height="60" width="50"></center></td>
            <td colspan="2" height="60px"><center>
                <strong><u >{{ $suket->jenis_suket }}</u></strong><br>
                {{-- Nomor : {{ $suket->no_reg_kel }} --}}
            </center>
            </td></center>
        </tr>
        <tr>
            <td colspan="3" height="70px"><center>
                <strong style="text-transform: uppercase;">KELURAHAN {{ $suket->relasiSuketKeKelurahan->nama_kelurahan }}<br> KECAMATAN {{ $suket->relasiSuketKeKecamatan->nama_kecamatan }} <br> {{ $suket->relasiSuketKeKotakab->nama_kota_kab }}</strong>
            </center></td>
        </tr>
        <tr >
            <td colspan="3" style="text-indent: 20px">Yang bertanda tangan di bawah ini menerangkan bahwa:</td>
        </tr>
        <tr>
            <td style="vertical-align: top;" width="37%" height="130px">
                Nama <br>
                NIK <br>
                Kelamin<br>
                Lahir<br>
                No. KK <br>
                Alamat
            </td>
            <td style="vertical-align: top;" width="5%">:<br>:<br>:<br>:<br>:<br>:</td>
            <td style="text-transform: uppercase; vertical-align: top" width="58%">
                {{  $suket->relasiSuketKeWarga->nama }}<br>
                {{  $suket->relasiSuketKeWarga->nik }}<br>
                {{  $suket->relasiSuketKeWarga->jenis_kelamin }}<br>
                {{  $suket->relasiSuketKeWarga->tanggal_lahir }}<br>
                {{  $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->no_kk }}<br>
                {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeAlamat->nama_alamat }} RT. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeErte->nama_erte }} RW. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeErwe->nama_erwe }} Kel. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeKelurahan->nama_kelurahan }} Kec. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeKecamatan->nama_kecamatan }} {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeKotakab->nama_kota_kab }}
            </td>
        </tr>
        <tr >
            <td colspan="3" style="text-indent: 20px">Telah meninggal dunia, pada:</td>
        </tr>
        <tr>
            <td style="vertical-align: top;" width="37%" height="70px">
                {{-- Hari<br> --}}
                Tanggal<br>
                Di<br>
                Sebab Kematian
            </td>
            <td style="vertical-align: top;" width="5%">:<br>:<br>:
            </td>
            <td style="text-transform: uppercase; vertical-align: top" width="58%">
                {{-- @if( date('l', strtotime($suket->relasiSuketKeWarga->status_kematian)) == "Sunday") Minggu @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->status_kematian)) == "Monday") Senin @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->status_kematian)) == "Tuesday") Selasa @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->status_kematian)) == "wednesday") Rabu @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->status_kematian)) == "Thrusday") Kamis @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->status_kematian)) == "Friday") Jumat @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->status_kematian)) == "Saturday") Sabtu @endif<br> --}}
                {{ date('d-m-Y', strtotime($suket->relasiSuketKeWarga->status_kematian)) }}<br>
                @if ($suket->tempat){{ $suket->tempat }}@else Rumah Kediaman @endif<br>
                {{  $suket->sebab }}
            </td>
        </tr>
        <tr >
            <td colspan="3" style="text-indent: 20px">Surat Keterangan ini dibuat atas dasar yang sebenarnya.</td>
        </tr>
        <tr >
            <td height="50px"></td>
            <td colspan="2"  align="end" style="vertical-align: top"><center>
                {{-- {{ $suket->relasiSuketKeKotakab->nama_kota_kab }}, {{ date('d-m-Y', strtotime($suket->tgl_reg_kel)) }}<br>
                @if($suket->relasiSuketKePejabat->jabatan != "Lurah")
                    a.n. LURAH <span style="text-transform: uppercase">{{ $suket->relasiSuketKeKelurahan->nama_kelurahan }}<br>
                    {{ $suket->relasiSuketKePejabat->jabatan }}
                @else
                    LURAH <span style="text-transform: uppercase">{{ $suket->relasiSuketKeKelurahan->nama_kelurahan }}
                @endif</center> --}}
            </td>
        </tr>
        <tr>
            <td colspan="3"  height="70"></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2"  align="end"><center><strong><span style="text-transform: uppercase">
                {{-- {{ $suket->relasiSuketKePejabat->name }}<br>
                NIP. {{ $suket->relasiSuketKePejabat->nip }}</strong></center> --}}
            </td>
        </tr>
    </table>

    <table align="left" width="20px" height="750px" cellpadding="7" style="border: 4px double black;">
    </table>

    <table align="left" width="400px" height="750px"  cellpadding="7" style="border: 4px double black; ">
        <tr><center>
            <td><center><img src="{{ asset('img/logo bandung.png') }}" height="60" width="50"></center></td>
            <td colspan="2" height="60px"><center>
                <strong><u >{{ $suket->jenis_suket }}</u></strong><br>
                {{-- Nomor : {{ $suket->no_reg_kel }} --}}
            </center>
            </td></center>
        </tr>
        <tr>
            <td colspan="3" height="70px"><center>
                <strong style="text-transform: uppercase;">KELURAHAN {{ $suket->relasiSuketKeKelurahan->nama_kelurahan }}<br> KECAMATAN {{ $suket->relasiSuketKeKecamatan->nama_kecamatan }} <br> {{ $suket->relasiSuketKeKotakab->nama_kota_kab }}</strong>
            </center></td>
        </tr>
        <tr >
            <td colspan="3" style="text-indent: 20px">Yang bertanda tangan di bawah ini menerangkan bahwa:</td>
        </tr>
        <tr>
            <td style="vertical-align: top;" width="37%" height="130px">
                Nama <br>
                NIK <br>
                Kelamin<br>
                Lahir<br>
                No. KK <br>
                Alamat
            </td>
            <td style="vertical-align: top;" width="5%">:<br>:<br>:<br>:<br>:<br>:</td>
            <td style="text-transform: uppercase; vertical-align: top" width="58%">
                {{  $suket->relasiSuketKeWarga->nama }}<br>
                {{  $suket->relasiSuketKeWarga->nik }}<br>
                {{  $suket->relasiSuketKeWarga->jenis_kelamin }}<br>
                {{  $suket->relasiSuketKeWarga->tanggal_lahir }}<br>
                {{  $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->no_kk }}<br>
                {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeAlamat->nama_alamat }} RT. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeErte->nama_erte }} RW. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeErwe->nama_erwe }} Kel. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeKelurahan->nama_kelurahan }} Kec. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeKecamatan->nama_kecamatan }} {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeKotakab->nama_kota_kab }}
            </td>
        </tr>
        <tr >
            <td colspan="3" style="text-indent: 20px">Telah meninggal dunia, pada:</td>
        </tr>
        <tr>
            <td style="vertical-align: top;" width="37%" height="70px">
                {{-- Hari<br> --}}
                Tanggal<br>
                Di<br>
                Sebab Kematian
            </td>
            <td style="vertical-align: top;" width="5%">:<br>:<br>:
            </td>
            <td style="text-transform: uppercase; vertical-align: top" width="58%">
                {{-- @if( date('l', strtotime($suket->relasiSuketKeWarga->status_kematian)) == "Sunday") Minggu @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->status_kematian)) == "Monday") Senin @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->status_kematian)) == "Tuesday") Selasa @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->status_kematian)) == "wednesday") Rabu @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->status_kematian)) == "Thrusday") Kamis @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->status_kematian)) == "Friday") Jumat @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->status_kematian)) == "Saturday") Sabtu @endif<br> --}}
                {{ date('d-m-Y', strtotime($suket->relasiSuketKeWarga->status_kematian)) }}<br>
                @if ($suket->tempat){{ $suket->tempat }}@else Rumah Kediaman @endif<br>
                {{  $suket->sebab }}
            </td>
        </tr>
        <tr >
            <td colspan="3" style="text-indent: 20px">Surat Keterangan ini dibuat atas dasar yang sebenarnya.</td>
        </tr>
        <tr >
            <td height="50px"></td>
            <td colspan="2"  align="end" style="vertical-align: top"><center>
                {{-- {{ $suket->relasiSuketKeKotakab->nama_kota_kab }}, {{ date('d-m-Y', strtotime($suket->tgl_reg_kel)) }}<br>
                @if($suket->relasiSuketKePejabat->jabatan != "Lurah")
                    a.n. LURAH <span style="text-transform: uppercase">{{ $suket->relasiSuketKeKelurahan->nama_kelurahan }}<br>
                    {{ $suket->relasiSuketKePejabat->jabatan }}
                @else
                    LURAH <span style="text-transform: uppercase">{{ $suket->relasiSuketKeKelurahan->nama_kelurahan }}
                @endif</center> --}}
            </td>
        </tr>
        <tr>
            <td colspan="3"  height="70"></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2"  align="end"><center><strong><span style="text-transform: uppercase">
                {{-- {{ $suket->relasiSuketKePejabat->name }}<br>
                NIP. {{ $suket->relasiSuketKePejabat->nip }}</strong></center> --}}
            </td>
        </tr>
    </table>

</body>

</html>
