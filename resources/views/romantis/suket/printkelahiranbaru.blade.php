

<!DOCTYPE html>
<html lang="en">

<head>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Surat Keterangan</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
            integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
        <style type="text/css">
            @page {
                size: 33cm 21.50cm;
                margin: 10mm 10mm 10mm 10mm;
                font-family: 'Times New Roman', Times, serif;
            }

            #judul {
                text-align: center;
                font-family: 'Times New Roman', Times, serif;
            }

            body {
                width: 1248px;
                height: 750px;
                margin: 0%;
                line-height: 3;
                font: 12pt "Tahoma";
                text-align: justify;
                font-family: 'Times New Roman', Times, serif;
            }

            @media print {

                .export {
                    display: none;
                    font-family: 'Times New Roman', Times, serif;
                }

            }
            tr {
                margin-bottom: 1px;
            }

        </style>

</head>

<body style="text-transform: uppercase; font-size: 14px">
    <table align="left" width="400px" height="750px"  cellpadding="7" style="border: 4px double black;">
        <tr><center>
            <td><center><img src="{{ asset('img/logo bandung.png') }}" height="60" width="50"></center></td>
            <td colspan="2"><center>
                <strong><u >{{ $suket->jenis_suket }}</u></strong><br>
                {{-- Nomor : {{ $suket->no_reg_kel }} --}}
            </center>
            </td></center>
        </tr>
        <tr>
            <td colspan="3"><center>
                <strong style="text-transform: uppercase; font-size: 16px">PEMERINTAH {{ $suket->relasiSuketKeKotakab->nama_kota_kab }}</strong>
            </center></td>
        </tr>
        <tr>
            <td colspan="3" height="10px"></td>
        </tr>
        <tr>
            <td colspan="3" style="text-indent: 20px">Yang bertanda tangan di bawah ini menerangkan bahwa pada :</td>
        </tr>
        <tr>
            <td style="vertical-align: top;" width="37%">Hari</td>
            <td style="vertical-align: top;" width="5%">:</td>
            <td style="text-transform: uppercase; vertical-align: top" width="58%">@if( date('l', strtotime($suket->relasiSuketKeWarga->tanggal_lahir)) == "Sunday") Minggu @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->tanggal_lahir)) == "Monday") Senin @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->tanggal_lahir)) == "Tuesday") Selasa @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->tanggal_lahir)) == "wednesday") Rabu @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->tanggal_lahir)) == "Thrusday") Kamis @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->tanggal_lahir)) == "Friday") Jumat @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->tanggal_lahir)) == "Saturday") Sabtu @endif @if ($suket->waktu_lahir) Pukul.{{ $suket->waktu_lahir}} WIB @endif</td>
        </tr>
        <tr>
            <td style="vertical-align: top;">Tanggal</td>
            <td style="vertical-align: top;">:</td>
            <td style="text-transform: uppercase; vertical-align: top">{{ date('d-m-Y', strtotime($suket->relasiSuketKeWarga->tanggal_lahir)) }}</td>
        </tr>
        <tr>
            <td style="vertical-align: top;">Di</td>
            <td style="vertical-align: top;">:</td>
            <td style="text-transform: uppercase; vertical-align: top">{{ $suket->relasiSuketKeWarga->tempat_lahir}}</td>
        </tr>

        <tr>
            <td colspan="3" style="text-indent: 20px">Telah lahir seorang anak {{ $suket->relasiSuketKeWarga->jenis_kelamin }} bernama :</td>
        </tr>

        <tr>
            <td colspan="3" height="20px" style="text-transform: capitalize; font-size: 16px"><center><strong>{{ $suket->relasiSuketKeWarga->nama }}</strong> <center></td>
        </tr>

        <tr>
            <td colspan="3" style="text-indent: 20px">Dari seorang ibu bernama :</td>
        </tr>
        <tr>
            <td colspan="3" style="text-transform: capitalize;"><center><strong>{{ $suket->nama_ibu }}</strong> <center></td>
        </tr>
        <tr>
            <td style="vertical-align: top;">Alamat</td>
            <td style="vertical-align: top;">:</td>
            <td style="text-transform: uppercase; vertical-align: top" width="70%">{{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeAlamat->nama_alamat }} RT. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeErte->nama_erte }} RW. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeErwe->nama_erwe }} Kel. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeKelurahan->nama_kelurahan }} Kec. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeKecamatan->nama_kecamatan }} {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeKotakab->nama_kota_kab }}</td>
        </tr>
        <tr>
            <td style="vertical-align: top;">Istri dari</td>
            <td style="vertical-align: top;">:</td>
            <td style="text-transform: uppercase; vertical-align: top"><strong>{{ $suket->nama_ayah }}</strong></td>
        </tr>
        <tr>
            <td style="vertical-align: top;">Tempat,Tgl Nikah</td>
            <td style="vertical-align: top;">:</td>
            <td style="text-transform: uppercase; vertical-align: top">{{ $suket->tempat_nikah}}, {{ date('d-m-Y', strtotime($suket->tgl_nikah)) }}</td>
        </tr>
        <tr>
            <td style="vertical-align: top;">Buku Nikah No</td>
            <td style="vertical-align: top;">:</td>
            <td style="text-transform: uppercase; vertical-align: top">{{ $suket->buku_nikah}}</td>
        </tr>
        <tr>
            <td colspan="3" style="text-indent: 20px">Surat Keterangan ini dibuat atas dasar yang sebenarnya.</td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2"  align="end" style="vertical-align: top"><center>
                {{-- {{ $suket->relasiSuketKeKotakab->nama_kota_kab }}, {{ date('d-m-Y', strtotime($suket->tgl_reg_kel)) }}<br>
                @if($suket->relasiSuketKePejabat->jabatan != "Lurah")
                    a.n. LURAH <span style="text-transform: uppercase">{{ $suket->relasiSuketKeKelurahan->nama_kelurahan }}<br>
                    {{ $suket->relasiSuketKePejabat->jabatan }}
                @else
                    LURAH <span style="text-transform: uppercase">{{ $suket->relasiSuketKeKelurahan->nama_kelurahan }}
                @endif</center> --}}
            </td>
        </tr>
        <tr>
            <td colspan="3"  height="70"></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2"  align="end"><center><strong><span style="text-transform: uppercase">
                {{-- {{ $suket->relasiSuketKePejabat->name }}<br>
                NIP. {{ $suket->relasiSuketKePejabat->nip }}</strong></center> --}}
            </td>
        </tr>
    </table>

    <table align="left" width="20px" height="750px" cellpadding="7" style="border: 4px double black;">
    </table>

    <table align="left" width="400px" height="750px" cellpadding="7" style="border: 4px double black;">
        <tr><center>
            <td><center><img src="{{ asset('img/logo bandung.png') }}" height="60" width="50"></center></td>
            <td colspan="2"><center>
                <strong><u >{{ $suket->jenis_suket }}</u></strong><br>
                {{-- Nomor : {{ $suket->no_reg_kel }} --}}
            </center>
            </td></center>
        </tr>
        <tr>
            <td colspan="3"><center>
                <strong style="text-transform: uppercase; font-size: 16px">PEMERINTAH {{ $suket->relasiSuketKeKotakab->nama_kota_kab }}</strong>
            </center></td>
        </tr>
        <tr>
            <td colspan="3" height="10px"></td>
        </tr>
        <tr>
            <td colspan="3" style="text-indent: 20px">Yang bertanda tangan di bawah ini menerangkan bahwa pada :</td>
        </tr>
        <tr>
            <td style="vertical-align: top;" width="37%">Hari</td>
            <td style="vertical-align: top;" width="5%">:</td>
            <td style="text-transform: uppercase; vertical-align: top" width="58%">@if( date('l', strtotime($suket->relasiSuketKeWarga->tanggal_lahir)) == "Sunday") Minggu @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->tanggal_lahir)) == "Monday") Senin @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->tanggal_lahir)) == "Tuesday") Selasa @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->tanggal_lahir)) == "wednesday") Rabu @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->tanggal_lahir)) == "Thrusday") Kamis @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->tanggal_lahir)) == "Friday") Jumat @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->tanggal_lahir)) == "Saturday") Sabtu @endif @if ($suket->waktu_lahir) Pukul.{{ $suket->waktu_lahir}} WIB @endif</td>
        </tr>
        <tr>
            <td style="vertical-align: top;">Tanggal</td>
            <td style="vertical-align: top;">:</td>
            <td style="text-transform: uppercase; vertical-align: top">{{ date('d-m-Y', strtotime($suket->relasiSuketKeWarga->tanggal_lahir)) }} </td>
        </tr>
        <tr>
            <td style="vertical-align: top;">Di</td>
            <td style="vertical-align: top;">:</td>
            <td style="text-transform: uppercase; vertical-align: top">{{ $suket->relasiSuketKeWarga->tempat_lahir}}</td>
        </tr>

        <tr>
            <td colspan="3" style="text-indent: 20px">Telah lahir seorang anak {{ $suket->relasiSuketKeWarga->jenis_kelamin }} bernama :</td>
        </tr>

        <tr>
            <td colspan="3" height="20px" style="text-transform: capitalize; font-size: 16px"><center><strong>{{ $suket->relasiSuketKeWarga->nama }}</strong> <center></td>
        </tr>

        <tr>
            <td colspan="3" style="text-indent: 20px">Dari seorang ibu bernama :</td>
        </tr>
        <tr>
            <td colspan="3" style="text-transform: capitalize;"><center><strong>{{ $suket->nama_ibu }}</strong> <center></td>
        </tr>
        <tr>
            <td style="vertical-align: top;">Alamat</td>
            <td style="vertical-align: top;">:</td>
            <td style="text-transform: uppercase; vertical-align: top" width="70%">{{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeAlamat->nama_alamat }} RT. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeErte->nama_erte }} RW. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeErwe->nama_erwe }} Kel. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeKelurahan->nama_kelurahan }} Kec. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeKecamatan->nama_kecamatan }} {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeKotakab->nama_kota_kab }}</td>
        </tr>
        <tr>
            <td style="vertical-align: top;">Istri dari</td>
            <td style="vertical-align: top;">:</td>
            <td style="text-transform: uppercase; vertical-align: top"><strong>{{ $suket->nama_ayah }}</strong></td>
        </tr>
        <tr>
            <td style="vertical-align: top;">Tempat,Tgl Nikah</td>
            <td style="vertical-align: top;">:</td>
            <td style="text-transform: uppercase; vertical-align: top">{{ $suket->tempat_nikah}}, {{ date('d-m-Y', strtotime($suket->tgl_nikah)) }}</td>
        </tr>
        <tr>
            <td style="vertical-align: top;">Buku Nikah No</td>
            <td style="vertical-align: top;">:</td>
            <td style="text-transform: uppercase; vertical-align: top">{{ $suket->buku_nikah}}</td>
        </tr>
        <tr>
            <td colspan="3" style="text-indent: 20px">Surat Keterangan ini dibuat atas dasar yang sebenarnya.</td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2"  align="end" style="vertical-align: top"><center>
                {{-- {{ $suket->relasiSuketKeKotakab->nama_kota_kab }}, {{ date('d-m-Y', strtotime($suket->tgl_reg_kel)) }}<br>
                @if($suket->relasiSuketKePejabat->jabatan != "Lurah")
                    a.n. LURAH <span style="text-transform: uppercase">{{ $suket->relasiSuketKeKelurahan->nama_kelurahan }}<br>
                    {{ $suket->relasiSuketKePejabat->jabatan }}
                @else
                    LURAH <span style="text-transform: uppercase">{{ $suket->relasiSuketKeKelurahan->nama_kelurahan }}
                @endif</center> --}}
            </td>
        </tr>
        <tr>
            <td colspan="3"  height="70"></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2"  align="end"><center><strong><span style="text-transform: uppercase">
                {{-- {{ $suket->relasiSuketKePejabat->name }}<br>
                NIP. {{ $suket->relasiSuketKePejabat->nip }}</strong></center> --}}
            </td>
        </tr>
    </table>

    <table align="left" width="20px" height="750px" cellpadding="7" style="border: 4px double black;">
    </table>

    <table align="left" width="400px" height="750px" cellpadding="7" style="border: 4px double black;">
        <tr><center>
            <td><center><img src="{{ asset('img/logo bandung.png') }}" height="60" width="50"></center></td>
            <td colspan="2"><center>
                <strong><u >{{ $suket->jenis_suket }}</u></strong><br>
                {{-- Nomor : {{ $suket->no_reg_kel }} --}}
            </center>
            </td></center>
        </tr>
        <tr>
            <td colspan="3"><center>
                <strong style="text-transform: uppercase">PEMERINTAH {{ $suket->relasiSuketKeKotakab->nama_kota_kab }}</strong>
            </center></td>
        </tr>
        <tr>
            <td colspan="3" height="10px"></td>
        </tr>
        <tr>
            <td colspan="3" style="text-indent: 20px">Yang bertanda tangan di bawah ini menerangkan bahwa pada :</td>
        </tr>
        <tr>
            <td style="vertical-align: top;" width="37%">Hari</td>
            <td style="vertical-align: top;" width="5%">:</td>
            <td style="text-transform: uppercase; vertical-align: top" width="58%">@if( date('l', strtotime($suket->relasiSuketKeWarga->tanggal_lahir)) == "Sunday") Minggu @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->tanggal_lahir)) == "Monday") Senin @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->tanggal_lahir)) == "Tuesday") Selasa @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->tanggal_lahir)) == "wednesday") Rabu @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->tanggal_lahir)) == "Thrusday") Kamis @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->tanggal_lahir)) == "Friday") Jumat @elseif ( date('l', strtotime($suket->relasiSuketKeWarga->tanggal_lahir)) == "Saturday") Sabtu @endif @if ($suket->waktu_lahir) Pukul.{{ $suket->waktu_lahir}} WIB @endif</td>
        </tr>
        <tr>
            <td style="vertical-align: top;">Tanggal</td>
            <td style="vertical-align: top;">:</td>
            <td style="text-transform: uppercase; vertical-align: top">{{ date('d-m-Y', strtotime($suket->relasiSuketKeWarga->tanggal_lahir)) }}</td>
        </tr>
        <tr>
            <td style="vertical-align: top;">Di</td>
            <td style="vertical-align: top;">:</td>
            <td style="text-transform: uppercase; vertical-align: top">{{ $suket->relasiSuketKeWarga->tempat_lahir}}</td>
        </tr>

        <tr>
            <td colspan="3" style="text-indent: 20px">Telah lahir seorang anak {{ $suket->relasiSuketKeWarga->jenis_kelamin }} bernama :</td>
        </tr>

        <tr>
            <td colspan="3" height="20px" style="text-transform: capitalize"><center><strong>{{ $suket->relasiSuketKeWarga->nama }}</strong> <center></td>
        </tr>

        <tr>
            <td colspan="3" style="text-indent: 20px">Dari seorang ibu bernama :</td>
        </tr>
        <tr>
            <td colspan="3" style="text-transform: capitalize;"><center><strong>{{ $suket->nama_ibu }}</strong> <center></td>
        </tr>
        <tr>
            <td style="vertical-align: top;">Alamat</td>
            <td style="vertical-align: top;">:</td>
            <td style="text-transform: uppercase; vertical-align: top" width="70%">{{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeAlamat->nama_alamat }} RT. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeErte->nama_erte }} RW. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeErwe->nama_erwe }} Kel. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeKelurahan->nama_kelurahan }} Kec. {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeKecamatan->nama_kecamatan }} {{ $suket->relasiSuketKeWarga->relasiWargaKeKeluarga->relasiKeluargaKeKotakab->nama_kota_kab }}</td>
        </tr>
        <tr>
            <td style="vertical-align: top;">Istri dari</td>
            <td style="vertical-align: top;">:</td>
            <td style="text-transform: uppercase; vertical-align: top"><strong>{{ $suket->nama_ayah }}</strong></td>
        </tr>
        <tr>
            <td style="vertical-align: top;">Tempat,Tgl Nikah</td>
            <td style="vertical-align: top;">:</td>
            <td style="text-transform: uppercase; vertical-align: top">{{ $suket->tempat_nikah}}, {{ date('d-m-Y', strtotime($suket->tgl_nikah)) }}</td>
        </tr>
        <tr>
            <td style="vertical-align: top;">Buku Nikah No</td>
            <td style="vertical-align: top;">:</td>
            <td style="text-transform: uppercase; vertical-align: top">{{ $suket->buku_nikah}}</td>
        </tr>
        <tr>
            <td colspan="3" style="text-indent: 20px">Surat Keterangan ini dibuat atas dasar yang sebenarnya.</td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2"  align="end" style="vertical-align: top"><center>
                {{-- {{ $suket->relasiSuketKeKotakab->nama_kota_kab }}, {{ date('d-m-Y', strtotime($suket->tgl_reg_kel)) }}<br>
                @if($suket->relasiSuketKePejabat->jabatan != "Lurah")
                    a.n. LURAH <span style="text-transform: uppercase">{{ $suket->relasiSuketKeKelurahan->nama_kelurahan }}<br>
                    {{ $suket->relasiSuketKePejabat->jabatan }}
                @else
                    LURAH <span style="text-transform: uppercase">{{ $suket->relasiSuketKeKelurahan->nama_kelurahan }}
                @endif</center> --}}
            </td>
        </tr>
        <tr>
            <td colspan="3"  height="70"></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2"  align="end"><center><strong><span style="text-transform: uppercase">
                {{-- {{ $suket->relasiSuketKePejabat->name }}<br>
                NIP. {{ $suket->relasiSuketKePejabat->nip }}</strong></center> --}}
            </td>
        </tr>
    </table>
</body>

</html>
