@extends('romantis.layout1')

@section('content')
 <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container mt-2" >

        <ol>
          <li><a href="{{route('romantis')}}#why-us" style="text-decoration:none">Home</a></li>
          <li><a href="#">Program Kang Pisman</a></li>
        </ol>
        <h2>Program Pengelolaan Sampah Masyarakat</h2>

      </div>
    </section>

      <!-- ======= Portfolio Section ======= -->
    <section id="portfolio" class="portfolio" style="margin-top: -50px">
        <div class="container" data-aos="fade-up">


            <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
                @foreach ($quotes as $galeri)
                @if($galeri->image)
                    <div class="col-lg-4 col-md-6 portfolio-item filter-rw">
                        <div class="portfolio-img"><img src="{{ asset('storage/' .$galeri->image) }}" class="img-fluid" alt=""></div>
                        <div class="portfolio-info">
                            <h4>{{$galeri->title}}</h4>
                            <p>{!!$galeri->excerpt!!}</p>
                            <a href="{{ asset('storage/' .$galeri->image) }}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="{{$galeri->title}}<br>{{$galeri->publish_at}}"><i class="bx bx-plus"></i></a>
                            <a href="{{route('romantis.posting.allnews', $galeri->id)}}" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                        </div>
                    </div>
                    @endif
                @endforeach


            </div>

        </div>
    </section><!-- End Portfolio Section -->


@endsection

