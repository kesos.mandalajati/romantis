@extends('romantis.layout1')

@section('content')

<!-- ======= Breadcrumbs ======= -->
<section id="breadcrumbs" class="breadcrumbs">
    <div class="container" >

    <ol  class="mt-5">
      <li><a href="{{route('romantis')}}" style="text-decoration:none">Home</a></li>
      <li>Galeri Giat</li>
    </ol>
    <h2>Galeri Giat</h2>

  </div>
</section><!-- End Breadcrumbs -->

<!-- ======= Galeri Giat Section ======= -->
<section id="portfolio-details" class="portfolio-details">
    <div class="container-fluid px-4" >

        <div class="row" data-masonry='{"percentPosition": true }'>
            @foreach ($posting as $posting)
                <div class="col-sm-6 col-lg-4 mb-1">
                    <div class="card shadow-sm" style="height: 500px">
                        <div style="max-height: 250px; overflow:hidden;">
                            @if ($posting->quote)
                            <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/tv/{{ $posting->quote }}/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="14" style="width: 100%"></blockquote>
                            @elseif ($posting->penulis)
                                <iframe src="{{ url('https://www.youtube.com/embed/' .$posting->penulis) }}" allow="encrypted-media; clipboard-write; gyroscope; picture-in-picture" allowfullscreen style="width: 100%"></iframe>
                            @elseif ($posting->image)
                                <img src="{{ asset('storage/' .$posting->image) }}" class="img-preview img-fluid">
                            @endif
                        </div>
                        <div class="card-body">
                            <h6 class="card-title">{{ $posting->title }} <p style="font-size: 12px"> {{ date('d-m-Y', strtotime($posting->publish_at)) }}</p></h6>
                            <p class="card-text">{!! $posting->excerpt !!} <a type="button" class="link" style="font-size: 12px" href="{{ route('romantis.posting.allnews', $posting->id) }}">selengkapnya</a></p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

  </div>
</section><!-- End Portfolio Details Section -->



@endsection
