@extends('romantis.layout1')

@section('content')

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container mt-2" >

        <ol >
          <li><a href="{{route('romantis')}}#portfolio" style="text-decoration:none">Home</a></li>
          <li>Galeri Giat</li>
        </ol>
        <h2>Galeri Giat</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Galeri Giat Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
        <div class="container-fluid px-4" >

        <div class="col-12 table-responsive-sm">
            <table class="table table-bordered align-middle text-center" style="border: 4px; border-color: blue;font-size: 12px">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Tanggal</th>
                        <th scope="col">Foto</th>
                        <th scope="col">Kegiatan</th>
                    </tr>
                </thead>
                <tbody
                    @foreach ($posting as $key => $posting)
                    <tr>
                        <th scope="row">{{ ++$key }}</th>
                        <td scope="row">
                            {{  date('d-m-Y', strtotime ($posting->publish_at)) }}</td>
                        <td scope="row">
                            @if($posting->image)
                                <img src="{{ asset('storage/' .$posting->image) }}" class="img-preview img-fluid" style="width: 25%;">
                            @endif<br>
                            @if($posting->penulis)
                                <iframe src="{{ url('https://www.youtube.com/embed/' .$posting->penulis) }}" allow="encrypted-media; clipboard-write; gyroscope; picture-in-picture" style="width: 25%"></iframe>
                            @endif<br>
                            @if($posting->quote)
                            <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/tv/{{ $posting->quote }}/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="14" style="height: 20%"></blockquote>
                            @endif
                        </td>
                        <td scope="row" style="text-align: justify">
                            {{ $posting->title }}<br>
                            {{ $posting->excerpt }} <a href="{{ route('romantis.posting.allnews', $posting->id) }}"  style="align: end"><i class="bi bi-folder2-open"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

      </div>
    </section><!-- End Galeri Giat Section -->


@endsection
@section('scripts')

<script async src="//www.instagram.com/embed.js"></script>

@endsection

