@extends('romantis.layout1')

@section('content')
 <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container mt-2" >

        <ol>
          <li><a href="{{route('romantis')}}#why-us" style="text-decoration:none">Home</a></li>
          <li><a href="#">Potensi Perekonomian</a></li>
        </ol>
        <h2>Usaha Kecil Menengah</h2>

      </div>
    </section>

      <!-- ======= Portfolio Section ======= -->
    <section id="portfolio" class="portfolio" style="margin-top: -50px">
        <div class="container" data-aos="fade-up">

            <ul id="portfolio-flters" class="d-flex justify-content-center" data-aos="fade-up" data-aos-delay="100">
                {{-- <li data-filter="*" class="nav-item active">
                    All
                </li> --}}
                <li data-filter=".filter-kopishop">Kopi Shop</li>
                <li data-filter=".filter-barbershop">Barber Shop</li>
                <li data-filter=".filter-bengkel">Bengkel</li>
                <li data-filter=".filter-warung">Warung</li>
            </ul>

            <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
                @foreach ($kopishop as $galeri)
                @if($galeri->image)
                    <div class="col-lg-4 col-md-6 portfolio-item filter-kopishop">
                        <div class="portfolio-img"><img src="{{ asset('storage/' .$galeri->image) }}" class="img-fluid" alt=""></div>
                        <div class="portfolio-info">
                            <h4>{{$galeri->title}}</h4>
                            <p>{!!$galeri->excerpt!!}</p>
                            <a href="{{ asset('storage/' .$galeri->image) }}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="{{$galeri->title}}<br>{{$galeri->publish_at}}"><i class="bx bx-plus"></i></a>
                            <a href="{{route('romantis.profil.lihatumkm', $galeri->id)}}" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                        </div>
                    </div>
                    @endif
                @endforeach

                @foreach ($barbershop as $galeri)
                @if($galeri->image)
                    <div class="col-lg-4 col-md-6 portfolio-item filter-barbershop">
                        <div class="portfolio-img"><img src="{{ asset('storage/' .$galeri->image) }}" class="img-fluid" alt=""></div>
                        <div class="portfolio-info">
                            <h4>{{$galeri->title}}</h4>
                            <p>{!!$galeri->excerpt!!}</p>
                            <a href="{{ asset('storage/' .$galeri->image) }}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="{{$galeri->title}}<br>{{$galeri->publish_at}}"><i class="bx bx-plus"></i></a>
                            <a href="{{route('romantis.profil.lihatumkm', $galeri->id)}}" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                        </div>
                    </div>
                    @endif
                @endforeach

                @foreach ($bengkel as $galeri)
                @if($galeri->image)
                    <div class="col-lg-4 col-md-6 portfolio-item filter-bengkel">
                        <div class="portfolio-img"><img src="{{ asset('storage/' .$galeri->image) }}" class="img-fluid" alt=""></div>
                        <div class="portfolio-info">
                            <h4>{{$galeri->title}}</h4>
                            <p>{!!$galeri->excerpt!!}</p>
                            <a href="{{ asset('storage/' .$galeri->image) }}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="{{$galeri->title}}<br>{{$galeri->publish_at}}"><i class="bx bx-plus"></i></a>
                            <a href="{{route('romantis.profil.lihatumkm', $galeri->id)}}" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                        </div>
                    </div>
                    @endif
                @endforeach

                @foreach ($warung as $galeri)
                @if($galeri->image)
                    <div class="col-lg-4 col-md-6 portfolio-item filter-bengkel">
                        <div class="portfolio-img"><img src="{{ asset('storage/' .$galeri->image) }}" class="img-fluid" alt=""></div>
                        <div class="portfolio-info">
                            <h4>{{$galeri->title}}</h4>
                            <p>{!!$galeri->excerpt!!}</p>
                            <a href="{{ asset('storage/' .$galeri->image) }}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="{{$galeri->title}}<br>{{$galeri->publish_at}}"><i class="bx bx-plus"></i></a>
                            <a href="{{route('romantis.profil.lihatumkm', $galeri->id)}}" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>

        </div>
    </section><!-- End Portfolio Section -->


@endsection

