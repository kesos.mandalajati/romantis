@extends('romantis.layout1')

@section('content')
 <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container mt-2" >

        <ol>
          <li><a href="{{route('romantis')}}#portfolio" style="text-decoration:none">Home</a></li>
          <li><a href="{{route('romantis.posting.giat')}}" style="text-decoration:none">Galeri Giat</a></li>
        </ol>
        <h2>Giat</h2>

      </div>
    </section><!-- End Breadcrumbs -->

<!-- ======= Portfolio Details Section ======= -->
<section id="portfolio-details" class="portfolio-details">
  <div class="container" style="margin-top: -25px">
        <div class="row ">
            @if ($posting->image)
                <div class="col-lg-6">
                    <img src="{{ asset('storage/' .$posting->image) }}" class="img-preview img-fluid">
                </div>
                <div class="col-lg-6">
                    <div class="portfolio-info">
                        {{-- <h3>{{ $posting->title }}</h3> --}}
                        <ul>
                            <li><strong>Penulis</strong>: {{ $posting->relasiPostingKeUser->name }}</li>
                            <li><strong>Waktu</strong>: {{ date('d-m-Y', strtotime($posting->publish_at)) }}</li>
                        </ul>
                    </div>
                    <div class="portfolio-description">
                        <h2>{{ $posting->title }}</h2>
                        <p class="normal" style="normal">
                            {!! $posting->body !!}
                        </p>
                    </div>
                </div>
            @else
                <div class="col-lg-12">
                    <div class="portfolio-description">
                        <h2>{{ $posting->title }} <br>
                        <h6>{{ $posting->relasiPostingKeUser->name }},
                            {{ date('d-m-Y', strtotime($posting->publish_at)) }}</h6></h2>
                        <p class="normal" style="normal">
                            {!! $posting->body !!}
                        </p>
                    </div>
                </div>
            @endif
        </div>
    </section>
    <!-- End Portfolio Details Section -->


@endsection

