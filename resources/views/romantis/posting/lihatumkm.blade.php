@extends('romantis.layout1')

@section('content')
 <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container mt-2" >

        <ol>
          <li><a href="{{route('romantis')}}#why-us" style="text-decoration:none">Home</a></li>
          <li><a href="{{route('romantis.profil.perekonomian')}}" style="text-decoration:none">UKM</a></li>
        </ol>
        <h2>Galeri Potensi Wilayah</h2>

      </div>
    </section><!-- End Breadcrumbs -->

<!-- ======= Portfolio Details Section ======= -->
<section id="portfolio-details" class="portfolio-details">
  <div class="container" style="margin-top: -25px">
        <div class="row ">
            @if ($umkm->image)
                <div class="col-lg-6">
                    <img src="{{ asset('storage/' .$umkm->image) }}" class="img-preview img-fluid">
                </div>
            @endif
            <div class="col-lg-6">
                <div class="portfolio-info">
                    {{-- <h3>{{ $umkm->title }}</h3> --}}
                    <ul>
                        <li><strong>Nama Tempat</strong>: {{ $umkm->verifikasialamat }}</li>
                        <li><strong>Alamat</strong>: {{ $umkm->nama_alamat }}</li>
                        <li><strong>NOP</strong>: {{ $umkm->nop }}</li>
                    </ul>
                </div>
                <div class="portfolio-description">
                    @if (!empty($umkm->alamat_kk_latitude))
                        <iframe
                            src="https://maps.google.com/?q={{ $umkm->alamat_kk_latitude }},{{ $umkm->alamat_kk_longitude }}&z=19&t=h&output=embed" width="100%" height="250">
                        </iframe>
                    @endif
                </div>
            </div>
    </section>
    <!-- End Portfolio Details Section -->


@endsection

