<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Sukamiskin | Romantis</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{asset('vendor/aos/aos.css')}}" rel="stylesheet">
  <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
  <link href="{{asset('vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendor/remixicon/remixicon.css')}}" rel="stylesheet">
  <link href="{{asset('vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{asset('css/romantis/style.css')}}" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Arsha
  * Updated: Jul 05 2023 with Bootstrap v5.3.0
  * Template URL: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center">

      <h1 class="logo me-auto"><a href="{{route('romantis')}}">Romantis</a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <a href="{{route('romantis')}}" class="logo me-auto"><img src="{{asset('img/romantis/logo.png')}}" alt="" class="img-fluid"></a>

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="#hero">Beranda</a></li>
          <li><a class="nav-link scrollto" href="{{route('romantis.profil.kelurahan')}}">Profil</a></li>
          {{-- <li class="dropdown"><a href="#"><span>Profil</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="#">Struktur Pemerintahan</a></li>
              <li><a href{{route('romantis.profil.kelurahan')}}>Monografi</a></li>
              <li class="dropdown"><a href="#"><span>Sarana Prasarana</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="#">Kesehatan</a></li>
                  <li><a href="#">Pendidikan</a></li>
                  <li><a href="#">Keagamaan</a></li>
                  <li><a href="#">Perekonomian</a></li>
                </ul>
              </li>
              <li><a href="#">Sejarah</a></li>
              <li><a href="#">Prestasi</a></li>
            </ul>
          </li> --}}
          <li><a class="nav-link scrollto" href="#services">Layanan</a></li>
          <li><a class="nav-link scrollto" href="#portfolio">Giat</a></li>
          <li><a class="nav-link   scrollto" href="#why-us">Pemberdayaan</a></li>
          <li><a class="getstarted scrollto" href="#contact">Hubungi Kami</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">

    <div class="container">
      <div class="row">
        <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
          <h1>Ruang Informasi Warga Sukamiskin</h1>
          <h2>Layanan Pintar Kelurahan | Inspiring Sukamiskin</h2>
          <div class="d-flex justify-content-center justify-content-lg-start">
            <a href="https://wa.me/628976644554" target="blank" class="btn-get-started scrollto">Hubungi Kami</a>
            <a href="https://www.youtube.com/watch?v=WafA0UEBKt0" class="glightbox btn-watch-video"><i class="bi bi-play-circle"></i><span>Selayang Pandang</span></a>
          </div>
        </div>
        <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
          {{-- <img src="{{asset('img/romantis/hero-img.png')}}" class="img-fluid animated" alt=""> --}}
           <img src="{{asset('img/romantis/kantorsukamiskin.jpg')}}" class="img-fluid animated" alt="">
        </div>
      </div>
    </div>

  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= Clients Section ======= -->
    <section id="clients" class="clients section-bg">
      <div class="container">

        <div class="row" data-aos="zoom-in">

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <a href="https://wargapedia.mandalajati.bandung.go.id/login" target="blank">
            <img src="{{asset('img/romantis/warped.png')}}" class="img-fluid" alt=""></a>
          </div>

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <a href="https://sipaku.bandung.go.id/" target="blank"><img src="{{asset('img/romantis/sipaku.png')}}" class="img-fluid" alt=""></a>
          </div>

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <a href="https://cekbansos.kemensos.go.id/" target="blank"><img src="{{asset('img/romantis/dtks.png')}}" class="img-fluid" alt=""></a>
          </div>

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <a href="https://posyandu.kecamatanarcamanik.com/" target="blank"><img src="{{asset('img/romantis/sipofo.jpg')}}" class="img-fluid" alt=""></a>
          </div>

          {{-- <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <a href="https://simdik.bandung.go.id/login" target="blank"><img src="{{asset('img/romantis/simdik.png')}}" class="img-fluid" alt=""></a>
          </div> --}}

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <a href="https://siks.kemensos.go.id/login" target="blank"><img src="{{asset('img/romantis/siksng.png')}}" class="img-fluid" alt=""></a>
          </div>

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <a href="https://sipp.bapenda.bandung.go.id/" target="blank"><img src="{{asset('img/romantis/sipp.jpg')}}" class="img-fluid" alt=""></a>
          </div>

        </div>

      </div>
    </section><!-- End Cliens Section -->

    <!-- ======= About Us Section ======= -->
    <section id="about" class="about">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Tentang Romantis</h2>
        </div>

        <div class="row content">
          <div class="col-lg-6">
            <p>
              Dalam rangka meningkatan kepuasan masyarakat, Kelurahan Sukamiskin menciptakan Ruang Informasi Warga Sukamiskin guna menyediakan aksesibilitas yang mudah, respon cepat dan informasi yang jelas melalui website atau hotline pelayanan. Sehingga warga dapat menyampaikan permintaan atau keluhan dengan mudah dan cepat.
            </p>

          </div>
          <div class="col-lg-6 pt-4 pt-lg-0">
            <ul>
                <li><i class="ri-check-double-line"></i> Aksesibilitas 24/7: memungkinkan masyarakat untuk mengakses informasi atau mendapatkan bantuan kapan saja, tanpa terbatas oleh jam kerja atau waktu operasional.</li>
                <li><i class="ri-check-double-line"></i> Keterjangkauan: masyarakat dapat memperoleh informasi tanpa perlu biaya operasional yang besar.</li>
                <li><i class="ri-check-double-line"></i>Komunikasi yang efektif: informasi yang disampaikan dapat disesuaikan dengan kebutuhan masyarakat, dan kelurahan dapat memberikan pembaruan, pengumuman, atau promosi dengan cepat dan mudah.</li>
              </ul>
            {{-- <a href="#" class="btn-learn-more">Pelajari</a> --}}
          </div>
        </div>

      </div>
    </section><!-- End About Us Section -->

    <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="why-us section-bg">
      <div class="container-fluid" data-aos="fade-up">

        <div class="row">

          <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">

            <div class="content" style="margin-top: -50px">
              <h3>Program dan Kegiatan <strong>Pemberdayaan Masyarakat Unggulan</strong></h3>
              <p>
                Berikut adalah program dan kegiatan pemberdayaan masyarakat yang dikembangkan, baik oleh warga, pemerintahan, maupun lembaga kemasyarakatan pada Kelurahan Sukamiskin.
              </p>
            </div>

            <div class="accordion-list">
              <ul>
                <li>
                  <a data-bs-toggle="collapse" class="collapse" data-bs-target="#accordion-list-1"><span>01</span> Sampah : Tinu Rujit Jadi Duit <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-1" class="collapse show" data-bs-parent=".accordion-list">
                    <p>
                        Kang Pisman telah memberikan nilai ekonomi tambahan di warga Sukamiskin. Dengan memilah sampah organik warga dapat berkreasi dan berinovasi serta menjual hasil sampah anorganik.
                    </p>
                    <a href="{{route('romantis.profil.pengelolaansampah')}}"><i class="bi bi-search"></i></a>
                  </div>
                </li>

                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-2" class="collapsed"><span>02</span> Program Pemberdayaan Masyarakat <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-2" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                        Pemerintahan yang efektif dan sukses tidak hanya fokus pada pembangunan fisik dan ekonomi, tetapi juga memperhatikan aspek sosial dan pemberdayaan masyarakat secara menyeluruh.
                    </p>
                    <a href="{{route('romantis.profil.pemberdayaanmasyarakat')}}"><i class="bi bi-search"></i></a>
                  </div>
                </li>


                <li>
                    <a  data-bs-toggle="collapse" data-bs-target="#accordion-list-3" class="collapsed"><span>03</span> Peningkatan Usaha Kecil Menengah <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                    <div id="accordion-list-3" class="collapse" data-bs-parent=".accordion-list">
                      <p>
                        UKM memiliki peran yang signifikan dalam menciptakan lapangan kerja, menggerakkan perekonomian lokal, dan mendorong inovasi. Tersedianya data UMKM dalam Romantis merupakan upaya Kelurahan Sukamiskin untuk meningkatkan akses UMKM terhadap infrastruktur dan teknologi modern sebagai sarana pemasaran gratis.
                      </p>
                      <a href="{{route('romantis.profil.perekonomian')}}"><i class="bi bi-search"></i></a>
                    </div>
                  </li>

              </ul>
            </div>

          </div>

          <div class="col-lg-5 mb-2 align-items-stretch order-1 order-lg-2 img"  data-aos="zoom-in" data-aos-delay="150">
                <img src="{{asset('img/romantis/why-us.png')}}" class="img-fluid animated" alt="">
            </div>
        </div>

      </div>
    </section><!-- End Why Us Section -->

    <!-- ======= Skills Section ======= -->
    <section id="skills" class="skills">
      <div class="container" data-aos="fade-up">

        <div class="row">
          <div class="col-lg-6 d-flex align-items-center" data-aos="fade-right" data-aos-delay="100">
            <img src="{{asset('img/romantis/skills.png')}}" class="img-fluid animated mt-5" alt="">
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0 content" data-aos="fade-left" data-aos-delay="100">
            <h3 class="mb-5">Gambaran Kondisi Lingkungan dan Kesejahteraan Sosial</h3>
            {{-- <p class="fst-italic">
              Berikut adalah persentase Pemerlu Pelayanan Kesejahteraan Sosial (PPKS), yaitu jumlah PPKS dibandingkan dengan jumlah penduduk pada Kelurahan Sukamiskin.
            </p> --}}

            <div class="skills-content">

              <div class="progress">
                <span class="skill">Open Defecation<i class="val">96%</i></span>
                <div class="progress-bar-wrap">
                  <div class="progress-bar" role="progressbar" aria-valuenow="96" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>

              <div class="progress">
                <span class="skill">Stunting<i class="val">98 anak</i></span>
                <div class="progress-bar-wrap">
                  <div class="progress-bar" role="progressbar" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>

              <div class="progress">
                <span class="skill">Bank Sampah<i class="val">82%</i></span>
                <div class="progress-bar-wrap">
                  <div class="progress-bar" role="progressbar" aria-valuenow="14" aria-valuemin="0" aria-valuemax="17"></div>
                </div>
              </div>

              <div class="progress">
                <span class="skill">Rutilahu<i class="val">55%</i></span>
                <div class="progress-bar-wrap">
                  <div class="progress-bar" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>

            </div>

          </div>
        </div>

      </div>
    </section><!-- End Skills Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Layanan</h2>
          <p>Kami menyediakan layanan dengan persyaratan yang jelas dan aplikasi penunjang yang inovatif, memastikan pelayanan yang efisien dan berkualitas kepada masyarakat | Kepuasan masyarakat adalah prioritas utama kami</p>
        </div>

        <div class="row">
            <div class="col-xl-3 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
                <div class="icon-box">
                    <div class="icon"><i class="bx bx-layer"></i></div>

                <h4><a href="{{route('romantis.profil.persyaratan')}}">Persyaratan</a></h4>
                <p>Pastikan Anda membawa semua dokumen yang relevan untuk membantu mempercepat proses pelayanan kami. </p>
                </div>
            </div>

            <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="zoom-in" data-aos-delay="200">
                <div class="icon-box">
                <div class="icon"><i class="bi bi-laptop"></i></div>
                <h4><a href="{{route('romantis.profil.simpelonline')}}">Simpel Online</a></h4>
                <p>Self service pelayanan pembuatan surat-surat keterangan dan ijin usaha dari rumah</p>
                </div>
            </div>

          <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-xl-0" data-aos="zoom-in" data-aos-delay="300">
            <div class="icon-box">
              <div class="icon"><i class="bi bi-folder-check"></i></div>
              <h4><a href="https://sipaku.bandung.go.id/" target="blank">Sipaku</a></h4>
              <p> Sistem Informasi Pelayanan Administrasi Kewilayahan Terpadu untuk memantau proses administrasi kependudukan </p>
            </div>
          </div>

          <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-xl-0" data-aos="zoom-in" data-aos-delay="400">
            <div class="icon-box">
                <div class="icon"><i class="bx bx-file"></i></div>
              <h4><a href="{{route('romantis.profil.skm')}}">Survey kepuasan</a></h4>
              <p>Umpan balik untuk membantu memperbaiki dan meningkatkan pelayanan kami</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Services Section -->

    <!-- ======= Cta Section ======= -->
    <section id="cta" class="cta">
      <div class="container" data-aos="zoom-in">

        <div class="row">
          <div class="col-lg-9 text-center text-lg-start">
            <h3>Simpelikes</h3>
            <h5 class="text-white"><strong class="text-danger">S</strong>elf<strong class="text-danger"> Im</strong>put <strong class="text-danger">P</strong>encatatan <strong class="text-danger">El</strong>ektronik <strong class="text-danger">I</strong>dentitas <strong class="text-danger">Ke</strong>pendudukan <strong class="text-danger">S</strong>ukamiskin</h5>
            <p> Partisipasi masyarakat sangat penting dalam proses digitalisasi data kependudukan. Masyarakat dapat berperan aktif dalam memperbarui data diri mereka sendiri, seperti perubahan alamat, status perkawinan, atau penambahan anggota keluarga. Partisipasi aktif masyarakat dalam digitalisasi data kependudukan akan membantu menciptakan data yang lebih akurat, meningkatkan efisiensi pelayanan, dan pengambilan keputusan yang lebih baik dalam kebijakan publik.</p>
          </div>
          <div class="col-lg-3 cta-btn-container text-center">
            <a class="cta-btn align-middle" href="{{route('romantis.profil.simpelikes')}}">Mengisi Identitas Sendiri</a>
          </div>
        </div>

      </div>
    </section><!-- End Cta Section -->

    <!-- ======= Portfolio Section ======= -->
    <section id="portfolio" class="portfolio">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Giat</h2>
                <p>Berikut adalah galeri giat koordinasi dan kerjasama dengan instansi terkait, penyuluhan, pembinaan lembaga kemasyarakatan, penanganan masalah dan penyaluran bantuan sosial, pembangunan infrastruktur, pemantauan dan evaluasi pembangunan, pembinaan aparatur kelurahan, pengelolaan keuangan penanganan administrasi kependudukan, pengembangan potensi ekonomi lokal dan pemberdayaan masyarakat. </p>
            </div>

            <ul id="portfolio-flters" class="d-flex justify-content-center" data-aos="fade-up" data-aos-delay="100">
                {{-- <li data-filter="*" class="nav-item active">
                    All
                </li> --}}
                <li data-filter=".filter-all">Medsos</li>
                <li data-filter=".filter-program">Program</li>
                <li data-filter=".filter-giat">Giat</li>
            </ul>
            <ul style="margin-top: -25px"><center>
                <a class="nav-link" href="{{route('romantis.posting.giat')}}"><i class="bi bi-three-dots"></i></a>
                </center>
            </ul>

            <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
                @foreach ($medsos as $galeri)
                @if($galeri->image)
                    <div class="col-lg-4 col-md-6 portfolio-item filter-all">
                        <div class="portfolio-img"><img src="{{ asset('storage/' .$galeri->image) }}" class="img-fluid" alt=""></div>
                        <div class="portfolio-info">
                            <h4>{{$galeri->title}}</h4>
                            <p>{!!$galeri->excerpt!!}</p>
                            <a href="{{ asset('storage/' .$galeri->image) }}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="{{$galeri->title}}<br>{{$galeri->publish_at}}"><i class="bx bx-plus"></i></a>
                            <a href="{{route('romantis.posting.allnews', $galeri->id)}}" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                        </div>
                    </div>
                    @endif
                @endforeach

                @foreach ($headlines as $galeri)
                @if($galeri->image)
                    <div class="col-lg-4 col-md-6 portfolio-item filter-program">
                        <div class="portfolio-img"><img src="{{ asset('storage/' .$galeri->image) }}" class="img-fluid" alt=""></div>
                        <div class="portfolio-info">
                            <h4>{{$galeri->title}}</h4>
                            <p>{!!$galeri->excerpt!!}</p>
                            <a href="{{ asset('storage/' .$galeri->image) }}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="{{$galeri->title}}<br>{{$galeri->publish_at}}"><i class="bx bx-plus"></i></a>
                            <a href="{{route('romantis.posting.allnews', $galeri->id)}}" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                        </div>
                    </div>
                    @endif
                @endforeach

                @foreach ($giat as $galeri)
                @if($galeri->image)
                    <div class="col-lg-4 col-md-6 portfolio-item filter-giat">
                        <div class="portfolio-img"><img src="{{ asset('storage/' .$galeri->image) }}" class="img-fluid" alt=""></div>
                        <div class="portfolio-info">
                            <h4>{{$galeri->title}}</h4>
                            <p>{!!$galeri->excerpt!!}</p>
                            <a href="{{ asset('storage/' .$galeri->image) }}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="{{$galeri->title}}<br>{{$galeri->publish_at}}"><i class="bx bx-plus"></i></a>
                            <a href="{{route('romantis.posting.allnews', $galeri->id)}}" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>

        </div>
    </section><!-- End Portfolio Section -->

    <!-- ======= Team Section ======= -->
    <section id="team" class="team section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Team</h2>
          <p>Dalam menjalankan tugas pokok dan fungsi, Kelurahan Sukamiskin memiliki team yang responsif, profesional, empatis, komunikatif, adaptif, ramah, tanggap dan berkomitmen yang tinggi pada pelayanan kepada masyarakat.</p>
        </div>

        <div class="row">

          <div class="col-lg-6 mt-4" data-aos="zoom-in" data-aos-delay="100">
            <div class="member d-flex align-items-start">
              <div class="pic"><img src="{{asset('img/romantis/team/1.jpg')}}" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>FARIDA AGUSTINI,S.PD,MM</h4>
                <span>Lurah</span>
                <p>Memimpin dan mengoordinasikan semua kegiatan pemerintahan kelurahan sesuai dengan peraturan perundang-undangan yang berlaku. Melakukan pengawasan, pembinaan, dan evaluasi kinerja aparatur kelurahan guna memastikan tugas dan tanggung jawab mereka terlaksana dengan baik.</p>
              </div>
            </div>
          </div>

          <div class="col-lg-6 mt-4" data-aos="zoom-in" data-aos-delay="200">
            <div class="member d-flex align-items-start">
              <div class="pic"><img src="{{asset('img/romantis/team/3.jpeg')}}" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>RADEN AYU ANNISA YUNIASTUTI,SE</h4>
                <span>Kasi Pemerintahan</span>
                <p>Memastikan pelayanan administrasi kependudukan, pelayanan surat menyurat, dan izin usaha berjalan dengan baik.</p>
              </div>
            </div>
          </div>

          <div class="col-lg-6 mt-4" data-aos="zoom-in" data-aos-delay="300">
            <div class="member d-flex align-items-start">
              <div class="pic"><img src="{{asset('img/romantis/team/4.jpeg')}}" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>FITRI FITRYANA MUHARAM,S.AP</h4>
                <span>Kasi Kesejahteraan Sosial</span>
                <p>Memastikan sinergi dalam penanganan masalah-masalah sosial, kondisi krisis dan kedaruratan, keamanan, kesejahteraan masyarakat, dan pemberdayaan masyarakat</p>
              </div>
            </div>
          </div>

          <div class="col-lg-6 mt-4" data-aos="zoom-in" data-aos-delay="400">
            <div class="member d-flex align-items-start">
              <div class="pic"><img src="{{asset('img/romantis/team/8.jpeg')}}" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>SULAEMAN,SE</h4>
                <span>Kasi Ekomoni Pembangunan</span>
                <p>Mengidentifikasi potensi ekonomi, infrastruktur dan lingkungan. Memastikan program pembangunan berjalan dengan lancar. Memberikan pembinaan dan dukungan kepada UMKM</p>
              </div>
            </div>
          </div>

          <div class="col-lg-6 mt-4" data-aos="zoom-in" data-aos-delay="500">
            <div class="member d-flex align-items-start">
              <div class="pic"><img src="{{asset('img/romantis/team/7.jpeg')}}" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>FEBBRIAN BABY PAMUNGKAS, S.IP</h4>
                <span>Pengadministrasian Pemerintahan</span>
                <p>Berada di bawah Kasi Pemerintahan</p>
              </div>
            </div>
          </div>

          <div class="col-lg-6 mt-4" data-aos="zoom-in" data-aos-delay="600">
            <div class="member d-flex align-items-start">
              <div class="pic"><img src="{{asset('img/romantis/team/6.jpeg')}}" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>PUJI WARDANI, ST, MM</h4>
                <span>Pengelola Keuangan</span>
                <p>Berada di bawah Sekretaris Kelurahan</p>
              </div>
            </div>
          </div>

          <div class="col-lg-6 mt-4" data-aos="zoom-in" data-aos-delay="700">
            <div class="member d-flex align-items-start">
              <div class="pic"><img src="{{asset('img/romantis/team/5.jpeg')}}" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>DIKIKI HERMAWAN</h4>
                <span>Pengadministrasi Kesejahteraan Sosial</span>
                <p>Berada di bawah Kasi Kesejahteraan SOsial</p>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Team Section -->


    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Hubungi Kami</h2>
          <p>Selamat datang di Hotline Romantis, pintu akses langsung untuk mendapatkan informasi dari berbagai pelayanan yang anda butuhkan. Dengan layanan kami, kami ada di sini untuk membantu menjawab pertanyaan Anda, menyelesaikan masalah, dan memberikan bimbingan yang Anda butuhkan | Respon Cepat, Informasi Terpercaya</p>
        </div>

        <div class="row">

          <div class="col-sm-5 d-flex align-items-stretch">
            <div class="info">
              <div class="address">
                <i class="bi bi-geo-alt"></i>
                <h4>Alamat:</h4>
                <p>Jl. Pacuan Kuda No.204, Sukamiskin, Kec. Arcamanik, Kota Bandung, Jawa Barat 40293</p>
              </div>

              <div class="email">
                <a href=""mailto: kel.sukamiskin204@gmail.com?subject=konsultasi dan informasi" target="blank"><i class="bi bi-envelope"></i></a>
                <h4>Email:</h4>
                <p>kel.sukamiskin204@gmail.com</p>
              </div>

              <div class="phone">
                <a href="https://wa.me/628976644554" target="blank"><i class="bi bi-whatsapp"></i></a>
                <h4>Whatsapp:</h4>
                <p>+62 81221655797</p>
              </div>

              <iframe
                src="https://maps.google.com/?q=-6.9099006, 107.6746083&z=19&t=h&output=embed" frameborder="0" style="border:0; width: 100%; height: 290px;" allowfullscreen>
            </iframe>


            </div>

          </div>

          <div class="col-sm-7 mt-5 mt-lg-0 d-flex align-items-stretch">
            <form action="forms/contact.php" method="post" role="form" class="php-email-form">
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="name">Nama Anda</label>
                  <input type="text" name="name" class="form-control" id="name" required>
                </div>
                <div class="form-group col-md-6">
                  <label for="name">Email Anda</label>
                  <input type="email" class="form-control" name="email" id="email" required>
                </div>
              </div>
              <div class="form-group">
                <label for="name">Perihal</label>
                <input type="text" class="form-control" name="subject" id="subject" required>
              </div>
              <div class="form-group">
                <label for="name">Pesan</label>
                <textarea class="form-control" name="message" rows="10" required></textarea>
              </div>
              <div class="my-3">
                <div class="loading">Loading</div>
                <div class="error-message"></div>
                <div class="sent-message">Pesan anda sudah terkirim!</div>
              </div>
              <div class="text-center"><button type="submit">Kirim Pesan</button></div>
            </form>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-newsletter">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-6">
            <h4>Media Sosial</h4>
            <p>Bagikan halaman kami kepada teman dan keluarga Anda yang mungkin membutuhkan layanan kami. Dengan demikian, mereka juga dapat mengikuti dan mendapatkan manfaat dari konten dan informasi yang kami berikan.</p>
            <div class="social-links mt-3">
                <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="https://instagram.com/kel_sukamiskin?igshid=YmM0MjE2YWMzOA==" class="instagram" target="_blank"><i class="bx bxl-instagram"></i></a>
                <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
              </div>
            {{-- <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form> --}}
          </div>
        </div>
      </div>
    </div>


    <div class="container footer-bottom clearfix">
        <div class="copyright">
          &copy; Copyright <strong><span>kel. sukamiskin</span></strong>. <br>All Rights Reserved
        </div>
        <div class="credits">
          <!-- All the links in the footer should remain intact. -->
          <!-- You can delete the links only if you purchased the pro version. -->
          <!-- Licensing information: https://bootstrapmade.com/license/ -->
          <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/ -->
          Designed by <a href="#">dummieling@gmail.com</a>
        </div>
      </div>
    </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{asset('vendor/aos/aos.js')}}"></script>
  <script src="{{asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('vendor/glightbox/js/glightbox.min.js')}}"></script>
  <script src="{{asset('vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
  <script src="{{asset('vendor/swiper/swiper-bundle.min.js')}}"></script>
  <script src="{{asset('vendor/waypoints/noframework.waypoints.js')}}"></script>
  <script src="{{asset('vendor/php-email-form/validate.js')}}"></script>

  <!-- Template Main JS File -->
  <script src="{{asset('js/romantis/main.js')}}"></script>

</body>

</html>
