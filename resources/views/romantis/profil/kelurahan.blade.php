@extends('romantis.layout1')

@section('content')
<!-- ======= Breadcrumbs ======= -->
<section id="breadcrumbs" class="breadcrumbs">
    <div class="container mt-2" >

    <ol >
      <li><a href="{{route('romantis')}}#cta" style="text-decoration:none">Home</a></li>
      <li>Profil</li>
    </ol>
    <h2>Monografi</h2>

  </div>
</section><!-- End Breadcrumbs -->

<!-- ======= Persyaratan Section ======= -->
<section id="portfolio-details" class="portfolio-details">
    <div class="container mt-1">
        <div class="row mb-1">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingFive">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-bs-toggle="collapse" data-bs-target="#collapseFive"
                                aria-expanded="true" aria-controls="collapseFive">
                                Gambaran Umum
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-bs-parent="#accordion">
                        <div class="card-body">
                            {{-- <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal100">Batas Wilayah</a>
                                    <div class="modal fade" id="exampleModal100" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Batas Wilayah</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    Pasir Impun berdiri sejak tahun 2006 sesuai dengan Peraturan Daerah Kota Bandung Nomor 06 Tahun 2006 Tentang Pemekaran dan Pembentukan Wilayah Kerja Kecamatan Dan Kelurahan Di Lingkungan Pemerintah Kota Bandung.  Kelurahan Pasir Impun terletak pada koordinat 107°41’30” BT-107°40’30” BT dan 6°53’30” LU-6°54’30 LU, dan berada di Timur Laut Kota Bandung dengan jarak 9,6 KM.<br>
                                                    Secara administratif Kelurahan Pasir Impun dibatasi oleh :<br>
                                                    Utara   : Kec. Cimenyan, Kab. Bandung <br>
                                                    Selatan : Kel. Sukamiskin Kec. Arcamanik <br>
                                                    Barat   : Kel. Karang Pamulang <br>
                                                    Timur   : Kel. Sindang Jaya
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal200">Kondisi Geografis</a>
                                    <div class="modal fade" id="exampleModal200" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Kondisi Geografis</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    Secara geografis Kelurahan Pasir Impun Kecamatan Mandalajati memiliki bentuk wilayah datar sebesar 60% dan bentuk berombak sebesar 40% dari total keseluruhan Luas Wilayah. Ditinjau dari sudut ketinggian tanah, Kelurahan Pasir Impun berada pada ketinggian 700 m diatas permukaan air laut. Suhu maksimum dan minimum di Kelurahan Pasir Impun berkisar 19 – 30 oC, sedangkan dilihat dari segi hujan berkisar 2.400 mm/th dan jumlah hari dengan curah hujan yang terbanyak sebesar 90 hari.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal300">Tata Guna Lahan</a>
                                    <div class="modal fade" id="exampleModal300" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Persyaratan</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    Kelurahan Pasir Impun mempunyai luas 84.11 Ha dengan penggunaan areal tanah, sbb:<br>
                                                    1. Tanah sawah 3.0 ha <br>
                                                    2. Tanah Kering 66.1 ha <br>
                                                    3. Tanah Basah 10.0 ha <br>
                                                    4. Fasilitas Umum 5.0 ha
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal400">Luas Wilayah</a>
                                    <div class="modal fade" id="exampleModal400" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Kelurahan Pasir Impun terdiri dari 11 RW dan 57 RT dengan pembagian jumlah RT dan luas wilayah RW, sbb :</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <img src="{{ asset('img/luasperrw.png') }}" class="img-fluid" style="height: 100%">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul> --}}
                            <img src="{{ asset('img/romantis/gambaranumum.jpg') }}" class="img-fluid" style="height: 100%">
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-bs-toggle="collapse" data-bs-target="#collapseOne"
                                aria-expanded="true" aria-controls="collapseOne">
                                Struktur Organisasi
                            </button>
                        </h5>
                    </div>
                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-bs-parent="#accordion">
                        <div class="card-body">
                            <img src="{{ asset('img/romantis/strukturorganisasi.jpg') }}" class="img-fluid" style="height: 100%">
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-bs-toggle="collapse" data-bs-target="#collapseTwo"
                                aria-expanded="false" aria-controls="collapseTwo">
                                Peta Batas Wilayah
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-bs-parent="#accordion">
                        <div class="card-body">
                            <img src="{{ asset('img/romantis/petawilayah.jpg') }}" class="img-fluid" style="height: 100%">

                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-bs-toggle="collapse" data-bs-target="#collapseThree"
                                aria-expanded="true" aria-controls="collapseThree">
                                    Kependudukan
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-bs-parent="#accordion">
                        <div class="card-body">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal1">Jumlah Keluarga Dan Penduduk</a>
                                    <div class="modal fade" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <div class="card text-white" style="background-color: #f55c0a; box-shadow: 0 3px 20px rgba(0, 0, 0, 0.8);
                                                    border-radius: 12px;border-image-width: 10%">
                                                        <div class="card-body">
                                                            <h4>{{ $kk }}</h4>
                                                            <h6>Keluarga</h6>
                                                        </div>
                                                    </div>
                                                    <div class="card text-white" style="background-color: #2d44d7; box-shadow: 0 3px 20px rgba(0, 0, 0, 0.8);
                                                    border-radius: 12px;  border-image-width: 10%">
                                                        <div class="card-body">
                                                            <h4>{{ $penduduk }}</h4>
                                                            <h6>Penduduk</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal2">Jumlah Penduduk Berdasarkan Umur</a>
                                    <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <canvas id="umurAreaChart" style="width: 100%; height: 100%"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal3">Jumlah Penduduk Berdasarkan Pendidikan</a>
                                    <div class="modal fade" id="exampleModal3" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <canvas id="pendidikanAreaChart" style="width: 100%; height: 100%"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal4">Jumlah Penduduk Berdasarkan Agama</a>
                                    <div class="modal fade" id="exampleModal4" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <canvas id="agamaRealAreaChart" style="width: 100%; height: 100%"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal5">Jumlah Penduduk Berdasarkan Pekerjaan</a>
                                    <div class="modal fade" id="exampleModal5" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <canvas id="pekerjaanAreaChart" style="width: 100%; height: 100%"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal6">Jumlah Penduduk Berdasarkan Status Perkawinan</a>
                                    <div class="modal fade" id="exampleModal6" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <div class="col-lg">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <canvas id="perkawinanAreaChart" width="25%" height="25%"></canvas>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal7">Jumlah Penduduk Berdasarkan Domisili</a>
                                    <div class="modal fade" id="exampleModal7" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <div class="col-lg">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <canvas id="domisiliAreaChart" width="25%" height="25%"></canvas>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal8">Jumlah Penduduk Berdasarkan Jenis Kelamin</a>
                                    <div class="modal fade" id="exampleModal8" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <div class="col-lg">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <canvas id="kelaminAreaChart" width="25%" height="25%"></canvas>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal9">Jumlah Penduduk Berdasarkan Jenis Kewarganegaraan</a>
                                    <div class="modal fade" id="exampleModal9" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <div class="col-lg">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <canvas id="warganegaraAreaChart" width="25%" height="25%"></canvas>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingSix">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-bs-toggle="collapse" data-bs-target="#collapseSix"
                                aria-expanded="true" aria-controls="collapseSix">
                                    Potensi Wilayah
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-bs-parent="#accordion">
                        <div class="card-body">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal10">Potensi Perekonomian</a>
                                    <div class="modal fade" id="exampleModal10" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <div class="card text-white" style="background-color: #f05151; box-shadow: 0 3px 20px rgba(0, 0, 0, 0.8);
                                                    border-radius: 12px;border-image-width: 10%">
                                                        <div class="card-body">
                                                            <h4>{{ $umkm }}</h4>
                                                            <h6>UMKM</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal11">Potensi Kesehatan</a>
                                    <div class="modal fade" id="exampleModal11" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <div class="card text-white" style="background-color: #f55c0a; box-shadow: 0 3px 20px rgba(0, 0, 0, 0.8);
                                                    border-radius: 12px;border-image-width: 10%">
                                                        <div class="card-body">
                                                            <h4>{{ $rtod }}</h4>
                                                            <h6>Rumah Tangga OD</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal111">Potensi PBB</a>
                                    <div class="modal fade" id="exampleModal111" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <div class="card text-white" style="background-color: #b80c00; box-shadow: 0 3px 20px rgba(0, 0, 0, 0.8);
                                                    border-radius: 12px;border-image-width: 10%">
                                                        <div class="card-body">
                                                            <h4>{{ $umpi }}</h4>
                                                            <h6>Bumi/Bangunan</h6>
                                                        </div>
                                                    </div>
                                                    <div class="card text-white" style="background-color: #de21a5; box-shadow: 0 3px 20px rgba(0, 0, 0, 0.8);
                                                    border-radius: 12px;border-image-width: 10%">
                                                        <div class="card-body">
                                                            <h4>{{ $nop_erte }}</h4>
                                                            <h6>SPPT PBB</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingFour">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-bs-toggle="collapse" data-bs-target="#collapseFour"
                                aria-expanded="true" aria-controls="collapseFour">
                                Prestasi
                            </button>
                        </h5>
                    </div>
                    {{-- <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-bs-parent="#accordion">
                        <div class="card-body">
                            <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
                                @foreach ($prestasi as $galeri)
                                @if($galeri->image)
                                    <div class="col-lg-4 col-md-6 portfolio-item filter-all">
                                        <div class="portfolio-img"><img src="{{ asset('storage/' .$galeri->image) }}" class="img-fluid" alt=""></div>
                                        <div class="portfolio-info">
                                            <h4>{{$galeri->title}}</h4>
                                            <p>{!!$galeri->excerpt!!}</p>
                                            <a href="{{ asset('storage/' .$galeri->image) }}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="{{$galeri->title}}<br>{{$galeri->publish_at}}"><i class="bx bx-plus"></i></a>
                                            <a href="{{route('romantis.posting.allnews', $galeri->id)}}" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                                        </div>
                                    </div>
                                    @endif
                                @endforeach

                            </div>

                        </div>
                    </div> --}}
                </div>


            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/chart.min.js') }}"></script>
    <script>

        // jumlah penduduk berdasarkan umur
        var ctx = document.getElementById("umurAreaChart");
        var label = @json($chart_umur_label);
        var data_kel = @json($chart_umur_data);
        var mybarChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: label,
                datasets: [
                    {
                        label: "",
                        lineTension: 0.3,
                        backgroundColor: "rgba(200, 0, 255, 1.0)",
                        borderColor: "rgba(200249, 0, 255, 1.0)",
                        pointRadius: 5,
                        pointBackgroundColor: "rgba(200, 0, 255, 1.0)",
                        pointBorderColor: "rgba(200, 0, 255, 1.0)",
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(200, 0, 255, 1.0)",
                        pointHitRadius: 50,
                        pointBorderWidth: 2,
                        data: data_kel,
                        stack: 'stack 1',
                    },
                ],
            },
            options: {
                plugins: {
                    title: {
                        display: true,
                        text: 'Jumlah Penduduk Berdasarkan Umur'
                    },
                },
                responsive: true,
                scales: {
                    x: {
                        stacked: true,
                    },
                    y: {
                        stacked: true
                    }
                }

            }
        });

        // jumlah penduduk berdasarkan jenis kelamin
        var ctx = document.getElementById("kelaminAreaChart");
        var label = @json($chart_jenis_kelamin_label);
        var data = @json($chart_jenis_kelamin_data);
        var mybarChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: label,
                datasets: [{
                    backgroundColor: ['#4dc9f6', '#f67019'],
                    data: data,
                }],
            },
            options: {
                indexAxis: 'y',
                // Elements options apply to all of the options unless overridden in a dataset
                // In this case, we are setting the border of each horizontal bar to be 2px wide
                elements: {
                    bar: {
                        borderWidth: 2,
                    }
                },
                responsive: true,
                plugins: {
                    legend: {
                        position: 'bottom',
                    },
                    title: {
                        display: true,
                        text: 'Jumlah Penduduk Berdasarkan Jenis Kelamin',
                    }
                }
            },
        });

        // jumlah penduduk berdasarkan status pernikahan
        var ctx = document.getElementById("perkawinanAreaChart");
        var label = @json($chart_status_perkawinan_label);
        var data = @json($chart_status_perkawinan_data);
        var mybarChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: label,
                datasets: [{
                    backgroundColor: ['#956ccb', '#77bcca', '#f0a689', '#a19eb4'],
                    data: data,
                }],
            },
            options: {
                indexAxis: 'y',
                // Elements options apply to all of the options unless overridden in a dataset
                // In this case, we are setting the border of each horizontal bar to be 2px wide
                elements: {
                    bar: {
                        borderWidth: 2,
                    }
                },
                responsive: true,
                plugins: {
                    legend: {
                        position: 'bottom',
                    },
                    title: {
                        display: true,
                        text: 'Jumlah Penduduk Berdasarkan Status Perkawinan',
                    }
                }
            },
        });

        // jumlah penduduk berdasarkan kewarganegaraan
        var ctx = document.getElementById("warganegaraAreaChart");
        var label = @json($warganegara);
        var data = @json($jumlahWargaNegara);
        var mybarChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: label,
                datasets: [{
                    backgroundColor: ['#868a0a', '#fbd604'],
                    data: data,
                }],
            },
            options: {
                indexAxis: 'y',
                // Elements options apply to all of the options unless overridden in a dataset
                // In this case, we are setting the border of each horizontal bar to be 2px wide
                elements: {
                    bar: {
                        borderWidth: 2,
                    }
                },
                responsive: true,
                plugins: {
                    legend: {
                        position: 'bottom',
                    },
                    title: {
                        display: true,
                        text: 'Jumlah Penduduk Berdasarkan Kewarganegaraan',
                    }
                }
            },
        });

        // jumlah penduduk berdasarkan domisili
        var ctx = document.getElementById("domisiliAreaChart");
        var label = @json($chart_domisili_label);
        var data = @json($chart_domisili_data);
        var mybarChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: label,
                datasets: [{
                    backgroundColor: ['#8B008B', '#E9967A'],
                    data: data,
                }],
            },
            options: {
                indexAxis: 'y',
                // Elements options apply to all of the options unless overridden in a dataset
                // In this case, we are setting the border of each horizontal bar to be 2px wide
                elements: {
                    bar: {
                        borderWidth: 2,
                    }
                },
                responsive: true,
                plugins: {
                    legend: {
                        position: 'bottom',
                    },
                    title: {
                        display: true,
                        text: 'Jumlah Penduduk Berdasarkan Domisili',
                    }
                }
            },
        });



        // jumlah penduduk berdasarkan agama
        var ctx = document.getElementById("agamaRealAreaChart");
        var label = @json($tittleAgama);
        var data = @json($jumlahWargaAgama);
        var mybarChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: label,
                datasets: [{
                    lineTension: 0.3,
                    borderColor: "rgba(128, 0, 128, 1.0)",
                    pointRadius: 5,
                    pointBackgroundColor: "rgba(128, 0, 128, 1.0)",
                    pointBorderColor: "rgba(128, 0, 128, 1.0)",
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(128, 0, 128, 1.0)",
                    pointHitRadius: 50,
                    pointBorderWidth: 2,
                    pointStyle: 'circle',
                    backgroundColor: "rgba(128, 0, 128, 0.5)",
                    data: data,
                }],
            },
            options: {
                plugins: {
                    title: {
                        display: true,
                        text: 'Jumlah Penduduk Berdasarkan Agama'
                    },
                },
                responsive: true,
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }

            }

        });

        // jumlah penduduk berdasarkan pekerjaan
        var ctx = document.getElementById("pekerjaanAreaChart");
        var label = @json($tittlePekerjaan);
        var data = @json($jumlahWargaPekerjaan);
        var mybarChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: label,
                datasets: [{
                    lineTension: 0.1,
                    lineColor: '#898c94',
                    borderColor: '#3f4146',
                    pointRadius: 5,
                    pointBackgroundColor: '#8f9299',
                    pointBorderColor: '#1d1e20',
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: '#1d1e20',
                    pointHitRadius: 50,
                    pointBorderWidth: 2,
                    pointStyle: 'rectRounded',
                    backgroundColor: '#6b6e76',
                    data: data,
                }],
            },
            options: {
                plugins: {
                    title: {
                        display: true,
                        text: 'Jumlah Penduduk Berdasarkan Pekerjaan'
                    },
                },
                responsive: true,
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }

            }

        });

        // jumlah penduduk berdasarkan pendidikan
        var ctx = document.getElementById("pendidikanAreaChart");
        var label = @json($tittlePendidikan);
        var data = @json($jumlahWargaPendidikan);
        var mybarChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: label,
                datasets: [{
                    lineTension: 0.1,
                    lineColor: "rgba(128, 0, 128, 0.2)",
                    borderColor: "rgba(128, 0, 128, 1.0)",
                    pointRadius: 5,
                    pointBackgroundColor: "rgba(128, 0, 128, 0.3)",
                    pointBorderColor: "rgba(128, 0, 128, 1.0)",
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(128, 0, 128, 1.0)",
                    pointHitRadius: 50,
                    pointBorderWidth: 2,
                    pointStyle: 'rectRounded',
                    backgroundColor: '#eb6070',
                    borderWidth: 2,
                    borderRadius: 50,
                    borderSkipped: false,
                    data: data,
                }],
            },
            options: {
                plugins: {
                    title: {
                        display: true,
                        text: 'Jumlah Penduduk Berdasarkan Pendidikan'
                    },
                },
                responsive: true,
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }

            }

        });
    </script>
@endsection

