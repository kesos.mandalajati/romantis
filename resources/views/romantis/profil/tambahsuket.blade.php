@extends('romantis.layout1')

@section('content')

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container mt-2" >

        <ol >
          <li>Home</li>
          <li>Layanan</li>
          <li><a href="{{route('romantis.profil.simpelonline')}}#services" style="text-decoration:none">Simpel Online</a></li>
        </ol>
        <h2>Buat Surat Keterangan</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Persyaratan Section ======= -->
    <section id="why-us" class="why-us section-bg">
        <div class="container-fluid" data-aos="fade-up">

          <div class="row">
            <div class="col-lg-12 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">

                <div class="content" style="margin-top: -100px">
                    <div class="accordion-list">
                        <ul>
                            <li>
                                <form method="POST" action="{{ route('romantis.simpansuket') }}">
                                    @csrf
                                    <div class="row mb-3">
                                        <div class="col-sm-4" hidden>
                                            <input type="hidden" class="form-control mb-3" id="warga_id" name="warga_id" value="{{ $warga->id }}">
                                        </div>
                                        <div class="col-sm-4" hidden>
                                            <label for="status_verifikasi_suket" class="form-label">Verifikasi Suket</label>
                                            <input type="text" name="status_verifikasi_suket" id="status_verifikasi_suket" class="form-control mb-3" value="{{ 'simpelonline' }}">
                                        </div>
                                        <div class="col-sm-4" hidden>
                                            <label for="kota_kab_id" class="form-label">Kota/Kab</label>
                                            <input type="hidden" class="form-control mb-3" id="kota_kab_id" name="kota_kab_id" value="{{ $warga->relasiWargaKeKeluarga->alamat_kk_kota_kab }}">
                                        </div>
                                        <div class="col-sm-4" hidden>
                                            <label for="kecamatan_id" class="form-label">Kecamatan</label>
                                            <input type="hidden" class="form-control mb-3" id="kecamatan_id" name="kecamatan_id" value="{{ $warga->relasiWargaKeKeluarga->alamat_kk_kecamatan }}">
                                        </div>
                                        <div class="col-sm-4" hidden>
                                            <label for="kelurahan_id" class="form-label">Kelurahan</label>
                                            <input type="hidden" class="form-control mb-3" id="kelurahan_id" name="kelurahan_id" value="{{ $warga->relasiWargaKeKeluarga->alamat_kk_kelurahan }}" class=>
                                        </div>
                                        <div class="col-sm-4" hidden>
                                            <label for="rw_id" class="form-label">RW</label>
                                            <input type="hidden" class="form-control mb-3" id="rw_id" name="rw_id" value="{{ $warga->relasiWargaKeKeluarga->alamat_kk_rw }}">
                                        </div>
                                        <div class="col-sm-4" hidden>
                                            <label for="rt_id" class="form-label">RT</label>
                                            <input type="hidden" class="form-control mb-3" id="rt_id" name="rt_id" value="{{ $warga->relasiWargaKeKeluarga->alamat_kk_rt }}">
                                        </div>
                                        <div class="col-sm-4">
                                            <label for="jenis_suket" class="form-label">Jenis Suket</label>
                                            <select class="form-control mb-3" aria-label="Default select example" id="jenis_suket"
                                                name="jenis_suket">
                                                <option value="Serbaguna" @if(old('jenis_suket') == 'Serbaguna') selected @endif>Surat Keterangan Serbaguna</option>
                                                <option value="Surat Keterangan Usaha" @if(old('jenis_suket') == 'Surat Keterangan Usaha') selected @endif>Surat Keterangan Usaha</option>
                                                <option value="Surat Keterangan Tidak Mampu" @if(old('jenis_suket') == 'Surat Keterangan Tidak Mampu') selected @endif>Surat Keterangan Tidak Mampu</option>
                                                <option value="Surat Keterangan Orang Yang Sama" @if(old('jenis_suket') == 'Surat Keterangan Orang Yang Sama') selected @endif>Surat Keterangan Orang Yang Sama</option>
                                                <option value="Surat Pengantar Perkawinan" @if(old('jenis_suket') == 'Surat Pengantar Perkawinan') selected @endif>Surat Pengantar Perkawinan</option>
                                                <option value="Surat Keterangan Kematian" @if(old('jenis_suket') == 'Surat Keterangan Kematian') selected @endif>Surat Kematian Lebih 60 Hari</option>
                                                <option value="Surat Keterangan Kelahiran" @if(old('jenis_suket') == 'Surat Keterangan Kelahiran') selected @endif>Surat Kelahiran Lebih 30 Hari</option>
                                                <option value="Surat Kelahiran" @if(old('jenis_suket') == 'Surat Kelahiran') selected @endif>Surat Kelahiran</option>
                                                <option value="Surat Kematian" @if(old('jenis_suket') == 'Surat Kematian') selected @endif>Surat Kematian</option>
                                                <option value="Formulir Permohonan Pindah WNI/Keluar" @if(old('jenis_suket') == 'Formulir Permohonan Pindah WNI') selected @endif>Formulir Permohonan Pindah WNI/Keluar</option>
                                                <option value="Surat Keterangan Domisili" @if(old('jenis_suket') == 'Surat Keterangan Domisili') selected @endif>Surat Keterangan Domisili</option>
                                                <option value="Surat Keterangan Domisili Organisasi" @if(old('jenis_suket') == 'Surat Keterangan Domisili Organisasi') selected @endif>Surat Keterangan Domisili Organisasi</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <label for="no_tlp" class="form-label">No Tlp</label>
                                            <input id="no_tlp" type="text" name="no_tlp" placeholder="6287888888888" value="{{ old('no_tlp') }}" class="form-control mb-3">
                                        </div>
                                        <div class="col-sm-4">
                                            <label for="tgl_reg_rt" class="form-label">Tanggal Permohonan</label>
                                            <input type="date" name="tgl_reg_rt" id="tgl_reg_rt" class="form-control mb-3">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <label for="no_reg_rw" class="form-label">No Register RT/RW</label>
                                            <input id="no_reg_rw" type="text" name="no_reg_rw" value="{{ old('no_reg_rw') }}" class="form-control mb-3" placeholder="Silahkan menghubungi RT/RW setempat">
                                        </div>
                                        <div class="col-sm-3">
                                            <label for="tgl_reg_rw" class="form-label">Tanggal Pengantar RT/RW</label>
                                            <input type="date" name="tgl_reg_rw" id="tgl_reg_rw" class="form-control mb-3">
                                        </div>


                                        {{-- <div class="col-sm-6">
                                            <label for="no_reg_kel" class="form-label">No Register</label>
                                            <input id="no_reg_kel" type="text" name="no_reg_kel" value="{{ old('no_reg_kel') }}" class="form-control mb-3">
                                        </div>
                                        <div class="col-sm-3">
                                            <label for="tgl_reg_kel" class="form-label">Tanggal</label>
                                            <input type="date" name="tgl_reg_kel" id="tgl_reg_kel" class="form-control mb-3">
                                        </div> --}}

                                    </div>
                                    <button type="submit" class="btn btn-primary">Buat Surat</button>
                                </form>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End Persyaratan Section -->
@endsection

@section('scripts')
<script>
    $(function() { //jq ready

            // search kota kabupaten by provinsi_id
            $('#alamat_kk_provinsi').blur(function() {
                let _provinsi_id = $('#alamat_kk_provinsi').val();
                let _search = 'provinsi';
                let _url = "{{ url('romantis/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    provinsi_id: _provinsi_id,
                },
                success: function(response) {
                    $('#alamat_kk_kota_kab').empty();

                    response.forEach(element => {
                        $('#alamat_kk_kota_kab').append(
                            `<option value="${element.id}"> ${element.nama_kota_kab} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

            // search kecamatan by kota_kab_id
            $('#alamat_kk_kota_kab').blur(function() {
                let _kota_kab_id = $('#alamat_kk_kota_kab').val();
                let _search = 'kota_kab';
                let _url = "{{ url('romantis/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    kota_kab_id: _kota_kab_id,
                },
                success: function(response) {
                    $('#alamat_kk_kecamatan').empty();

                    response.forEach(element => {
                        $('#alamat_kk_kecamatan').append(
                            `<option value="${element.id}"> ${element.nama_kecamatan} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

            // search kelurahan by kecamatan_id
            $('#alamat_kk_kecamatan').blur(function() {
            let _kecamatan_id = $('#alamat_kk_kecamatan').val();
            let _search = 'kecamatan';
            let _url = "{{ url('romantis/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    kecamatan_id: _kecamatan_id,
                },
                success: function(response) {
                    $('#alamat_kk_kelurahan').empty();

                    response.forEach(element => {
                        $('#alamat_kk_kelurahan').append(
                            `<option value="${element.id}"> ${element.nama_kelurahan} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

             // search rw by kelurahan_id
             $('#alamat_kk_kelurahan').blur(function() {
            let _kelurahan_id = $('#alamat_kk_kelurahan').val();
            let _search = 'kelurahan';
            let _url = "{{ url('romantis/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    kelurahan_id: _kelurahan_id,
                },
                success: function(response) {
                    $('#alamat_kk_rw').empty();

                    response.forEach(element => {
                        $('#alamat_kk_rw').append(
                            `<option value="${element.id}"> ${element.nama_erwe} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

            // search rt by rw_id
            $('#alamat_kk_rw').blur(function() {
            let _rw_id = $('#alamat_kk_rw').val();
            let _search = 'rw';
            let _url = "{{ url('romantis/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    rw_id: _rw_id,
                },
                success: function(response) {
                    $('#alamat_kk_rt').empty();

                    response.forEach(element => {
                        $('#alamat_kk_rt').append(
                            `<option value="${element.id}"> ${element.nama_erte} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

             // search alamat by rt_id
             $('#alamat_kk_rt').blur(function() {
            let _rt_id = $('#alamat_kk_rt').val();
            let _search = 'rt';
            let _url = "{{ url('romantis/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    rt_id: _rt_id,
                },
                success: function(response) {
                    $('#alamat_kk').empty();

                    response.forEach(element => {
                        $('#alamat_kk').append(
                            `<option value="${element.id}"> ${element.nama_alamat} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

        }); //jq close


</script>


@endsection

