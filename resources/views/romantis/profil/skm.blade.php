@extends('romantis.layout1')

@section('content')

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container mt-2" >

        <ol >
          <li><a href="{{route('romantis')}}#services" style="text-decoration:none">Home</a></li>
          <li>Layanan</li>
        </ol>
        <h2>Persyaratan</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Persyaratan Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
        <div class="container" >
            <div class="row">
            <div class="col-lg-6">
                <img src="{{ asset('img/romantis/skm.png') }}" class="img-fluid" style="height: 100%">
            </div>
            <div class="col-lg-6">
                <div class="portfolio-info">
                    <p class="normal" style="normal">
                        mohon luangkan waktu sejenak untuk memberikan umpan balik Anda. Berikut adalah langkah-langkah untuk memberikan umpan balik pelayanan publik:
                    </p>
                    <p class="normal" style="normal">
                        1. Buka aplikasi pemindai kode QR atau barcode di perangkat Anda.<br>
                        2. Aktifkan fitur pemindai barcode di aplikasi tersebut.<br>
                        3. Arahkan kamera perangkat Anda ke barcode atau kode QR yang diberikan. Pastikan Anda memiliki cahaya yang cukup untuk membaca barcode dengan jelas.<br>
                        4. Ikuti petunjuk yang diberikan dalam survei kepuasan. Baca pertanyaan dengan cermat dan pilih jawaban yang paling sesuai dengan pengalaman Anda.<br>
                        5. Setelah selesai mengisi survei, kirimkan jawaban Anda atau ikuti petunjuk lebih lanjut yang tertera dalam aplikasi.<br>
                        6. Terima kasih telah berpartisipasi dalam survei kepuasan masyarakat kami!
                    </p>
                </div>
            </div>
        </div>
    </section><!-- End Persyaratan Section -->


@endsection
@section('scripts')

<script async src="//www.instagram.com/embed.js"></script>

@endsection

