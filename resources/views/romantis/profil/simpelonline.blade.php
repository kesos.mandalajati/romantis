@extends('romantis.layout1')

@section('content')

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container mt-2" >

        <ol >
          <li><a href="{{route('romantis')}}#services" style="text-decoration:none">Home</a></li>
          <li>Layanan</li>
        </ol>
        <h2>Simpel Online</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Persyaratan Section ======= -->
    <section  class="why-us section-bg">
        <div class="container-fluid" data-aos="fade-up">

          <div class="row">
            <div class="col-lg-12 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">

                <div class="content" style="margin-top: -100px">
                    <div class="accordion-list">
                        <ul>

                            <li>
                                <a data-bs-toggle="collapse" data-bs-target="#accordion-list-1" class="collapsed" style="text-decoration:none"><span>01</span> Buat Surat Keterangan Online<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                                <div id="accordion-list-1" class="collapse show" data-bs-parent=".accordion-list">
                                    <div class="row mb-3">
                                        <div class="col-12 mt-5">
                                            <h5>Silahkan Masukan 16 Digit NIK Anda!</h5>
                                            <form action="{{ route('romantis.profil.simpelonline') }}" class="d-flex">
                                                <div class="input-group mb-3">
                                                    <input type="text" class="form-control " maxlength="16" placeholder="Cari NIK" aria-label="Search"
                                                        aria-describedby="Search" name="searchText" value="@if (!empty($searchNik)) {{ $searchNik }} @endif">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-info" type="submit" name="submit" value="searchNik">Cari</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="row">
                                            <div class="col table-responsive">
                                                @isset($warga)
                                                    <table class="table table-bordered align-middle">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">No</th>
                                                                <th scope="col">Nama</th>
                                                                <th scope="col">ALamat</th>
                                                                <th scope="col">Aksi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($warga as $key => $warga)
                                                                <tr>
                                                                    <th scope="row">{{ ++$key }}</th>
                                                                    <td>{{ $warga->nama }} <span style="{{ $warga->status_kematian != null ? 'color:red' : ''  }}">{{ $warga->status_kematian != null ? '(Alm)' : '' }}</span><br>
                                                                    {{ date('M-Y', strtotime($warga->tanggal_lahir)) }}<br></td>
                                                                    1<td><strong>Domisili</strong><br>RT.{{ $warga->relasiWargaKeErte()->get()[0]->nama_erte }} RW.{{ $warga->relasiWargaKeErwe()->get()[0]->nama_erwe }}<br>{{ $warga->relasiWargaKeKelurahan()->get()[0]->nama_kelurahan }}<br> <strong>KK</strong> <br>RT.{{ $warga->relasiWargaKeKeluarga->relasiKeluargaKeErte()->get()[0]->nama_erte }} RW.{{ $warga->relasiWargaKeKeluarga->relasiKeluargaKeErwe()->get()[0]->nama_erwe }}<br>{{ $warga->relasiWargaKeKeluarga->relasiKeluargaKeKelurahan()->get()[0]->nama_kelurahan }}</td>
                                                                    <td>
                                                                        @if ($warga->relasiWargaKeKeluarga->alamat_kk_kelurahan == 12880)
                                                                        <p>SIlahkan Pilih Sesuai Kebutuhan Anda</p>
                                                                        <ul>
                                                                            <a target="_blank" href="{{ route('romantis.tambahsuketserbaguna', 'id=' . $warga->id) }}" style="text-decoration:none">Surat Serbaguna</a>
                                                                            <a target="_blank" href="{{ route('romantis.tambahsuketorangyangsama', 'id=' . $warga->id) }}" style="text-decoration:none">Surat Keterangan Orang Yang Sama</a>
                                                                            <a target="_blank" href="{{ route('romantis.tambahsuketna', 'id=' . $warga->id) }}" style="text-decoration:none">Surat Pengantar Perkawinan</a>
                                                                            <a target="_blank" href="{{ route('romantis.tambahsukettidakmampu', 'id=' . $warga->id) }}" style="text-decoration:none">Surat Keterangan Tidak Mampu</a>
                                                                            <a target="_blank" href="{{ route('romantis.tambahsuketsku', 'id=' . $warga->id) }}">Surat Keterangan Usaha</a>
                                                                            <a target="_blank" href="{{ route('romantis.tambahsuketkematianbaru', 'id=' . $warga->id) }}" style="text-decoration:none">Surat Kematian</a>
                                                                            <a target="_blank" href="{{ route('romantis.tambahsuketkematian', 'id=' . $warga->id) }}" style="text-decoration:none">Surat Keterangan Kematian</a>
                                                                            <a target="_blank" href="{{ route('romantis.tambahsuketkelahiranbaru', 'id=' . $warga->id) }}" style="text-decoration:none">Surat Kelahiran</a>
                                                                            <a target="_blank" href="{{ route('romantis.tambahsuketkelahiran', 'id=' . $warga->id) }}" style="text-decoration:none">Surat Keterangan Kelahiran</a>

                                                                       </ul>
                                                                        @else
                                                                        <p>
                                                                            silahkan hubungi Kelurahan sesuai alamat KK warga yang bersangkutan, atau pindahkan dahulu alamat ybs ke wilayah kelurahan Sukamiskin.</p>
                                                                        <div class="col-sm" style="float: left;">
                                                                            <a href="https://wa.me/628976644554?&text=Dengan hormat, Bapak/Ibu RT/RW/Petugas Pelayanan Romantis, data kependudukan saya {{$warga->nama}}-{{$warga->nik}} sudah terdaftar dalam aplikasi tetapi ada perubahan data sehingga perlu dilakukan penyesuaian. Ijin untuk mengirimkan data untuk perubahannya. Hatur Nuhun. Salam Romantis." target="blank" class="btn btn-sm btn-info"><i class="bi bi-whatsapp"></i></a></div>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                @endisset
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>


                            <li>
                                <a href="{{route('romantis.profil.simpelikes')}}" target="_blank" style="text-decoration:none"><span>02</span> Mengisi Identitas Sendiri </a>
                            </li>

                            <li>
                                <a href="#" target="_blank" style="text-decoration:none"><span>03</span> Upload data </a>
                            </li>


                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End Persyaratan Section -->
@endsection

@section('scripts')
<script>
    $(function() { //jq ready

            // search kota kabupaten by provinsi_id
            $('#alamat_kk_provinsi').blur(function() {
                let _provinsi_id = $('#alamat_kk_provinsi').val();
                let _search = 'provinsi';
                let _url = "{{ url('/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    provinsi_id: _provinsi_id,
                },
                success: function(response) {
                    $('#alamat_kk_kota_kab').empty();

                    response.forEach(element => {
                        $('#alamat_kk_kota_kab').append(
                            `<option value="${element.id}"> ${element.nama_kota_kab} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

            // search kecamatan by kota_kab_id
            $('#alamat_kk_kota_kab').blur(function() {
                let _kota_kab_id = $('#alamat_kk_kota_kab').val();
                let _search = 'kota_kab';
                let _url = "{{ url('/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    kota_kab_id: _kota_kab_id,
                },
                success: function(response) {
                    $('#alamat_kk_kecamatan').empty();

                    response.forEach(element => {
                        $('#alamat_kk_kecamatan').append(
                            `<option value="${element.id}"> ${element.nama_kecamatan} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

            // search kelurahan by kecamatan_id
            $('#alamat_kk_kecamatan').blur(function() {
            let _kecamatan_id = $('#alamat_kk_kecamatan').val();
            let _search = 'kecamatan';
            let _url = "{{ url('/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    kecamatan_id: _kecamatan_id,
                },
                success: function(response) {
                    $('#alamat_kk_kelurahan').empty();

                    response.forEach(element => {
                        $('#alamat_kk_kelurahan').append(
                            `<option value="${element.id}"> ${element.nama_kelurahan} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

             // search rw by kelurahan_id
             $('#alamat_kk_kelurahan').blur(function() {
            let _kelurahan_id = $('#alamat_kk_kelurahan').val();
            let _search = 'kelurahan';
            let _url = "{{ url('/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    kelurahan_id: _kelurahan_id,
                },
                success: function(response) {
                    $('#alamat_kk_rw').empty();

                    response.forEach(element => {
                        $('#alamat_kk_rw').append(
                            `<option value="${element.id}"> ${element.nama_erwe} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

            // search rt by rw_id
            $('#alamat_kk_rw').blur(function() {
            let _rw_id = $('#alamat_kk_rw').val();
            let _search = 'rw';
            let _url = "{{ url('/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    rw_id: _rw_id,
                },
                success: function(response) {
                    $('#alamat_kk_rt').empty();

                    response.forEach(element => {
                        $('#alamat_kk_rt').append(
                            `<option value="${element.id}"> ${element.nama_erte} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

             // search alamat by rt_id
             $('#alamat_kk_rt').blur(function() {
            let _rt_id = $('#alamat_kk_rt').val();
            let _search = 'rt';
            let _url = "{{ url('/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    rt_id: _rt_id,
                },
                success: function(response) {
                    $('#alamat_kk').empty();

                    response.forEach(element => {
                        $('#alamat_kk').append(
                            `<option value="${element.id}"> ${element.nama_alamat} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

        }); //jq close


</script>


@endsection



