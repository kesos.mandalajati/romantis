@extends('romantis.layout1')

@section('content')

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container mt-2" >

        <ol >
          <li>Home</li>
          <li>Layanan</li>
          <li><a href="{{route('romantis.profil.simpelonline')}}#services" style="text-decoration:none">Simpel Online</a></li>
        </ol>
        <h2>Tambah Warga</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Persyaratan Section ======= -->
    <section id="why-us" class="why-us section-bg">
        <div class="container-fluid" data-aos="fade-up">

          <div class="row">
            <div class="col-lg-12 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">

                <div class="content" style="margin-top: -100px">
                    <div class="accordion-list">
                        <ul>
                            <li>
                                <form class="col" action="{{ route('romantis.simpanwarga') }}" method="POST">
                                @csrf

                                    <div class="collapse show" data-bs-parent=".accordion-list">
                                        <div class="row">
                                            <div class="form-group col-sm-4 mb-4" hidden>
                                                <input type="text" class="form-control" id="keluarga_id" name="keluarga_id" value="{{ $keluarga->id }}">
                                            </div>
                                            <div class="form-group col-sm-4 mb-4" hidden>
                                                <label for="alamat_domisili_provinsi">Provinsi</label>
                                                <select class="form-control" aria-label="Default select example" id="alamat_domisili_provinsi" name="alamat_domisili_provinsi" value="{{ old($keluarga->alamat_kk_provinsi) }}">
                                                    <option selected value="">Pilih</option>
                                                    @foreach ($provinsis as $provinsi)
                                                        <option value="{{ $provinsi->id }}" @if ($provinsi->id == $keluarga->alamat_kk_provinsi) selected
                                                        @endif>{{ $provinsi->nama_provinsi }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-4 mb-4" hidden>
                                                <label for="alamat_domisili_kota_kab">Kab/Kota</label>
                                                <select class="form-control" aria-label="Default select example" id="alamat_domisili_kota_kab" name="alamat_domisili_kota_kab" value="{{ old($keluarga->alamat_kk_kota_kab) }}">
                                                    <option selected value="{{ $keluarga->alamat_kk_kota_kab }}">{{ $keluarga->relasiKeluargaKeKotaKab()->get()[0]->nama_kota_kab }}</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-4 mb-4" hidden>
                                                <label for="alamat_domisili_kecamatan">Kecamatan</label>
                                                <select class="form-control" aria-label="Default select example" id="alamat_domisili_kecamatan" name="alamat_domisili_kecamatan" value="{{ old($keluarga->alamat_kk_kecamatan) }}">
                                                    <option selected value="{{ $keluarga->alamat_kk_kecamatan }}">{{ $keluarga->relasiKeluargaKeKecamatan()->get()[0]->nama_kecamatan }}</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-4 mb-4" hidden>
                                                <label for="alamat_domisili_kelurahan">Kelurahan</label>
                                                <select class="form-control" aria-label="Default select example" id="alamat_domisili_kelurahan" name="alamat_domisili_kelurahan" value="{{ old($keluarga->alamat_kk_kelurahan) }}">
                                                    <option selected value="{{ $keluarga->alamat_kk_kelurahan }}">{{ $keluarga->relasiKeluargaKeKelurahan()->get()[0]->nama_kelurahan }}</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-4 mb-4" hidden>
                                                <label for="alamat_domisili_rw">RW</label>
                                                <select class="form-control" aria-label="Default select example" id="alamat_domisili_rw" name="alamat_domisili_rw" value="{{ old($keluarga->alamat_kk_rw) }}">
                                                    <option selected value="{{ $keluarga->alamat_kk_rw }}">{{ $keluarga->relasiKeluargaKeErwe()->get()[0]->nama_erwe }}</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-4 mb-4" hidden>
                                                <label for="alamat_domisili_rt">RT</label>
                                                <select class="form-control" aria-label="Default select example" id="alamat_domisili_rt" name="alamat_domisili_rt" value="{{ old($keluarga->alamat_kk_rt) }}">
                                                    <option selected value="{{ $keluarga->alamat_kk_rt }}">{{ $keluarga->relasiKeluargaKeErte()->get()[0]->nama_erte }}</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-12 mb-4" hidden>
                                                <label for="alamat_domisili">Alamat Domisili</label>
                                                <select class="form-control" aria-label="Default select example" id="alamat_domisili" name="alamat_domisili" value="{{ old($keluarga->alamat_kk) }}">
                                                    <option selected value="{{ $keluarga->alamat_kk }}">{{ $keluarga->relasiKeluargaKeAlamat()->get()[0]->nama_alamat }}</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-4 mb-4">
                                                <label for="nik">NIK</label>
                                                <input type="text" class="form-control @error('nik') is-invalid @enderror" id="nik" name="nik" maxlength="16" value="{{ old('nik') }}">
                                            </div>
                                            <div class="form-group col-sm-4 mb-4">
                                                <label for="nama">Nama</label>
                                                <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama') }}">
                                            </div>
                                            <div class="form-group col-sm-4 mb-4">
                                                <label for="jenis_kelamin">Jenis Kelamin</label>
                                                <select class="form-control" aria-label="Default select example" id="jenis_kelamin" name="jenis_kelamin">
                                                    <option value="" @if(old('jenis_kelamin')) selected @endif>Pilih</option>
                                                    <option value="LAKI-LAKI"  @if(old('jenis_kelamin')) selected @endif>LAKI-LAKI</option>
                                                    <option value="PEREMPUAN" @if(old('jenis_kelamin')) selected @endif>PEREMPUAN</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-4 mb-4">
                                                <label for="tempat_lahir">Tempat Lahir</label>
                                                <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" value="{{ old('tempat_lahir') }}">
                                            </div>
                                            <div class="form-group col-sm-4 mb-4">
                                                <label for="tanggal_lahir">Tanggal Lahir</label>
                                                <input type="date" class="form-control" id="tanggal_lahir" name="tanggal_lahir" value="{{ date('d-m-Y', strtotime(old('tanggal_lahir'))) }}">
                                            </div>
                                            <div class="form-group col-sm-4 mb-4">
                                                <label for="sdhrt">SDHRT</label>
                                                <select class="form-control" aria-label="Default select example" id="sdhrt" name="sdhrt">
                                                    <option selected value="">Pilih</option>
                                                    @foreach ($sdhrts as $sdhrt)
                                                        <option value="{{ $sdhrt->id }}">{{ $sdhrt->nama_sdhrt }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-4 mb-4">
                                                <label for="pendidikan">Pendidikan</label>
                                                <select class="form-control" aria-label="Default select example" id="pendidikan" name="pendidikan">
                                                    <option selected value="">Pilih</option>
                                                    @foreach ($pendidikans as $pendidikan)
                                                        <option value="{{ $pendidikan->id }}">{{ $pendidikan->nama_pendidikan }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-4 mb-4">
                                                <label for="pekerjaan">Pekerjaan</label>
                                                <select class="form-control" aria-label="Default select example" id="pekerjaan" name="pekerjaan">
                                                    <option selected value="">Pilih</option>
                                                    @foreach ($pekerjaans as $pekerjaan)
                                                        <option value="{{ $pekerjaan->id }}">{{ $pekerjaan->nama_pekerjaan }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-4 mb-4">
                                                <label for="agama">Agama</label>
                                                <select class="form-control" aria-label="Default select example" id="agama" name="agama">
                                                    <option selected value="">Pilih</option>
                                                    @foreach ($agamas as $agama)
                                                        <option value="{{ $agama->id }}">{{ $agama->nama_agama }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-4 mb-4">
                                                <label for="status_perkawinan">Status Perkawinan</label>
                                                <select class="form-control" aria-label="Default select example" id="status_perkawinan" name="status_perkawinan">
                                                    <option selected value="">Pilih</option>
                                                    <option value="BELUM KAWIN">BELUM KAWIN</option>
                                                    <option value="CERAI HIDUP">CERAI HIDUP</option>
                                                    <option value="CERAI MATI">CERAI MATI</option>
                                                    <option value="KAWIN">KAWIN</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-4 mb-4">
                                                <label for="kewarganegaraan">Kewarganegaraan</label>
                                                <select class="form-control" aria-label="Default select example" id="kewarganegaraan" name="kewarganegaraan">
                                                    <option selected value="">Pilih</option>
                                                    <option value="WNI">WNI</option>
                                                    <option value="WNA">WNA</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-4 mb-4">
                                                <label for="golongan_darah">Golongan Darah</label>
                                                <select class="form-control" aria-label="Default select example" id="golongan_darah" name="golongan_darah">
                                                    <option value="" selected>Pilih</option>
                                                    <option value="A">A</option>
                                                    <option value="B">B</option>
                                                    <option value="AB">AB</option>
                                                    <option value="O">O</option>
                                                    <option value="Tidak Tahu">TIDAK TAHU</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-12">
                                                <button type="submit" class="btn btn-info">Tambah Warga</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End Persyaratan Section -->
@endsection

@section('scripts')
<script>
    $(function() { //jq ready

            // search kota kabupaten by provinsi_id
            $('#alamat_kk_provinsi').blur(function() {
                let _provinsi_id = $('#alamat_kk_provinsi').val();
                let _search = 'provinsi';
                let _url = "{{ url('romantis/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    provinsi_id: _provinsi_id,
                },
                success: function(response) {
                    $('#alamat_kk_kota_kab').empty();

                    response.forEach(element => {
                        $('#alamat_kk_kota_kab').append(
                            `<option value="${element.id}"> ${element.nama_kota_kab} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

            // search kecamatan by kota_kab_id
            $('#alamat_kk_kota_kab').blur(function() {
                let _kota_kab_id = $('#alamat_kk_kota_kab').val();
                let _search = 'kota_kab';
                let _url = "{{ url('romantis/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    kota_kab_id: _kota_kab_id,
                },
                success: function(response) {
                    $('#alamat_kk_kecamatan').empty();

                    response.forEach(element => {
                        $('#alamat_kk_kecamatan').append(
                            `<option value="${element.id}"> ${element.nama_kecamatan} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

            // search kelurahan by kecamatan_id
            $('#alamat_kk_kecamatan').blur(function() {
            let _kecamatan_id = $('#alamat_kk_kecamatan').val();
            let _search = 'kecamatan';
            let _url = "{{ url('romantis/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    kecamatan_id: _kecamatan_id,
                },
                success: function(response) {
                    $('#alamat_kk_kelurahan').empty();

                    response.forEach(element => {
                        $('#alamat_kk_kelurahan').append(
                            `<option value="${element.id}"> ${element.nama_kelurahan} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

             // search rw by kelurahan_id
             $('#alamat_kk_kelurahan').blur(function() {
            let _kelurahan_id = $('#alamat_kk_kelurahan').val();
            let _search = 'kelurahan';
            let _url = "{{ url('romantis/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    kelurahan_id: _kelurahan_id,
                },
                success: function(response) {
                    $('#alamat_kk_rw').empty();

                    response.forEach(element => {
                        $('#alamat_kk_rw').append(
                            `<option value="${element.id}"> ${element.nama_erwe} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

            // search rt by rw_id
            $('#alamat_kk_rw').blur(function() {
            let _rw_id = $('#alamat_kk_rw').val();
            let _search = 'rw';
            let _url = "{{ url('romantis/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    rw_id: _rw_id,
                },
                success: function(response) {
                    $('#alamat_kk_rt').empty();

                    response.forEach(element => {
                        $('#alamat_kk_rt').append(
                            `<option value="${element.id}"> ${element.nama_erte} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

             // search alamat by rt_id
             $('#alamat_kk_rt').blur(function() {
            let _rt_id = $('#alamat_kk_rt').val();
            let _search = 'rt';
            let _url = "{{ url('romantis/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    rt_id: _rt_id,
                },
                success: function(response) {
                    $('#alamat_kk').empty();

                    response.forEach(element => {
                        $('#alamat_kk').append(
                            `<option value="${element.id}"> ${element.nama_alamat} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

        }); //jq close


</script>


@endsection

