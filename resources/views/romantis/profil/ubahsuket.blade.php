@extends('romantis.layout1')

@section('content')

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container mt-2" >

        <ol >
          <li>Home</li>
          <li>Layanan</li>
          <li><a href="{{route('romantis.profil.simpelonline')}}#services" style="text-decoration:none">Simpel Online</a></li>
        </ol>
        <h2>Cek Surat Keterangan</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Persyaratan Section ======= -->
    <section id="why-us" class="why-us section-bg">
        <div class="container-fluid" data-aos="fade-up">

          <div class="row">
            <div class="col-lg-12 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">

                <div class="content" style="margin-top: -100px">
                    <div class="accordion-list">
                        <ul>
                            <li>
                                <form method="POST" action="{{ route('romantis.perbaharuisuket', $suket->id) }}">
                                    @csrf @method('PUT')
                                    <div class="btn-group" role="group" style="float: start;">
                                        <button type="submit" class="btn btn-secondary btn-sm">kirim pesan atau print review</button>
                                        <a href="https://wa.me/628976644554?&text=Dengan hormat, Bapak/Ibu RT/RW/Petugas Pelayanan Romantis, saya telah membuat permohonan surat keterangan online yaitu {{ $suket->jenis_suket }} atas nama {{$suket->relasiSuketKeWarga->nama}} dengan nomor register RT/RW {{$suket->no_reg_rw}} pada tanggal {{date('d-m-Y', strtotime($suket->tgl_reg_rw))}}. Saya sudah menyiapkan berkas yang diperlukan. Hatur Nuhun. Salam Romantis." target="blank" class="btn btn-sm btn-warning" ><i class="bi bi-whatsapp"></i></a>

                                        @if($suket->jenis_suket == "Surat Keterangan Orang Yang Sama")
                                            <a href="{{ route('romantis.printorangyangsama', $suket->id) }}" class="btn btn-info btn-sm" ><i class="bi bi-printer"></i></a>

                                        @elseif($suket->jenis_suket == "Surat Pengantar Perkawinan")
                                            <a href="{{ route('romantis.printna', $suket->id) }}" class="btn btn-info btn-sm" ><i class="bi bi-printer"></i></a>

                                        @elseif($suket->jenis_suket == "Surat Keterangan Domisili Organisasi")
                                            <a href="{{ route('romantis.printdomisiliorganisasi', $suket->id) }}" class="btn btn-info btn-sm" ><i class="bi bi-printer"></i></a>

                                        @elseif($suket->jenis_suket == "Formulir Permohonan Pindah WNI/Keluar")
                                            <a href="{{ route('romantis.printpindahkeluar', $suket->id) }}" class="btn btn-info btn-sm" ><i class="bi bi-printer"></i></a>

                                        @elseif($suket->jenis_suket == "Surat Keterangan Kelahiran")
                                            <a href="{{ route('romantis.printkelahiran', $suket->id) }}" class="btn btn-info btn-sm" ><i class="bi bi-printer"></i></a>

                                        @elseif ($suket->jenis_suket == "Surat Keterangan Kematian")
                                            <a href="{{ route('romantis.printkematian', $suket->id) }}" class="btn btn-info btn-sm" ><i class="bi bi-printer"></i></a>

                                        @elseif ($suket->jenis_suket == "Surat Kelahiran")
                                            <a href="{{ route('romantis.printkelahiranbaru', $suket->id) }}" class="btn btn-info btn-sm" ><i class="bi bi-printer"></i></a>

                                        @elseif ($suket->jenis_suket == "Surat Kematian")
                                            <a href="{{ route('romantis.printkematianbaru', $suket->id) }}" class="btn btn-info btn-sm" ><i class="bi bi-printer"></i></a>

                                        @elseif ($suket->jenis_suket == "Surat Keterangan Usaha")
                                            <a href="{{ route('romantis.printsku', $suket->id) }}" class="btn btn-info btn-sm" ><i class="bi bi-printer"></i></a>

                                        @else
                                            <a href="{{ route('romantis.printserbaguna', $suket->id) }}" class="btn btn-info btn-sm" ><i class="bi bi-printer"></i></a>

                                        @endif
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-sm-4" hidden>
                                            <input type="hidden" class="form-control" id="warga_id" name="warga_id" value="{{ $suket->warga_id }}">
                                        </div>
                                        <div class="col-sm-4" hidden>
                                            <strong><label for="status_verifikasi_suket" class="form-label">Verifikasi Suket</label></strong>
                                            <input type="text" name="status_verifikasi_suket" id="status_verifikasi_suket" class="form-control" value="{{ 'simpelonline' }}">
                                        </div>
                                        <div class="col-sm-4">
                                            <strong><label for="jenis_suket" class="form-label">Jenis Suket</label></strong>
                                            <select class="form-control" aria-label="Default select example" id="jenis_suket"
                                                name="jenis_suket">
                                                <option value="Serbaguna" @if($suket->jenis_suket == 'Serbaguna') selected @endif>Surat Keterangan Serbaguna</option><option value="Surat Keterangan Usaha" @if($suket->jenis_suket == 'Surat Keterangan Usaha') selected @endif>Surat Keterangan Usaha</option>
                                                <option value="Surat Keterangan Orang Yang Sama" @if($suket->jenis_suket == 'Surat Keterangan Orang Yang Sama') selected @endif>Surat Keterangan Orang Yang Sama</option>
                                                <option value="Surat Pengantar Perkawinan" @if($suket->jenis_suket == 'Surat Pengantar Perkawinan') selected @endif>Surat Pengantar Perkawinan</option>
                                                <option value="Surat Keterangan Kematian" @if($suket->jenis_suket == 'Surat Keterangan Kematian') selected @endif>Surat Kematian Lebih 60 Hari</option>
                                                <option value="Surat Keterangan Kelahiran" @if($suket->jenis_suket == 'Surat Keterangan Kelahiran') selected @endif>Surat Kelahiran Lebih 30 Hari</option>
                                                <option value="Surat Kelahiran" @if($suket->jenis_suket == 'Surat Kelahiran') selected @endif>Surat Kelahiran</option>
                                                <option value="Surat Kematian" @if($suket->jenis_suket == 'Surat Kematian') selected @endif>Surat Kematian</option>
                                                <option value="Formulir Permohonan Pindah WNI/Keluar" @if($suket->jenis_suket == 'Formulir Permohonan Pindah WNI') selected @endif>Formulir Permohonan Pindah WNI/Keluar</option>
                                                <option value="Surat Keterangan Domisili Organisasi" @if($suket->jenis_suket == 'Surat Keterangan Domisili Organisasi') selected @endif>Surat Keterangan Domisili Organisasi</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <strong><label for="no_reg_rw" class="form-label">No Register RT/RW</label></strong>
                                            <input id="no_reg_rw" type="text" name="no_reg_rw" value="{{ $suket->no_reg_rw}}" class="form-control" placeholder="(no urut)/RT.05/RW.02/(bulan)/2023">
                                        </div>
                                        <div class="col-sm-2">
                                            <strong><label for="tgl_reg_rw" class="form-label">Tanggal</label></strong>
                                            <input type="date" name="tgl_reg_rw" id="tgl_reg_rw" class="form-control" value="{{ $suket->tgl_reg_rw }}">
                                        </div>
                                        <div class="col-sm-3">
                                            <strong><label for="no_tlp" class="form-label">No Tlp</label></strong>
                                            <input id="no_tlp" type="text" name="no_tlp" placeholder="6287888888888" value="{{ $suket->no_tlp }}" class="form-control">
                                        </div>
                                        {{-- <div class="col-sm-3">
                                            <strong><label for="no_reg_kel" class="form-label">No Register Kelurahan</label></strong>
                                            <input id="no_reg_kel" type="text" name="no_reg_kel" value="{{ $suket->no_reg_kel}}" class="form-control" placeholder="(no urut)/RW.02/(bulan)/2023">
                                        </div> --}}
                                        {{-- <div class="col-sm-2">
                                            <strong><label for="tgl_reg_kel" class="form-label">Tanggal</label></strong>
                                            <input type="date" name="tgl_reg_kel" id="tgl_reg_kel" class="form-control" value="{{ $suket->tgl_reg_kel }}">
                                        </div> --}}
                                        @if ($suket->jenis_suket == "Surat Keterangan Kematian")
                                            <div class="col-sm-3">
                                                <strong><label for="pemohon" class="form-label">Nama Pemohon</label></strong>
                                                <input id="pemohon" type="text" name="pemohon" value="{{ $suket->pemohon }}" class="form-control">
                                            </div>
                                            <div class="col-sm-3">
                                                <strong><label for="hubungan" class="form-label">Hubungan Dgn Alm</label></strong>
                                                <input id="hubungan" type="text" name="hubungan" value="{{ $suket->hubungan }}" class="form-control">
                                            </div>
                                            <div class="col-sm-12">
                                                <strong><label for="maksud_suket" class="form-label">melengkapi persyaratan...</label></strong>
                                                <input id="maksud_suket" type="text" name="maksud_suket" value="{{ $suket->maksud_suket }}" class="form-control">
                                            </div>
                                        @endif

                                        @if ($suket->jenis_suket == "Surat Kematian")
                                            <div class="col-sm-3">
                                                <strong><label for="tempat" class="form-label">Tempat Kematian</label></strong>
                                                <input id="tempat" type="text" name="tempat" value="{{ $suket->tempat }}" class="form-control" >
                                            </div>
                                            <div class="col-sm-3">
                                                <strong><label for="sebab" class="form-label">Sebab Kematian</label></strong>
                                                <input id="sebab" type="text" name="sebab" value="{{ $suket->sebab }}" class="form-control" >
                                            </div>
                                        @endif

                                        @if ($suket->jenis_suket == "Surat Kelahiran")
                                            <div class="col-sm-2">
                                                <strong><label for="waktu_lahir" class="form-label">Waktu</label></strong>
                                                <input id="waktu_lahir" type="time" name="waktu_lahir" value="{{ $suket->waktu_lahir }}" class="form-control">
                                            </div>
                                            <div class="col-sm-3">
                                                <strong><label for="nama_ibu" class="form-label">Nama Ibu</label></strong>
                                                <input id="nama_ibu" type="text" name="nama_ibu" value="{{ $suket->nama_ibu }}" class="form-control">
                                            </div>
                                            <div class="col-sm-3">
                                                <strong><label for="nama_ayah" class="form-label">Nama Ayah</label></strong>
                                                <input id="nama_ayah" type="text" name="nama_ayah" value="{{ $suket->nama_ayah }}" class="form-control">
                                            </div>
                                            <div class="col-sm-3">
                                                <strong><label for="tempat_nikah" class="form-label">Tempat Nikah</label></strong>
                                                <input id="tempat_nikah" type="text" name="tempat_nikah" value="{{ $suket->tempat_nikah }}" class="form-control">
                                            </div>
                                            <div class="col-sm-3">
                                                <strong><label for="tgl_nikah" class="form-label">Tanggal Nikah</label></strong>
                                                <input id="tgl_nikah" type="date" name="tgl_nikah" value="{{ $suket->tgl_nikah }}" class="form-control">
                                            </div>
                                            <div class="col-sm-3">
                                                <strong><label for="buku_nikah" class="form-label">No Buku Nikah</label></strong>
                                                <input id="buku_nikah" type="text" name="buku_nikah" value="{{ $suket->buku_nikah }}" class="form-control">
                                            </div>
                                        @endif

                                        @if ($suket->jenis_suket == "Surat Keterangan Kelahiran")
                                            <div class="col-sm-2">
                                                <strong><label for="waktu_lahir" class="form-label">Waktu</label></strong>
                                                <input id="waktu_lahir" type="time" name="waktu_lahir" value="{{ $suket->waktu_lahir }}" class="form-control">
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <strong><label for="tempat_nikah" class="form-label">Tempat Nikah</label></strong>
                                                    <input id="tempat_nikah" type="text" name="tempat_nikah" value="{{ $suket->tempat_nikah }}" class="form-control">
                                                </div>
                                                <div class="col-sm-2">
                                                    <strong><label for="tgl_nikah" class="form-label">Tanggal Nikah</label></strong>
                                                    <input id="tgl_nikah" type="date" name="tgl_nikah" value="{{ $suket->tgl_nikah }}" class="form-control">
                                                </div>
                                                <div class="col-sm-3">
                                                    <strong><label for="buku_nikah" class="form-label">No Buku Nikah</label></strong>
                                                    <input id="buku_nikah" type="text" name="buku_nikah" value="{{ $suket->buku_nikah }}" class="form-control">
                                                </div>
                                                <div class="col-sm-3">
                                                    <strong><label for="pemohon" class="form-label">Nama Pemohon</label></strong>
                                                    <input id="pemohon" type="text" name="pemohon" value="{{ $suket->pemohon }}" class="form-control">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <strong><label for="nik_ibu" class="form-label">NIK Ibu</label></strong>
                                                    <input id="nik_ibu" type="text" name="nik_ibu" value="{{ $suket->nik_ibu }}" class="form-control">
                                                </div>
                                                <div class="col-sm-3">
                                                    <strong><label for="no_kk_ibu" class="form-label">No KK Ibu</label></strong>
                                                    <input id="no_kk_ibu" type="text" name="no_kk_ibu" value="{{ $suket->no_kk_ibu }}" class="form-control">
                                                </div>
                                                <div class="col-sm-3">
                                                    <strong><label for="nama_ibu" class="form-label">Nama Ibu</label></strong>
                                                    <input id="nama_ibu" type="text" name="nama_ibu" value="{{ $suket->nama_ibu }}" class="form-control">
                                                </div>
                                                <div class="col-sm-3">
                                                    <strong><label for="ttl_ibu" class="form-label">TTL Ibu</label></strong>
                                                    <input id="ttl_ibu" type="date" name="ttl_ibu" value="{{ $suket->ttl_ibu }}" class="form-control">
                                                </div>
                                                <div class="col-sm-3">
                                                    <strong><label for="tempat_lahir_ibu" class="form-label">Tempat Lahir Ibu</label></strong>
                                                    <input id="tempat_lahir_ibu" type="text" name="tempat_lahir_ibu" value="{{ $suket->tempat_lahir_ibu }}" class="form-control">
                                                </div>
                                                <div class="col-sm-3">
                                                    <strong><label for="pekerjaan_ibu" class="form-label">Pekerjaan Ibu</label></strong>
                                                    <input id="pekerjaan_ibu" type="text" name="pekerjaan_ibu" value="{{ $suket->pekerjaan_ibu }}" class="form-control">
                                                </div>
                                                <div class="col-sm-3">
                                                    <strong><label for="alamat_ibu" class="form-label">Alamat Ibu</label></strong>
                                                    <input id="alamat_ibu" type="text" name="alamat_ibu" value="{{ $suket->alamat_ibu }}" class="form-control">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <strong><label for="nik_ayah" class="form-label">NIK ayah</label></strong>
                                                    <input id="nik_ayah" type="text" name="nik_ayah" value="{{ $suket->nik_ayah }}" class="form-control">
                                                </div>
                                                <div class="col-sm-3">
                                                    <strong><label for="no_kk_ayah" class="form-label">No KK Ayah</label></strong>
                                                    <input id="no_kk_ayah" type="text" name="no_kk_ayah" value="{{ $suket->no_kk_ayah }}" class="form-control">
                                                </div>
                                                <div class="col-sm-3">
                                                    <strong><label for="nama_ayah" class="form-label">Nama ayah</label></strong>
                                                    <input id="nama_ayah" type="text" name="nama_ayah" value="{{ $suket->nama_ayah }}" class="form-control">
                                                </div>
                                                <div class="col-sm-3">
                                                    <strong><label for="ttl_ayah" class="form-label">TTL ayah</label></strong>
                                                    <input id="ttl_ayah" type="date" name="ttl_ayah" value="{{ $suket->ttl_ayah }}" class="form-control">
                                                </div>
                                                <div class="col-sm-3">
                                                    <strong><label for="tempat_lahir_ayah" class="form-label">Tempat Lahir Ayah</label></strong>
                                                    <input id="tempat_lahir_ayah" type="text" name="tempat_lahir_ayah" value="{{ $suket->tempat_lahir_ayah }}" class="form-control">
                                                </div>
                                                <div class="col-sm-3">
                                                    <strong><label for="pekerjaan_ayah" class="form-label">Pekerjaan Ayah</label></strong>
                                                    <input id="pekerjaan_ayah" type="text" name="pekerjaan_ayah" value="{{ $suket->pekerjaan_ayah }}" class="form-control">
                                                </div>
                                                <div class="col-sm-3">
                                                    <strong><label for="alamat_ayah" class="form-label">Alamat ayah</label></strong>
                                                    <input id="alamat_ayah" type="text" name="alamat_ayah" value="{{ $suket->alamat_ayah }}" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <strong><label for="maksud_suket" class="form-label">melengkapi persyaratan...</label></strong>
                                                <input id="maksud_suket" type="text" name="maksud_suket" value="{{ $suket->maksud_suket }}" class="form-control">
                                            </div>
                                        @endif

                                        @if ($suket->jenis_suket == "Surat Pengantar Perkawinan")
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <strong><label for="status_perkawinan_pasangan" class="form-label">Status Perkawinan Calon Pasangan</label></strong>
                                                <select class="form-control" aria-label="Default select example" id="status_perkawinan_pasangan" name="status_perkawinan_pasangan">
                                                    <option  @if ($suket->status_perkawinan_pasangan == '') selected
                                                        @endif value="">Pilih</option>
                                                    <option @if ($suket->status_perkawinan_pasangan == 'BELUM KAWIN') selected
                                                        @endif value="BELUM KAWIN">BELUM KAWIN</option>
                                                    <option @if ($suket->status_perkawinan_pasangan == 'CERAI HIDUP') selected
                                                        @endif value="CERAI HIDUP">CERAI HIDUP</option>
                                                    <option @if ($suket->status_perkawinan_pasangan == 'CERAI MATI') selected
                                                        @endif value="CERAI MATI">CERAI MATI</option>
                                                    <option @if ($suket->status_perkawinan_pasangan == 'KAWIN') selected
                                                        @endif value="KAWIN">KAWIN</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <strong><label for="nama_pasangan" class="form-label">Nama Suami/Istri Dahulu</label></strong>
                                                <input id="nama_pasangan" type="text" name="nama_pasangan" value="{{ $suket->nama_pasangan }}" class="form-control">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-3">
                                                <strong><label for="nik_ibu" class="form-label">NIK Ibu</label></strong>
                                                <input id="nik_ibu" type="text" name="nik_ibu" value="{{ $suket->nik_ibu }}" class="form-control">
                                            </div>
                                            <div class="col-sm-3">
                                                <strong><label for="nama_ibu" class="form-label">Nama Ibu</label></strong>
                                                <input id="nama_ibu" type="text" name="nama_ibu" value="{{ $suket->nama_ibu }}" class="form-control">
                                            </div>
                                            <div class="col-sm-3">
                                                <strong><label for="tempat_lahir_ibu" class="form-label">Tempat Lahir Ibu</label></strong>
                                                <input id="tempat_lahir_ibu" type="text" name="tempat_lahir_ibu" value="{{ $suket->tempat_lahir_ibu }}" class="form-control">
                                            </div>
                                            <div class="col-sm-3">
                                                <strong><label for="ttl_ibu" class="form-label">TTL Ibu</label></strong>
                                                <input id="ttl_ibu" type="date" name="ttl_ibu" value="{{ $suket->ttl_ibu }}" class="form-control">
                                            </div>
                                            <div class="col-sm-3">
                                                <strong><label for="pekerjaan_ibu" class="form-label">Pekerjaan Ibu</label></strong>
                                                <input id="pekerjaan_ibu" type="text" name="pekerjaan_ibu" value="{{ $suket->pekerjaan_ibu }}" class="form-control">
                                            </div>
                                            <div class="col-sm-3">
                                                <strong><label for="kewarganegaraan_ibu" class="form-label">Kewarganegaraan Ibu</label></strong>
                                                <select class="form-control" aria-label="Default select example" id="kewarganegaraan_ibu"  name="kewarganegaraan_ibu">
                                                    <option selected value="" @if($suket->kewarganegaraan_ibu == '') selected @endif>Pilih</option>
                                                    <option value="WNI" @if($suket->kewarganegaraan_ibu == 'WNI') selected @endif>WNI</option>
                                                    <option value="WNA" @if($suket->kewarganegaraan_ibu == 'WNA') selected @endif>WNA</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-3">
                                                <strong><label for="agama_ibu" class="form-label">Agama Ibu</label></strong>
                                                <select class="form-control" aria-label="Default select example" id="agama_ibu"  name="agama_ibu">
                                                    <option selected value="" @if($suket->agama_ibu == '') selected @endif>Pilih</option>
                                                    <option value="ISLAM" @if($suket->agama_ibu == 'ISLAM') selected @endif>ISLAM</option>
                                                    <option value="KATOLIK" @if($suket->agama_ibu == 'KATOLIK') selected @endif>KATOLIK</option>
                                                    <option value="KRISTEN" @if($suket->agama_ibu == 'KRISTEN') selected @endif>KRISTEN</option>
                                                    <option value="BUDHA" @if($suket->agama_ibu == 'BUDHA') selected @endif>BUDHA</option>
                                                    <option value="HINDU" @if($suket->agama_ibu == 'HINDU') selected @endif>HINDU</option>
                                                    <option value="KONGHUCU" @if($suket->agama_ibu == 'KONGHUCU') selected @endif>KONGHUCU</option>
                                                    <option value="KEPERCAYAAN KEPADA TUHAN YME" @if($suket->agama_ibu == 'KEPERCAYAAN KEPADA TUHAN YME') selected @endif>KEPERCAYAAN KEPADA TUHAN YME</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-3">
                                                <strong><label for="alamat_ibu" class="form-label">Alamat Ibu</label></strong>
                                                <input id="alamat_ibu" type="text" name="alamat_ibu" value="{{ $suket->alamat_ibu }}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <strong><label for="nik_ayah" class="form-label">NIK ayah</label></strong>
                                                <input id="nik_ayah" type="text" name="nik_ayah" value="{{ $suket->nik_ayah }}" class="form-control">
                                            </div>
                                            <div class="col-sm-3">
                                                <strong><label for="nama_ayah" class="form-label">Nama ayah</label></strong>
                                                <input id="nama_ayah" type="text" name="nama_ayah" value="{{ $suket->nama_ayah }}" class="form-control">
                                            </div>
                                            <div class="col-sm-3">
                                                <strong><label for="tempat_lahir_ayah" class="form-label">Tempat Lahir Ayah</label></strong>
                                                <input id="tempat_lahir_ayah" type="text" name="tempat_lahir_ayah" value="{{ $suket->tempat_lahir_ayah }}" class="form-control">
                                            </div>
                                            <div class="col-sm-3">
                                                <strong><label for="ttl_ayah" class="form-label">TTL ayah</label></strong>
                                                <input id="ttl_ayah" type="date" name="ttl_ayah" value="{{ $suket->ttl_ayah }}" class="form-control">
                                            </div>
                                            <div class="col-sm-3">
                                                <strong><label for="pekerjaan_ayah" class="form-label">Pekerjaan Ayah</label></strong>
                                                <input id="pekerjaan_ayah" type="text" name="pekerjaan_ayah" value="{{ $suket->pekerjaan_ayah }}" class="form-control">
                                            </div>
                                            <div class="col-sm-3">
                                                <strong><label for="kewarganegaraan_ayah" class="form-label">Kewarganegaraan Ayah</label></strong>
                                                <select class="form-control" aria-label="Default select example" id="kewarganegaraan_ayah"  name="kewarganegaraan_ayah">
                                                    <option selected value="" @if($suket->kewarganegaraan_ayah == '') selected @endif>Pilih</option>
                                                    <option value="WNI" @if($suket->kewarganegaraan_ayah == 'WNI') selected @endif>WNI</option>
                                                    <option value="WNA" @if($suket->kewarganegaraan_ayah == 'WNA') selected @endif>WNA</option>
                                                </select>
                                                {{-- <input id="kewarganegaraan_ayah" type="text" name="kewarganegaraan_ayah" value="{{ $suket->kewarganegaraan_ayah }}" class="form-control"> --}}
                                            </div>
                                            <div class="col-sm-3">
                                                <strong><label for="agama_ayah" class="form-label">Agama Ayah</label></strong>
                                                <select class="form-control" aria-label="Default select example" id="agama_ayah"  name="agama_ayah">
                                                    <option value="ISLAM" @if($suket->agama_ayah == '') selected @endif>PILIH</option>
                                                    <option value="ISLAM" @if($suket->agama_ayah == 'ISLAM') selected @endif>ISLAM</option>
                                                    <option value="KATOLIK" @if($suket->agama_ayah == 'KATOLIK') selected @endif>KATOLIK</option>
                                                    <option value="KRISTEN" @if($suket->agama_ayah == 'KRISTEN') selected @endif>KRISTEN</option>
                                                    <option value="BUDHA" @if($suket->agama_ayah == 'BUDHA') selected @endif>BUDHA</option>
                                                    <option value="HINDU" @if($suket->agama_ayah == 'HINDU') selected @endif>HINDU</option>
                                                    <option value="KONGHUCU" @if($suket->agama_ayah == 'KONGHUCU') selected @endif>KONGHUCU</option>
                                                    <option value="KEPERCAYAAN KEPADA TUHAN YME" @if($suket->agama_ayah == 'KEPERCAYAAN KEPADA TUHAN YME') selected @endif>KEPERCAYAAN KEPADA TUHAN YME</option>
                                                </select>
                                                {{-- <input id="agama_ayah" type="text" name="agama_ayah" value="{{ $suket->agama_ayah }}" class="form-control"> --}}
                                            </div>
                                            <div class="col-sm-3">
                                                <strong><label for="alamat_ayah" class="form-label">Alamat ayah</label></strong>
                                                <input id="alamat_ayah" type="text" name="alamat_ayah" value="{{ $suket->alamat_ayah }}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <strong><label for="maksud_suket" class="form-label">melengkapi persyaratan...</label></strong>
                                            <input id="maksud_suket" type="text" name="maksud_suket" value="{{ $suket->maksud_suket }}" class="form-control">
                                        </div>
                                    @endif

                                        @if ($suket->jenis_suket == "Surat Keterangan Orang Yang Sama")
                                            <div class="col-sm-12">
                                                <strong><label for="catatan" class="form-label">bahwa...</label></strong>
                                                <input id="catatan" type="hidden" name="catatan" value="{{ $suket->catatan }}" class="form-control" >
                                                <trix-editor input="catatan"></trix-editor>
                                            </div>
                                            <div class="col-sm-12">
                                                <strong><label for="maksud_suket" class="form-label">melengkapi persyaratan...</label></strong>
                                                <input id="maksud_suket" type="text" name="maksud_suket" value="{{ $suket->maksud_suket }}" class="form-control">
                                            </div>
                                        @endif
                                        @if ($suket->jenis_suket == "Serbaguna")
                                            <div class="col-sm-12">
                                                <strong><label for="catatan" class="form-label">bahwa...</label></strong>
                                                <input id="catatan" type="text" name="catatan" value="{{ $suket->catatan }}" class="form-control" >
                                            </div>
                                            <div class="col-sm-12">
                                                <strong><label for="maksud_suket" class="form-label">melengkapi persyaratan...</label></strong>
                                                <input id="maksud_suket" type="text" name="maksud_suket" value="{{ $suket->maksud_suket }}" class="form-control">
                                            </div>
                                        @endif
                                        @if ($suket->jenis_suket == "Surat Keterangan Usaha")
                                            <div class="col-sm-12">
                                                <strong><label for="catatan" class="form-label">bahwa...</label></strong>
                                                <input id="catatan" type="text" name="catatan" value="{{ $suket->catatan }}" class="form-control" >
                                            </div>
                                            <div class="col-sm-12">
                                                <strong><label for="maksud_suket" class="form-label">melengkapi persyaratan...</label></strong>
                                                <input id="maksud_suket" type="text" name="maksud_suket" value="{{ $suket->maksud_suket }}" class="form-control">
                                            </div>
                                        @endif
                                    </div>
                                </form>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End Persyaratan Section -->
@endsection

@section('scripts')
<script>
    $(function() { //jq ready

            // search kota kabupaten by provinsi_id
            $('#alamat_kk_provinsi').blur(function() {
                let _provinsi_id = $('#alamat_kk_provinsi').val();
                let _search = 'provinsi';
                let _url = "{{ url('romantis/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    provinsi_id: _provinsi_id,
                },
                success: function(response) {
                    $('#alamat_kk_kota_kab').empty();

                    response.forEach(element => {
                        $('#alamat_kk_kota_kab').append(
                            `<option value="${element.id}"> ${element.nama_kota_kab} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

            // search kecamatan by kota_kab_id
            $('#alamat_kk_kota_kab').blur(function() {
                let _kota_kab_id = $('#alamat_kk_kota_kab').val();
                let _search = 'kota_kab';
                let _url = "{{ url('romantis/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    kota_kab_id: _kota_kab_id,
                },
                success: function(response) {
                    $('#alamat_kk_kecamatan').empty();

                    response.forEach(element => {
                        $('#alamat_kk_kecamatan').append(
                            `<option value="${element.id}"> ${element.nama_kecamatan} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

            // search kelurahan by kecamatan_id
            $('#alamat_kk_kecamatan').blur(function() {
            let _kecamatan_id = $('#alamat_kk_kecamatan').val();
            let _search = 'kecamatan';
            let _url = "{{ url('romantis/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    kecamatan_id: _kecamatan_id,
                },
                success: function(response) {
                    $('#alamat_kk_kelurahan').empty();

                    response.forEach(element => {
                        $('#alamat_kk_kelurahan').append(
                            `<option value="${element.id}"> ${element.nama_kelurahan} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

             // search rw by kelurahan_id
             $('#alamat_kk_kelurahan').blur(function() {
            let _kelurahan_id = $('#alamat_kk_kelurahan').val();
            let _search = 'kelurahan';
            let _url = "{{ url('romantis/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    kelurahan_id: _kelurahan_id,
                },
                success: function(response) {
                    $('#alamat_kk_rw').empty();

                    response.forEach(element => {
                        $('#alamat_kk_rw').append(
                            `<option value="${element.id}"> ${element.nama_erwe} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

            // search rt by rw_id
            $('#alamat_kk_rw').blur(function() {
            let _rw_id = $('#alamat_kk_rw').val();
            let _search = 'rw';
            let _url = "{{ url('romantis/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    rw_id: _rw_id,
                },
                success: function(response) {
                    $('#alamat_kk_rt').empty();

                    response.forEach(element => {
                        $('#alamat_kk_rt').append(
                            `<option value="${element.id}"> ${element.nama_erte} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

             // search alamat by rt_id
             $('#alamat_kk_rt').blur(function() {
            let _rt_id = $('#alamat_kk_rt').val();
            let _search = 'rt';
            let _url = "{{ url('romantis/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    rt_id: _rt_id,
                },
                success: function(response) {
                    $('#alamat_kk').empty();

                    response.forEach(element => {
                        $('#alamat_kk').append(
                            `<option value="${element.id}"> ${element.nama_alamat} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

        }); //jq close


</script>


@endsection

