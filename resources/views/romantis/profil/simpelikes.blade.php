@extends('romantis.layout1')

@section('content')

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container mt-2" >

        <ol >
          <li><a href="{{route('romantis')}}#cta" style="text-decoration:none">Home</a></li>
          <li>Layanan</li>
        </ol>
        <h2>Simpelikes</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Persyaratan Section ======= -->
    <section  class="why-us section-bg">
        <div class="container-fluid" data-aos="fade-up">

          <div class="row">
            <div class="col-lg-12 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">

                <div class="content" style="margin-top: -100px">
                    <div class="accordion-list">
                        <ul>
                            <li>
                                <a data-bs-toggle="collapse" data-bs-target="#accordion-list-1" class="collapsed" style="text-decoration:none"><span>01</span> Kebijakan Privasi <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                                <div id="accordion-list-1" class="collapse" data-bs-parent=".accordion-list">
                                <p>
                                    Kebijakan Privasi berikut ini menjelaskan bagaimana kami mengumpulkan, menggunakan, memindahkan, melayani dan melindungi informasi pribadi anda yang dapat diidentifikasi yang diperoleh melalui Website atau Aplikasi kami (sebagaimana didefinisikan di bawah). Mohon anda membaca Kebijakan Privasi ini dengan seksama untuk memastikan bahwa anda memahami bagaimana ketentuan Kebijakan Privasi ini kami berlakukan.
                                </p>
                                <p>
                                    Penggunaan anda atas aplikasi dan layanan kami tunduk pada Ketentuan Penggunaan dan Kebijakan Privasi ini dan mengindikasikan persetujuan anda terhadap Ketentuan Penggunaan dan Kebijakan Privasi tersebut.
                                </p>
                                <p>
                                    Kami mengumpulkan Informasi Pribadi tertentu dari anda agar Aplikasi dapat menemukan Layanan dari Penyedia Layanan. Anda akan langsung memberikan Informasi Pribadi (sebagai contoh, saat anda mendaftar) dan beberapa informasi akan secara otomatis dikumpulkan ketika anda menggunakan Aplikasi.
                                </p>
                                <p>
                                    Kami menggunakan email, nama, nomor telepon, sandi akun, dan data pribadi anda untuk memverifikasi kepemilikan anda atas suatu akun agar kami dapat dengan mudah dapat melayani anda, untuk berkomunikasi dengan anda sehubungan dengan keluhan/laporan anda dan untuk memberikan anda informasi mengenai status permohonan anda. Kami juga dapat menggunakan nama, email, dan nomor telepon anda untuk mengirimkan pesan, pembaharuan yang bersifat umum atas website atau status pengajuan dokumen anda.
                                </p>
                                <p>
                                    Dengan mengunjungi website ini, anda mengakui bahwa anda telah membaca dan memahami Kebijakan Privasi ini dan Ketentuan Penggunaan dan setuju dan sepakat terhadap penggunaan, praktek, pemrosesan dan pengalihan informasi pribadi anda oleh kami sebagaimana dinyatakan di dalam Kebijakan Privasi ini.
                                </p>
                                <p>
                                    Anda juga menyatakan bahwa anda memiliki hak untuk membagikan seluruh informasi yang telah anda berikan kepada kami dan untuk memberikan hak kepada kami untuk menggunakan dan membagikan informasi tersebut kepada Penyedia Layanan.
                                </p>
                                </div>
                            </li>

                            <li>
                                <a href="{{route('romantis.tambahalamat')}}" style="text-decoration:none; " ><span>02</span> Tambah Alamat <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                                <div id="accordion-list-2" class="collapse" data-bs-parent=".accordion-list">
                            </li>

                            <li>
                                <a data-bs-toggle="collapse" data-bs-target="#accordion-list-3" class="collapsed" style="text-decoration:none"><span>03</span> Cek KK & Tambah NIK <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                                <div id="accordion-list-3" class="collapse show" data-bs-parent=".accordion-list">
                                    <div class="row mt-5">
                                        <div class="col-12">
                                            <h6>Silahkan Masukan Nomor Kartu Keluarga!</h6>
                                            <form action="{{ route('romantis.profil.simpelikes') }}" class="d-flex">
                                                <div class="input-group mb-3">
                                                    <input type="text" class="form-control" placeholder="Cari Keluarga" aria-label="Search" maxlength="16"
                                                        aria-describedby="Search" name="searchText" value="@if (!empty($search)) {{ $search }} @endif">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-info" type="submit" name="submit" value="search">Cari</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 table-responsive">
                                                @isset($keluarga)
                                                    <table class="table table-bordered align-middle">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">No</th>
                                                                {{-- <th scope="col">No KK</th>
                                                                <th scope="col">ALamat KK</th> --}}
                                                                <th scope="col">RT</th>
                                                                <th scope="col">RW</th>
                                                                <th scope="col">Kelurahan</th>
                                                                <th scope="col">Anggota Keluarga</th>
                                                                <th scope="col">Aksi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($keluarga as $key => $keluarga)
                                                                <tr>
                                                                    <th scope="row">{{ ++$key }}</th>
                                                                    {{-- <td>{{ $keluarga->no_kk }}</td>
                                                                    <td>{{ $keluarga->alamat_kk }}</td> --}}
                                                                    <td>{{ $keluarga->relasiKeluargaKeErte()->get()[0]->nama_erte }}</td>
                                                                    <td>{{ $keluarga->relasiKeluargaKeErwe()->get()[0]->nama_erwe }}</td>
                                                                    <td>{{ $keluarga->relasiKeluargaKeKelurahan()->get()[0]->nama_kelurahan }}</td>
                                                                    <td>
                                                                        @foreach ($keluarga->relasiKeluargaKeWarga as $item)
                                                                            {{ $item->nama }} <br>
                                                                        @endforeach
                                                                    </td>
                                                                    <td>
                                                                        <div class="btn-group" role="group">
                                                                            <a target="_blank" href="{{ route('romantis.tambahwarga', 'id=' . $keluarga->id) }}"
                                                                                class="btn btn-info">Tambah Warga</a>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                @endisset
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <a data-bs-toggle="collapse" data-bs-target="#accordion-list-4" class="collapsed" style="text-decoration:none"><span>04</span> Tambah KK <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                                <div id="accordion-list-4" class="collapse" data-bs-parent=".accordion-list">
                                    <form class="col" action="{{ route('romantis.tambahkeluarga') }}" method="POST">
                                        @csrf
                                        <div class="accordion mt-4" id="accordionExample">
                                            <div class="row mb-3">
                                                <div class="form-group col-md-4" hidden>
                                                    <label for="alamat_kk_provinsi">Provinsi</label>
                                                    <select class="form-control" aria-label="Default select example" id="alamat_kk_provinsi" name="alamat_kk_provinsi">
                                                        <option selected value="">Pilih</option>
                                                        @foreach ($provinsis as $provinsis)
                                                            <option value="{{ $provinsis->id }}">{{ $provinsis->nama_provinsi }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4" hidden>
                                                    <label for="alamat_kk_kota_kab">Kota/Kab</label>
                                                    <select class="form-control" aria-label="Default select example" id="alamat_kk_kota_kab" name="kota_kab_id" list="alamat_kk_kota_kab" >
                                                        <option selected value=""></option>
                                                        @foreach ($kota_kabs as $kota_kabs)
                                                            <option value="{{ $kota_kabs->id }}">{{ $kota_kabs->nama_kota_kab }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4" hidden>
                                                    <label for="alamat_kk_kecamatan">Kecamatan</label>
                                                    <select class="form-control" aria-label="Default select example" id="alamat_kk_kecamatan" name="kecamatan_id" list="alamat_kk_kecamatan" >
                                                        <option selected value=""></option>
                                                        @foreach ($kecamatans as $kecamatans)
                                                            <option value="{{ $kecamatans->id }}">{{ $kecamatans->nama_kecamatan }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="alamat_kk_kelurahan">Kelurahan</label>
                                                    <select class="form-control" aria-label="Default select example" id="alamat_kk_kelurahan" name="kelurahan_id" list="alamat_kk_kelurahan" >
                                                        @foreach ($keluarahans as $keluarahans)
                                                            <option value="{{ $keluarahans->id }}" selected>{{ $keluarahans->nama_kelurahan }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <label for="alamat_kk_rw">RW</label>
                                                    <select class="form-control" aria-label="Default select example" id="alamat_kk_rw" name="alamat_kk_rw">
                                                        <option selected value="">Pilih</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <label for="alamat_kk_rt">RT</label>
                                                    <select class="form-control" aria-label="Default select example" id="alamat_kk_rt" name="alamat_kk_rt">
                                                        <option selected value="">Pilih</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="alamat_kk">Alamat</label>
                                                    <select class="form-control" aria-label="Default select example" id="alamat_kk" name="alamat_kk">
                                                        <option selected value="">Pilih</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4 mt-3">
                                                    <label for="no_kk">Masukan No KK</label>
                                                    <input type="text" class="form-control @error('no_kk') is-invalid @enderror" id="no_kk" name="no_kk" maxlength="16" value="@if (!empty($search)) {{ $search }} @endif">
                                                </div>
                                                <div class="form-group col-md-4 mt-3">
                                                    <label for="status_verifikasi_keluarga">Tgl di KK</label>
                                                    <input type="date" class="form-control" id="status_verifikasi_keluarga" name="status_verifikasi_keluarga" value="{{ date('d-m-Y', strtotime(old('status_verifikasi_keluarga'))) }}">
                                                </div>
                                            </div>
                                            <div class="row mt-3">
                                                <div class="form-group col-sm-12">
                                                    <button type="submit" class="btn btn-info">Tambah Keluarga</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </li>

                            <li>
                                <a href="{{route('romantis.profil.simpelonline')}}" style="text-decoration:none; " ><span>05</span> Buat Surat Keterangan Online <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                                <div id="accordion-list-2" class="collapse" data-bs-parent=".accordion-list">
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End Persyaratan Section -->
@endsection

@section('scripts')
<script>
    $(function() { //jq ready

            // search kota kabupaten by provinsi_id
            $('#alamat_kk_provinsi').blur(function() {
                let _provinsi_id = $('#alamat_kk_provinsi').val();
                let _search = 'provinsi';
                let _url = "{{ url('/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    provinsi_id: _provinsi_id,
                },
                success: function(response) {
                    $('#alamat_kk_kota_kab').empty();

                    response.forEach(element => {
                        $('#alamat_kk_kota_kab').append(
                            `<option value="${element.id}"> ${element.nama_kota_kab} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

            // search kecamatan by kota_kab_id
            $('#alamat_kk_kota_kab').blur(function() {
                let _kota_kab_id = $('#alamat_kk_kota_kab').val();
                let _search = 'kota_kab';
                let _url = "{{ url('/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    kota_kab_id: _kota_kab_id,
                },
                success: function(response) {
                    $('#alamat_kk_kecamatan').empty();

                    response.forEach(element => {
                        $('#alamat_kk_kecamatan').append(
                            `<option value="${element.id}"> ${element.nama_kecamatan} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

            // search kelurahan by kecamatan_id
            $('#alamat_kk_kecamatan').blur(function() {
            let _kecamatan_id = $('#alamat_kk_kecamatan').val();
            let _search = 'kecamatan';
            let _url = "{{ url('/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    kecamatan_id: _kecamatan_id,
                },
                success: function(response) {
                    $('#alamat_kk_kelurahan').empty();

                    response.forEach(element => {
                        $('#alamat_kk_kelurahan').append(
                            `<option value="${element.id}"> ${element.nama_kelurahan} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

             // search rw by kelurahan_id
             $('#alamat_kk_kelurahan').blur(function() {
            let _kelurahan_id = $('#alamat_kk_kelurahan').val();
            let _search = 'kelurahan';
            let _url = "{{ url('/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    kelurahan_id: _kelurahan_id,
                },
                success: function(response) {
                    $('#alamat_kk_rw').empty();

                    response.forEach(element => {
                        $('#alamat_kk_rw').append(
                            `<option value="${element.id}"> ${element.nama_erwe} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

            // search rt by rw_id
            $('#alamat_kk_rw').blur(function() {
            let _rw_id = $('#alamat_kk_rw').val();
            let _search = 'rw';
            let _url = "{{ url('/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    rw_id: _rw_id,
                },
                success: function(response) {
                    $('#alamat_kk_rt').empty();

                    response.forEach(element => {
                        $('#alamat_kk_rt').append(
                            `<option value="${element.id}"> ${element.nama_erte} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

             // search alamat by rt_id
             $('#alamat_kk_rt').blur(function() {
            let _rt_id = $('#alamat_kk_rt').val();
            let _search = 'rt';
            let _url = "{{ url('/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    rt_id: _rt_id,
                },
                success: function(response) {
                    $('#alamat_kk').empty();

                    response.forEach(element => {
                        $('#alamat_kk').append(
                            `<option value="${element.id}"> ${element.nama_alamat} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

        }); //jq close


</script>


@endsection

