@extends('romantis.layout1')

@section('content')

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container mt-2" >

        <ol >
          <li>Home</li>
          <li>Layanan</li>
          <li><a href="{{route('romantis.profil.simpelikes')}}#cto" style="text-decoration:none">Simpelikes</a></li>
        </ol>
        <h2>Tambah Alamat</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Persyaratan Section ======= -->
    <section id="why-us" class="why-us section-bg">
        <div class="container-fluid" data-aos="fade-up">

          <div class="row">
            <div class="col-lg-12 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">

                <div class="content" style="margin-top: -100px">
                    <div class="accordion-list">
                        <ul>
                            <li>
                                <form class="col" action="{{ route('romantis.simpanalamat') }}" method="POST">
                                    @csrf
                                    <div class="accordion mt-4" id="accordionExample">
                                        <div class="row mb-3">
                                            <div class="form-group col-md-4" hidden>
                                                <label for="alamat_kk_provinsi">Provinsi</label>
                                                <select class="form-control" aria-label="Default select example" id="alamat_kk_provinsi" name="alamat_kk_provinsi">
                                                    <option selected value="">Pilih</option>
                                                    @foreach ($provinsis as $provinsis)
                                                        <option value="{{ $provinsis->id }}">{{ $provinsis->nama_provinsi }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-md-4" hidden>
                                                <label for="alamat_kk_kota_kab">Kota/Kab</label>
                                                <select class="form-control" aria-label="Default select example" id="alamat_kk_kota_kab" name="kota_kab_id" list="alamat_kk_kota_kab" >
                                                    <option selected value=""></option>
                                                    @foreach ($kota_kabs as $kota_kabs)
                                                        <option value="{{ $kota_kabs->id }}">{{ $kota_kabs->nama_kota_kab }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-md-4" hidden>
                                                <label for="alamat_kk_kecamatan">Kecamatan</label>
                                                <select class="form-control" aria-label="Default select example" id="alamat_kk_kecamatan" name="kecamatan_id" list="alamat_kk_kecamatan" >
                                                    <option selected value=""></option>
                                                    @foreach ($kecamatans as $kecamatans)
                                                        <option value="{{ $kecamatans->id }}">{{ $kecamatans->nama_kecamatan }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="alamat_kk_kelurahan">Kelurahan</label>
                                                <select class="form-control" aria-label="Default select example" id="alamat_kk_kelurahan" name="kelurahan_id" list="alamat_kk_kelurahan" >
                                                    @foreach ($keluarahan as $keluarahan)
                                                        <option value="{{ $keluarahan->id }}" selected>{{ $keluarahan->nama_kelurahan }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <label for="alamat_kk_rw">RW</label>
                                                <select class="form-control" aria-label="Default select example" id="alamat_kk_rw" name="alamat_kk_rw">
                                                    <option selected value="">Pilih</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <label for="alamat_kk_rt">RT</label>
                                                <select class="form-control" aria-label="Default select example" id="alamat_kk_rt" name="alamat_kk_rt">
                                                    <option selected value="">Pilih</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="alamat_kk">Alamat</label>
                                                <select class="form-control" aria-label="Default select example" id="alamat_kk" name="alamat_kk">
                                                    <option selected value="">Pilih</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-4 mt-3">
                                                <label for="nama_alamat">Masukan Alamat</label>
                                                <input type="text" class="form-control id="nama_alamat" name="nama_alamat"  value="{{old('nama_alamat')}}">
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="form-group col-sm-12">
                                                <button type="submit" class="btn btn-info">Tambah Alamat</button>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </form>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End Persyaratan Section -->
@endsection

@section('scripts')
<script>
    $(function() { //jq ready

            // search kota kabupaten by provinsi_id
            $('#alamat_kk_provinsi').blur(function() {
                let _provinsi_id = $('#alamat_kk_provinsi').val();
                let _search = 'provinsi';
                let _url = "{{ url('/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    provinsi_id: _provinsi_id,
                },
                success: function(response) {
                    $('#alamat_kk_kota_kab').empty();

                    response.forEach(element => {
                        $('#alamat_kk_kota_kab').append(
                            `<option value="${element.id}"> ${element.nama_kota_kab} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

            // search kecamatan by kota_kab_id
            $('#alamat_kk_kota_kab').blur(function() {
                let _kota_kab_id = $('#alamat_kk_kota_kab').val();
                let _search = 'kota_kab';
                let _url = "{{ url('/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    kota_kab_id: _kota_kab_id,
                },
                success: function(response) {
                    $('#alamat_kk_kecamatan').empty();

                    response.forEach(element => {
                        $('#alamat_kk_kecamatan').append(
                            `<option value="${element.id}"> ${element.nama_kecamatan} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

            // search kelurahan by kecamatan_id
            $('#alamat_kk_kecamatan').blur(function() {
            let _kecamatan_id = $('#alamat_kk_kecamatan').val();
            let _search = 'kecamatan';
            let _url = "{{ url('/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    kecamatan_id: _kecamatan_id,
                },
                success: function(response) {
                    $('#alamat_kk_kelurahan').empty();

                    response.forEach(element => {
                        $('#alamat_kk_kelurahan').append(
                            `<option value="${element.id}"> ${element.nama_kelurahan} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

             // search rw by kelurahan_id
             $('#alamat_kk_kelurahan').blur(function() {
            let _kelurahan_id = $('#alamat_kk_kelurahan').val();
            let _search = 'kelurahan';
            let _url = "{{ url('/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    kelurahan_id: _kelurahan_id,
                },
                success: function(response) {
                    $('#alamat_kk_rw').empty();

                    response.forEach(element => {
                        $('#alamat_kk_rw').append(
                            `<option value="${element.id}"> ${element.nama_erwe} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

            // search rt by rw_id
            $('#alamat_kk_rw').blur(function() {
            let _rw_id = $('#alamat_kk_rw').val();
            let _search = 'rw';
            let _url = "{{ url('/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    rw_id: _rw_id,
                },
                success: function(response) {
                    $('#alamat_kk_rt').empty();

                    response.forEach(element => {
                        $('#alamat_kk_rt').append(
                            `<option value="${element.id}"> ${element.nama_erte} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

             // search alamat by rt_id
             $('#alamat_kk_rt').blur(function() {
            let _rt_id = $('#alamat_kk_rt').val();
            let _search = 'rt';
            let _url = "{{ url('/searchlokasi') }}";

            $.ajax({
                url: _url,
                type: 'GET',
                data: {
                    search: _search,
                    rt_id: _rt_id,
                },
                success: function(response) {
                    $('#alamat_kk').empty();

                    response.forEach(element => {
                        $('#alamat_kk').append(
                            `<option value="${element.id}"> ${element.nama_alamat} </option>`
                        );
                    });
                },
                error: function(response) {},
                complete: function() {}
                });
            });

        }); //jq close


</script>


@endsection

