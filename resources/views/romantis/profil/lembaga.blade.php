@extends('siimpun.layout1')

@section('content')
    <div class="container mt-1">
        <div class="row mb-1">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-bs-toggle="collapse" data-bs-target="#collapseOne"
                                aria-expanded="true" aria-controls="collapseOne">
                                Ketua RW
                            </button>
                        </h5>
                    </div>
                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-bs-parent="#accordion">
                        <div class="card-body">







                            
                            <img src="{{ asset('img/rwpasim.png') }}" class="img-fluid" style="height: 100%">
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h5 class="mb-0 text-justify">
                            <button class="btn btn-link collapsed" data-bs-toggle="collapse" data-bs-target="#collapseTwo"
                                aria-expanded="false" aria-controls="collapseTwo">
                                Lembaga Kemasyarakatan Kelurahan
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-bs-parent="#accordion">
                        <div class="card-body">
                            <img src="{{ asset('img/lkkpasim.png') }}" class="img-fluid" style="height: 100%">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

