@extends('siimpun.layout1')

@section('content')
    <div class="container mt-1">
        <div class="row mb-1">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-bs-toggle="collapse" data-bs-target="#collapseOne"
                                aria-expanded="true" aria-controls="collapseOne">
                                Pengertian
                            </button>
                        </h5>
                    </div>
                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-bs-parent="#accordion">
                        <div class="card-body">

                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-bs-toggle="collapse" data-bs-target="#collapseTwo"
                                aria-expanded="false" aria-controls="collapseTwo">
                                Gambaran PPKS
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-bs-parent="#accordion">
                        <div class="card-body">


                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-bs-toggle="collapse" data-bs-target="#collapseThree"
                                aria-expanded="true" aria-controls="collapseThree">
                                    Gambaran PSKS
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-bs-parent="#accordion">
                        <div class="card-body">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal1">Kader RBM</a>
                                    <div class="modal fade" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Rodiah</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <img src="{{ asset('img/rbm.png') }}" class="img-fluid" style="height: 100%">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal2">Kader WPA</a>
                                    <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Persyaratan</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <img src="{{ asset('img/rbm.png') }}" class="img-fluid" style="height: 100%">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal3">Tim Pendamping Keluarga</a>
                                    <div class="modal fade" id="exampleModal3" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Persyaratan</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">




                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal4">Satgas Verivali</a>
                                    <div class="modal fade" id="exampleModal4" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Persyaratan</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <img src="{{ asset('img/rbm.png') }}" class="img-fluid" style="height: 100%">
                                                    <img src="{{ asset('img/rbm.png') }}" class="img-fluid" style="height: 100%">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal5">Sahabat Lansia</a>
                                    <div class="modal fade" id="exampleModal5" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Persyaratan</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <img src="{{ asset('img/rbm.png') }}" class="img-fluid" style="height: 100%">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal6">KK Hilang</a>
                                    <div class="modal fade" id="exampleModal6" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Persyaratan</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    1. SURAT PENGANTAR RT/RW;<br>
                                                    2. FOTOCOPY KTP DAN KK;<br>
                                                    3. FOTOCOPY SURAT NIKAH;<br>
                                                    4. FORMULIR PERMOHONAN KARTU KELUARGA;<br>
                                                    5. FOTOCOPY SURAT KETERANGAN KEHILANGAN DARI KEPOLISIAN;<br>
                                                    6. SEMUA BERKAS DIBUAT 2 RANGKAP DAN DIMASUKAN DALAM MAP BIRU.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal7">Surat Pengantar Pindah Keluar</a>
                                    <div class="modal fade" id="exampleModal7" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Persyaratan</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    1. SURAT PENGANTAR RT/RW;<br>
                                                    2. FORMULIR PINDAH KELUAR;<br>
                                                    3. KTP ASLI;<br>
                                                    4. KK ASLI (JIKA SATU KELUARGA PINDAH);<br>
                                                    5. FOTOCOPY AKTA CERAI (JIKA PINDAH KARENA CERAI);<br>
                                                    6. SEMUA BERKAS DIBUAT 2 RANGKAP DAN DIMASUKAN DALAM MAP BIRU.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal8">Surat Pengantar Kelahiran</a>
                                    <div class="modal fade" id="exampleModal8" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Persyaratan</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    1. SURAT PENGANTAR RT/RW;<br>
                                                    2. FOTOCOPY KTP DAN KK ORANG TUA;<br>
                                                    3. FOTOCOPY SURAT NIKAH ORANG TUA;<br>
                                                    4. FOTOCOPY KETERANGAN LAHIR DARI BIDAN/RUMAH SAKIT;<br>
                                                    5. SURAT PERNYATAAN (JIKA TIDAK ADA POIN KE 2 DAN 3);<br>
                                                    6. SEMUA BERKAS DIBUAT 2 RANGKAP DAN DIMASUKAN DALAM MAP BIRU.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal9">Surat Pengantar Kematian</a>
                                    <div class="modal fade" id="exampleModal9" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Persyaratan</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    1. SURAT PENGANTAR RT/RW;<br>
                                                    2. SURAT KET. KEMATIAN DARI RUMAH SAKIT/DOKTER (APABILA MENINGGAL DI RUMAH SAKIT);<br>
                                                    3. FOTOCOPY KTP DAN KK;<br>
                                                    4. MAP BIRU
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal10">Pengantar Nikah</a>
                                    <div class="modal fade" id="exampleModal0" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Persyaratan</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    1. SURAT PENGANTAR RT/RW;<br>
                                                    2. SURAT PERNYATAAN NUMPANG AKAD;<br>
                                                    3. FOTOCOPY KTP DAN KK ORANG TUA DAN BERSANGKUTAN;<br>
                                                    4. FOTOCOPY AKTA CERAI/ AKTA KEMATIAN (APABILA BERSTATUS JANDA/DUDA)
                                                    5. FOTOCOPY AKTA KELAHIRAN/ IJAZAH;<br>
                                                    6. SURAT KETERANGAN SEHAT DARI DOKTER/PUSKESMAS SETEMPAT
                                                    7. FOTOCOPY PBB TERAKHIR DAN KWITANSI PELUNASAN PBB;<br>
                                                    8. MAP BIRU
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal11">Surat Keterangan Penghasilan</a>
                                    <div class="modal fade" id="exampleModal11" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Persyaratan</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    1. SURAT PENGANTAR RT/RW;<br>
                                                    2. FOTOCOPY KTP DAN KK;<br>
                                                    3. SURAT PERNYATAAN PENGHASILAN;<br>
                                                    4. MAP BIRU.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal12">Surat Keterangan Tidak Mampu</a>
                                    <div class="modal fade" id="exampleModal12" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Persyaratan</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    1. SURAT PENGANTAR RT/RW;<br>
                                                    2. FOTOCOPY KTP DAN KK;<br>
                                                    3. SURAT PERNYATAAN TIDAK MAMPU;<br>
                                                    4. MAP BIRU.

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal13">Surat Keterangan Orang Yang Sama</a>
                                    <div class="modal fade" id="exampleModal13" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Persyaratan</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    1. SURAT PENGANTAR RT/RW;<br>
                                                    2. FOTOCOPY KTP DAN KK;<br>
                                                    3. FOTOCOPY DOKUMEN PENDUKUNG SEBAGAI DASAR PEMBUATAN SURAT KETERANGAN;<br>
                                                    4. MAP BIRU.

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal14">Surat Keterangan Janda/Duda</a>
                                    <div class="modal fade" id="exampleModal14" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Persyaratan</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    1. SURAT PENGANTAR RT/RW;<br>
                                                    2. FOTOCOPY KTP DAN KK;<br>
                                                    3. FOTOCOPY AKTA CERAI/AKTA KEMATIAN.
                                                    4. MAP BIRU.

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal15">Surat Keterangan Usaha</a>
                                    <div class="modal fade" id="exampleModal15" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Persyaratan</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    1. SURAT PENGANTAR RT/RW;<br>
                                                    2. FOTOCOPY KTP DAN KK;<br>
                                                    3. FOTO USAHA (DI PRINT DI KERTAS HVS);<br>
                                                    4. FOTOCOPY PBB TERAKHIR DAN KWITANSI PELUNASAN PBB;<br>
                                                    5. MAP BIRU.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal16">Domisili Haji</a>
                                    <div class="modal fade" id="exampleModal16" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Persyaratan</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    1. SURAT PENGANTAR RT/RW;<br>
                                                    2. FOTOCOPY KTP DAN KK;<br>
                                                    3. SEMUA BERKAS DIBUAT 2 RANGKAP DAN DIMASUKAN DALAM MAP BIRU.

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal17">Penandatanganan Berkas Pensiun</a>
                                    <div class="modal fade" id="exampleModal17" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Persyaratan</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    1. SURAT PENGANTAR RT/RW;<br>
                                                    2. FOTOCOPY KTP DAN KK;<br>
                                                    3. FOTOCOPY KARIP ATAU SK PENSIUN;<br>
                                                    4. BERKAS YANG PERLU DI TANDA TANGAN;<br>
                                                    5. FOTOCOPY PBB TERAKHIR DAN KWITANSI
                                                    6. MAP BIRU.

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal18">Surat Keterangan Belum Memiliki Rumah</a>
                                    <div class="modal fade" id="exampleModal18" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Persyaratan</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    1. SURAT PENGANTAR RT/RW;<br>
                                                    2. FOTOCOPY KTP DAN KK;<br>
                                                    3. PERNYATAAN DIATAS MATERAI  10000
                                                    4. MAP BIRU.

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal19">Surat Keterangan Belum Menikah</a>
                                    <div class="modal fade" id="exampleModal19" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Persyaratan</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    1. SURAT PENGANTAR RT/RW;<br>
                                                    2. FOTOCOPY KTP DAN KK;<br>
                                                    3. PERNYATAAN DIATAS MATRAI 10000
                                                    4. MAP BIRU.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal20">Ahli Waris</a>
                                    <div class="modal fade" id="exampleModal20" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Persyaratan</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    1. SURAT PENGANTAR RT/RW;<br>
                                                    2. LEGALISIR AKTA KEMATIAN/SURAT KETERANGAN KEMATIAN PEWARIS (3 BUAH);<br>
                                                    3. FOTOKOPI E-KTP PEWARIS DAN PARA AHLI WARIS (3 BUAH);<br>
                                                    4. LEGALISIR KK (KECUALI JIKA KK TERSEBUT MEMILIKI BARCODE TIDAK PERLU LEGALISIR) PEWARIS DAN PARA AHLI WARIS (3 BUAH);<br>
                                                    5. FOTOKOPI E-KTP SAKSI 2 ORANG (3 BUAH);<br>
                                                    6. LEGALISIR SURAT NIKAH PEWARIS DAN PARA AHLI WARIS (3 BUAH);<br>
                                                    7. LEGALISIR AKTA LAHIR/IJAZAH PARA AHLI WARIS (3BUAH);<br>
                                                    8. FOTOCOPY PBB TERAKHIR DAN KWITANSI;<br>
                                                    9. MAP BIRU.

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="modal" data-bs-target="#exampleModal21">Surat Keterangan Pemekaran</a>
                                    <div class="modal fade" id="exampleModal21" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Persyaratan</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    1. SURAT PENGANTAR RT/RW;<br>
                                                    2. FOTOCOPY KTP DAN KK;<br>
                                                    3. FOTOCOPY PBB TERAKHIR DAN KWITANSI PELUNASAN PBB;<br>
                                                    4. FOTOCOPY SERTIFIKAT;<br>
                                                    5. SURAT PERNYATAAN PEMEKARAN;<br>
                                                    6. SURAT PERNYATAAN BATAS WILAYAH;<br>
                                                    7. FOTOCOPY AKTA JUAL BELI;<br>
                                                    8. MAP BIRU.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-bs-toggle="collapse" data-bs-target="#collapseTen"
                                aria-expanded="true" aria-controls="collapseTen">
                                Aplikasi Penunjang
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-bs-parent="#accordion">
                        <div class="card-body">
                            <ul class="navbar-nav ml-auto">
                                <a href="{{ url('https://siks.kemensos.go.id/') }}">SiksNG</a>
                                <a href="{{ url('https://cekbansos.kemensos.go.id//') }}">Cek Bansos Kemensos</a>
                                <a href="{{ url('https://simdik.bandung.go.id/dtks/') }}">Simdik</a>
                            </ul>

                        </div>
                    </div>
                </div>
                {{-- <div class="card">
                    <div class="card-header" id="headingeleven">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-bs-toggle="collapse" data-bs-target="#collapseeleven"
                                aria-expanded="true" aria-controls="collapseeleven">
                                Administrasi Umum
                            </button>
                        </h5>
                    </div>
                    <div id="collapseeleven" class="collapse" aria-labelledby="headingeleven" data-bs-parent="#accordion">
                        <div class="card-body">




                        </div>
                    </div>
                </div> --}}

            </div>
        </div>
    </div>
@endsection

