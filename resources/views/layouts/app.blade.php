<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Login Wargapedia</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link rel="shortcut icon" href="{{ asset('img/warped.png') }}" />

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-md ml-5 mt-5 mr-5">
                    <a href="{{ route('mandalikes.index') }}"><img src="{{ asset('img/warped.png') }}" alt="logo1" width="100%" height="50"
                    class="d-inline-block align-text-top"></a>
                    <strong><a class="mr-5" style="margin-left: 20px">Wargapedia</a></strong>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
