<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Wargapedia||{{ Auth::user()->role }}</title>
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <link href="{{asset('css/styles.css')}}" rel="stylesheet" />
    <script defer src="/your_path_to_version_6_files/js/all.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/js/all.min.js" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/062d15988f.js" crossorigin="anonymous"></script>
    </script>

<link rel="stylesheet" type="text/css" href="{{ asset('css/trix.css') }}">
<script type="text/javascript" src="{{ asset('js/trix.js') }}"></script>
<style>
    trix-toolbar [data-trix-button-group="file-tools"] {
        display: none;
    }

    .instagram-media {
            background:#FFF; border:0; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 1px 0 rgba(0,0,0,0.15); padding:0;
        }
</style>
</head>

<body class="sb-nav-fixed" style="background-color: #f9f690">
    <nav class="sb-topnav navbar navbar-expand navbar-warning bg-warning">
        <!-- Navbar Brand-->
        <a class="navbar-brand ps-3" href="#">{{ Auth::user()->name }}</a>
        <!-- Sidebar Toggle-->
        <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i
                class="fas fa-bars"></i></button>
        <!-- Navbar-->
        <ul class="navbar-nav d-none d-sm-inline-block ms-auto me-0 me-sm-3 my-2 my-sm-0">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown"
                    aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="#!">Settings</a></li>
                    <li><a class="dropdown-item" href="#!">Activity Log</a></li>
                    <li>
                        <hr class="dropdown-divider" />
                    </li>
                    <li>
                        <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                    </li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </ul>
            </li>
        </ul>
    </nav>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <div class="sb-sidenav-menu-heading">Home</div>
                        <a class="nav-link" href="{{ route('kelurahan.dashboard') }}">
                            <div class="sb-nav-link-icon">
                                @if(Auth::user()->image)
                              .  <img src="{{ asset('storage/' .Auth::user()->image) }}" class="img-fluid rounded-circle" style="width: 40px; height: 40px;">
                                @else
                                <i class="fas fa-tachometer-alt"></i>
                                @endif
                            </div>
                            Dashboard
                        </a>
                        <a class="nav-link" href="{{ route('kelurahan.notifikasi') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-pen-square" style="color: red"></i></div>
                            Notifikasi
                        </a>
                        <div class="sb-sidenav-menu-heading">Detail</div>
                        <a class="nav-link collapsed" href="#" data-bs-toggle="collapse"
                            data-bs-target="#collapseKependudukan" aria-expanded="false" aria-controls="collapseKependudukan">
                            <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                            Kependudukan
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="collapseKependudukan" aria-labelledby="headingOne"
                            data-bs-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{ route('kelurahan.keluarga.index') }}">Data Keluarga</a>
                                <a class="nav-link" href="{{ route('kelurahan.warga.index') }}">Data Warga</a>
                            </nav>
                        </div>
                        <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLampid"
                            aria-expanded="false" aria-controls="collapseLampid">
                            <div class="sb-nav-link-icon"><i class="fas fa-people-arrows"></i></div>
                            Lampid
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="collapseLampid" aria-labelledby="headingOne"
                            data-bs-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{ route('kelurahan.kelahiran') }}">Kelahiran</a>
                                <a class="nav-link" href="{{ route('kelurahan.kematian') }}">Kematian</a>
                                <a class="nav-link" href="{{ route('kelurahan.pindahdatang') }}">Pindah Datang</a>
                                <a class="nav-link collapsed" href="#" data-bs-toggle="collapse"
                                    data-bs-target="#collapsepindahkeluar" aria-expanded="false" aria-controls="collapsepindahkeluar">
                                    <div>Pindah Keluar</div>
                                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                </a>
                                <div class="collapse" id="collapsepindahkeluar" aria-labelledby="headingOne">
                                    <nav class="sb-sidenav-menu-nested nav">
                                        <a class="nav-link" href="{{ route('kelurahan.pindahkeluarin') }}">Tidak Tahu Alamat</a>
                                        <a class="nav-link" href="{{ route('kelurahan.pindahkeluar') }}">KK Tetap/Numpang KK</a>
                                        <a class="nav-link" href="{{ route('kelurahan.pindahkeluarbaru') }}">KK Baru</a>
                                    </nav>
                                </div>
                            </nav>
                        </div>
                        <a class="nav-link" href="{{ route('kelurahan.suket.index') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-user-edit"></i></div>
                            Simpel Online
                        </a>
                        <a class="nav-link" href="{{ route('kelurahan.laporan.index') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-user-edit"></i></div>
                            Simpeladuk
                        </a>
                        <a class="nav-link" href="{{ route('kelurahan.posting.index') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-user-edit"></i></div>
                            Posting Informasi
                        </a>

                        <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseMasterdata"
                            aria-expanded="false" aria-controls="collapseMasterdata">
                            <div class="sb-nav-link-icon"><i class="fas fa-database"></i></div>
                            Master Data
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="collapseMasterdata" aria-labelledby="headingOne"
                            data-bs-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{ route('kelurahan.alamat.index') }}">Data Alamat</a>
                                <a class="nav-link" href="{{ route('kelurahan.update-alamat-kk') }}">Update Alamat KK</a>
                                <a class="nav-link" href="{{ route('kelurahan.users.index') }}">Kontak</a>
                                <a class="nav-link" href="{{ route('kelurahan.pejabat.index') }}">Data Pejabat</a>
                            </nav>
                        </div>

                        <div class="sb-sidenav-menu-heading">User</div>
                        <a class="nav-link" href="{{ route('kelurahan.users.edit', Auth::user()->id) }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-camera"></i></div>
                            Update Profil
                        </a>
                        <a class="nav-link" href="{{ route('kelurahan.getchangepassword') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-user-edit"></i></div>
                            Ubah Password
                        </a>
                        <a class="nav-link" href="{{ route('logout') }}"  onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                            <div class="sb-nav-link-icon"><i class="fa-solid fa-user"></i></div>
                            {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </div>
            </nav>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container-fluid px-4" >
                    @include('includes.notifikasi')
                    @yield('content')
                </div>
            </main>
            <footer class="py-2 bg-dark mt-auto">
                <div class="container-fluid px-4">
                    <div class="d-flex align-items-center justify-content-between small">
                        <div class="text-muted">Layanan Informasi Kesejahteraan Sosial &copy; Kesos Mandalajati 2021</div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous">
    </script>
    <script src="{{ asset('js/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    @yield('scripts')
    <script src="{{ asset('js/scripts.js') }}"></script>
</body>

</html>
