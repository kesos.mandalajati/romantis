<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Wargapedia||{{ Auth::user()->role }}</title>
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
    <link href="{{asset('css/styles.css')}}" rel="stylesheet" />
    <script defer src="/your_path_to_version_6_files/js/all.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/js/all.min.js" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/062d15988f.js" crossorigin="anonymous"></script>
    </script>

    <link rel="stylesheet" type="text/css" href="{{ asset('css/trix.css') }}">
    <script type="text/javascript" src="{{ asset('js/trix.js') }}"></script>
    <style>
        trix-toolbar [data-trix-button-group="file-tools"] {
            display: none;
        }

        .instagram-media {
                background:#FFF; border:0; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 1px 0 rgba(0,0,0,0.15); padding:0;
            }
    </style>
</head>

<body class="sb-nav-fixed" style="background-color: #f9f690">
    <nav class="sb-topnav navbar navbar-expand navbar-warning bg-warning">
        <!-- Navbar Brand-->
        <a class="navbar-brand ps-3" href="#">{{ Auth::user()->name }}</a>
        <!-- Sidebar Toggle-->
        <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i
                class="fas fa-bars"></i></button>
        <!-- Navbar-->
        <ul class="navbar-nav d-none d-sm-inline-block ms-auto me-0 me-sm-3 my-2 my-sm-0">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown"
                    aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="#!">Settings</a></li>
                    <li><a class="dropdown-item" href="#!">Activity Log</a></li>
                    <li>
                        <hr class="dropdown-divider" />
                    </li>
                    <li>
                        <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                    </li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </ul>
            </li>
        </ul>
    </nav>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <div class="sb-sidenav-menu-heading">Home</div>
                        <a class="nav-link" href="{{ route('rw.dashboard') }}">
                            <div class="sb-nav-link-icon">
                                @if(Auth::user()->image)
                                <img src="{{ asset('storage/' .Auth::user()->image) }}" class="img-fluid rounded-circle" style="width: 40px; height: 40px;">
                                @else
                                <i class="fas fa-tachometer-alt"></i>
                                @endif
                            </div>
                            Dashboard
                        </a>
                        <a class="nav-link" href="{{ route('rw.notifikasi') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-pen-square" style="color: red"></i></div>
                            Notifikasi
                        </a>
                        <div class="sb-sidenav-menu-heading">Detail</div>
                        <a class="nav-link collapsed" href="#" data-bs-toggle="collapse"
                            data-bs-target="#collapseKependudukan" aria-expanded="false" aria-controls="collapseKependudukan">
                            <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                            Kependudukan
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="collapseKependudukan" aria-labelledby="headingOne"
                            data-bs-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{ route('rw.keluarga.index') }}">Data Keluarga</a>
                                <a class="nav-link" href="{{ route('rw.warga.index') }}">Data Warga</a>
                            </nav>
                        </div>
                        <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLampid"
                            aria-expanded="false" aria-controls="collapseLampid">
                            <div class="sb-nav-link-icon"><i class="fas fa-people-arrows"></i></div>
                            Lampid
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="collapseLampid" aria-labelledby="headingOne"
                            data-bs-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{ route('rw.kelahiran') }}">Kelahiran</a>
                                <a class="nav-link" href="{{ route('rw.kematian') }}">Kematian</a>
                                <a class="nav-link collapsed" href="#" data-bs-toggle="collapse"
                                    data-bs-target="#collapsepindahkeluar" aria-expanded="false" aria-controls="collapsepindahkeluar">
                                    <div>Pindah Keluar</div>
                                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                </a>
                                <div class="collapse" id="collapsepindahkeluar" aria-labelledby="headingOne">
                                    <nav class="sb-sidenav-menu-nested nav">
                                        <a class="nav-link" href="{{ route('rw.pindahkeluarin') }}">Tidak Tahu Alamat</a>
                                        <a class="nav-link" href="{{ route('rw.pindahkeluar') }}">Tahu Alamat</a>
                                    </nav>
                                </div>
                            </nav>
                        </div>
                        <a class="nav-link" href="{{ route('rw.suket.index') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-user-edit"></i></div>
                            Surat Pengantar
                        </a>
                        <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseMasterdata"
                            aria-expanded="false" aria-controls="collapseMasterdata">
                            <div class="sb-nav-link-icon"><i class="fas fa-database"></i></div>
                            Master Data
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="collapseMasterdata" aria-labelledby="headingOne"
                            data-bs-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{ route('rw.alamat.index') }}">Data Alamat</a>
                                <a class="nav-link" href="{{ route('rw.update-alamat-kk') }}">Update Alamat KK</a>
                                <a class="nav-link" href="{{ route('rw.users.index') }}">Kontak</a>
                            </nav>
                        </div>
                        {{-- <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseValidasi"
                            aria-expanded="false" aria-controls="collapseValidasi">
                            <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                            Laporan-Laporan
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="collapseValidasi" aria-labelledby="headingOne"
                            data-bs-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav"> --}}
                                {{-- <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#kependudukan" aria-expanded="false" aria-controls="kependudukan">
                                    Kependudukan
                                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                </a>
                                <div class="collapse" id="kependudukan" aria-labelledby="headingOne" data-bs-parent="#kependudukan">
                                    <nav class="sb-sidenav-menu-nested nav">
                                        <a class="nav-link" href="{{ route('rw.lapduk-umur') }}">Umur</a>
                                        <a class="nav-link" href="{{ route('rw.lapduk-pekerjaan') }}">Pekerjaan</a>
                                        <a class="nav-link" href="{{ route('rw.lapduk-pendidikan') }}">Pendidikan</a>
                                        <a class="nav-link" href="{{ route('rw.lapduk-domisili') }}">Domisili</a>
                                        <a class="nav-link" href="{{ route('rw.lapduk-agama') }}">Agama</a>
                                    </nav>
                                </div> --}}
                                {{-- <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#sosial" aria-expanded="false" aria-controls="sosial">
                                    Sosial
                                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                </a>
                                <div class="collapse" id="sosial" aria-labelledby="headingOne" data-bs-parent="#sosial">
                                    <nav class="sb-sidenav-menu-nested nav">
                                        <a class="nav-link" href="{{ route('rw.laporan-sosial-ppks') }}">PPKS</a>
                                        <a class="nav-link" href="{{ route('rw.laporan-sosial-pedi') }}">Pedi</a>
                                        <a class="nav-link" href="{{ route('rw.laporan-sosial-kpm') }}">KPM</a>
                                    </nav>
                                </div> --}}
                                {{-- <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#lainnya" aria-expanded="false" aria-controls="lainnya">
                                    Lainnya
                                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                </a>
                                <div class="collapse" id="lainnya" aria-labelledby="headingOne" data-bs-parent="#lainnya">
                                    <nav class="sb-sidenav-menu-nested nav">
                                        <a class="nav-link" href="{{ route('rw.laporan-lainnya-odf') }}">ODF</a>
                                        <a class="nav-link" href="{{ route('rw.laporan-lainnya-vaksin') }}">Vaksinasi</a>
                                        <a class="nav-link" href="{{ route('rw.laporan-lainnya-nop') }}">No Objek Pajak</a>
                                        <a class="nav-link" href="{{ route('rw.laporan-lainnya-kosan') }}">Rumah Kos</a>
                                    </nav>
                                </div> --}}
                            {{-- </nav>
                        </div> --}}
                        <div class="sb-sidenav-menu-heading">User</div>
                        <a class="nav-link" href="{{ route('rw.users.edit', Auth::user()->id) }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-camera"></i></div>
                            Update Profil
                        </a>
                        <a class="nav-link" href="{{ route('rw.getchangepassword') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-user-edit"></i></div>
                            Ubah Password
                        </a>
                        <a class="nav-link" href="{{ route('logout') }}"  onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                            <div class="sb-nav-link-icon"><i class="fa-solid fa-user"></i></div>
                            {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </div>
            </nav>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container-fluid px-4" >
                    @include('includes.notifikasi')
                    @yield('content')
                </div>
            </main>
            <footer class="py-2 bg-dark mt-auto">
                <div class="container-fluid px-4">
                    <div class="d-flex align-items-center justify-content-between small">
                        <div class="text-muted">Layanan Informasi Kesejahteraan Sosial &copy; Kesos Mandalajati 2021</div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous">
    </script>
    <script src="{{ asset('js/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    @yield('scripts')
    <script src="{{ asset('js/scripts.js') }}"></script>
</body>

</html>
