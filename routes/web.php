<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// SUKAMISKIN

//posting romantis
Route::get('/', [\App\Http\Controllers\RomantisController::class, 'romantis'])->name('romantis');
Route::get('/posting/giat/{id}', [\App\Http\Controllers\RomantisController::class, 'allnews'])->name('romantis.posting.allnews');
Route::get('/posting/pemberdayaan/{id}', [\App\Http\Controllers\RomantisController::class, 'allpemberdayaan'])->name('romantis.posting.allpemberdayaan');
Route::get('/galeri', [\App\Http\Controllers\RomantisController::class, 'galeri'])->name('romantis.posting.galeri');
Route::get('/headlines/', [\App\Http\Controllers\RomantisController::class, 'headlines'])->name('romantis.posting.headlines');
Route::get('/medsos/', [\App\Http\Controllers\RomantisController::class, 'medsos'])->name('romantis.posting.medsos');
Route::get('/quotes/', [\App\Http\Controllers\RomantisController::class, 'quotes'])->name('romantis.posting.quotes');
Route::get('/giat/', [\App\Http\Controllers\RomantisController::class, 'giat'])->name('romantis.posting.giat');

//profil romantis
Route::get('/profil/pengelolaansampah', [\App\Http\Controllers\RomantisController::class, 'profilkelurahanpengelolaansampah'])->name('romantis.profil.pengelolaansampah');
Route::get('/profil/pemberdayaanmasyarakat', [\App\Http\Controllers\RomantisController::class, 'profilkelurahanpemberdayaanmasyarakat'])->name('romantis.profil.pemberdayaanmasyarakat');
Route::get('/profil/perekonomian', [\App\Http\Controllers\RomantisController::class, 'profilkelurahanperekonomian'])->name('romantis.profil.perekonomian');
Route::get('/profil/perekonomian/{id}', [\App\Http\Controllers\RomantisController::class, 'lihatumkm'])->name('romantis.profil.lihatumkm');
Route::get('/profil/', [\App\Http\Controllers\RomantisController::class, 'profilkelurahan'])->name('romantis.profil.kelurahan');
Route::get('/lembaga/', [\App\Http\Controllers\RomantisController::class, 'profillembaga'])->name('romantis.profil.lembaga');
Route::get('/cetar/', [\App\Http\Controllers\RomantisController::class, 'cetar'])->name('romantis.profil.cetar');
Route::get('/simpelonline/', [\App\Http\Controllers\RomantisController::class, 'simpelonline'])->name('romantis.profil.simpelonline');
Route::get('/simpelikes/', [\App\Http\Controllers\RomantisController::class, 'simpelikes'])->name('romantis.profil.simpelikes');
Route::get('/persyaratan/', [\App\Http\Controllers\RomantisController::class, 'persyaratan'])->name('romantis.profil.persyaratan');
Route::get('/kependudukan/', [\App\Http\Controllers\RomantisController::class, 'kependudukan'])->name('romantis.profil.kependudukan');
Route::get('/skm/', [\App\Http\Controllers\RomantisController::class, 'skm'])->name('romantis.profil.skm');

//tambah keluarga
Route::post('/tambahkeluarga', [\App\Http\Controllers\RomantisController::class, 'tambahkeluarga'])->name('romantis.tambahkeluarga');

//tambah warga
Route::get('/tambahwarga', [\App\Http\Controllers\RomantisController::class, 'tambahwarga'])->name('romantis.tambahwarga');
Route::post('/simpanwarga', [\App\Http\Controllers\RomantisController::class, 'simpanwarga'])->name('romantis.simpanwarga');


//tambah suket
Route::get('/tambahsuketkematian', [\App\Http\Controllers\RomantisController::class, 'tambahsuketkematian'])->name('romantis.tambahsuketkematian');
Route::get('/tambahsuketkematianbaru', [\App\Http\Controllers\RomantisController::class, 'tambahsuketkematianbaru'])->name('romantis.tambahsuketkematianbaru');
Route::get('/tambahsuketkelahiran', [\App\Http\Controllers\RomantisController::class, 'tambahsuketkelahiran'])->name('romantis.tambahsuketkelahiran');
Route::get('/tambahsuketkelahiranbaru', [\App\Http\Controllers\RomantisController::class, 'tambahsuketkelahiranbaru'])->name('romantis.tambahsuketkelahiranbaru');
Route::get('/tambahsuketserbaguna', [\App\Http\Controllers\RomantisController::class, 'tambahsuketserbaguna'])->name('romantis.tambahsuketserbaguna');
Route::get('/tambahsuketna', [\App\Http\Controllers\RomantisController::class, 'tambahsuketna'])->name('romantis.tambahsuketna');
Route::get('/tambahsuketorangyangsama', [\App\Http\Controllers\RomantisController::class, 'tambahsuketorangyangsama'])->name('romantis.tambahsuketorangyangsama');
Route::get('/tambahsukettidakmampu', [\App\Http\Controllers\RomantisController::class, 'tambahsukettidakmampu'])->name('romantis.tambahsukettidakmampu');
Route::get('/tambahsuketsku', [\App\Http\Controllers\RomantisController::class, 'tambahsuketsku'])->name('romantis.tambahsuketsku');
Route::post('/simpansuket', [\App\Http\Controllers\RomantisController::class, 'simpansuket'])->name('romantis.simpansuket');
Route::get('/ubahsuket/{id}', [\App\Http\Controllers\RomantisController::class, 'ubahsuket'])->name('romantis.ubahsuket');
Route::put('/perbaharuisuket', [\App\Http\Controllers\RomantisController::class, 'perbaharuisuket'])->name('romantis.perbaharuisuket');


//print suket
Route::get('/printkematian/{id}', 'App\Http\Controllers\RomantisController@printkematian')->name('romantis.printkematian');
Route::get('/printsku/{id}', 'App\Http\Controllers\RomantisController@printsku')->name('romantis.printsku');
Route::get('/printkematianbaru/{id}', 'App\Http\Controllers\RomantisController@printkematianbaru')->name('romantis.printkematianbaru');
Route::get('/printkelahiran/{id}', 'App\Http\Controllers\RomantisController@printkelahiran')->name('romantis.printkelahiran');
Route::get('/printkelahiranbaru/{id}', 'App\Http\Controllers\RomantisController@printkelahiranbaru')->name('romantis.printkelahiranbaru');
Route::get('/printna/{id}', 'App\Http\Controllers\RomantisController@printna')->name('romantis.printna');
Route::get('/printserbaguna/{id}', 'App\Http\Controllers\RomantisController@printserbaguna')->name('romantis.printserbaguna');
Route::get('/printtidakmampu/{id}', 'App\Http\Controllers\RomantisController@printtidakmampu')->name('romantis.printtidakmampu');
Route::get('/printorangyangsama/{id}', 'App\Http\Controllers\RomantisController@printorangyangsama')->name('romantis.printorangyangsama');

//tambah alamat
Route::get('/searchlokasi', [\App\Http\Controllers\RomantisController::class, 'searchlokasi']);
Route::get('/tambahalamat', [\App\Http\Controllers\RomantisController::class, 'tambahalamat'])->name('romantis.tambahalamat');
Route::post('/simpanalamat', [\App\Http\Controllers\RomantisController::class, 'simpanalamat'])->name('romantis.simpanalamat');
