<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Keluarga extends Model
{
    use HasFactory;

    protected $fillable = [
        'no_kk',
        'alamat_kk_provinsi',
        'alamat_kk_kota_kab',
        'alamat_kk_kecamatan',
        'alamat_kk_kelurahan',
        'alamat_kk_rw',
        'alamat_kk_rt',
        'alamat_kk',
        'status_verifikasi_keluarga',
        'verifikator_keluarga',
        'verifikasiKeluarga',
        'validasi_keluarga',
    ];

    public function relasiKeluargaKeWarga()
    {
        return $this->hasMany(warga::class, 'keluarga_id');
    }

    public function relasiKeluargaKePmks()
    {
        return $this->belongsToMany(Pmks::class)->withTimestamps();
    }

    public function relasiKeluargaKeProvinsi()
    {
        return $this->belongsTo(Provinsi::class, 'alamat_kk_provinsi');
    }

    public function relasiKeluargaKeKotaKab()
    {
        return $this->belongsTo(Kota::class, 'alamat_kk_kota_kab');
    }

    public function relasiKeluargaKeKecamatan()
    {
        return $this->belongsTo(Kecamatan::class, 'alamat_kk_kecamatan');
    }

    public function relasiKeluargaKeKelurahan()
    {
        return $this->belongsTo(Kelurahan::class, 'alamat_kk_kelurahan');
    }

    public function relasiKeluargaKeErwe()
    {
        return $this->belongsTo(Erwe::class, 'alamat_kk_rw');
    }

    public function relasiKeluargaKeErte()
    {
        return $this->belongsTo(Erte::class, 'alamat_kk_rt');
    }

    public function relasiKeluargaKeAlamat()
    {
        return $this->belongsTo(Alamat::class, 'alamat_kk');
    }

    public function relasiKeluargaKeDtks()
    {
        return $this->belongsTo(Dtks::class, 'no_kk');
    }

    public function relasiKeluargaKeUser()
    {
        return $this->belongsTo(User::class, 'verifikator_keluarga');
    }
}
