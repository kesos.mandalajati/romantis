<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bantuan extends Model
{
    use HasFactory;
    protected $fillable = [
        'bulan',
        'jenis_bantuan',
        'sumber_dana',
    ];

    public function relasiBantuanKeWarga()
    {
        return $this->belongsToMany(warga::class)->withTimestamps();
    }

}
