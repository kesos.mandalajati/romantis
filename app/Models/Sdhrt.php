<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sdhrt extends Model
{
    use HasFactory;

    public function relasiSdhrtKeWarga()
    {
        return $this->hasMany(Warga::class, 'sdhrt');
    }

}
