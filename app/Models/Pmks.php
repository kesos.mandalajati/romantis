<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pmks extends Model
{
    use HasFactory;
    protected $fillable = [
        'type',
        'jenis_pmks',
        'jenis_disabilitas',
    ];

    public function relasiPmksKeWarga ()
    {
        return $this->belongsToMany(warga::class)->withPivot('verifikasiWarga', 'status_verifikasi_ppks', 'verifikator', 'validasi_warga')->withTimestamps();
    }

    public function relasiPmksKeKeluarga()
    {
        return $this->belongsToMany(Keluarga::class)->withTimestamps();
    }

}
