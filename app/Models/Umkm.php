<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Umkm extends Model
{
    use HasFactory;

    protected $fillable = [

        'id_warga',
        'id_alamat',
        'jenis_umkm',
        'nama_brand',
        'omset',
        'kebutuhan',
        'deskripsi_usaha',
        'tahun_mulai_usaha',
        'foto_brand',
        'legalitas',
        'aset',
        'jumlah_karyawan',

    ];

    public function relasiUmkmKeWarga()
    {
        return $this->belongsTo(warga::class, 'id_warga');
    }
}
