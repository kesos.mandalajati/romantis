<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pejabat extends Model
{
    use HasFactory;

    protected $fillable = [

        'rt',
        'rw',
        'kelurahan',
        'kecamatan',
        'kota_kab',
        'provinsi',
        'role',
        'name',
        'jabatan',
        'nip',
        'no_tlp',
        'instansi',

    ];

    public function relasiPejabatKeErte()
    {
        return $this->belongsTo(Erte::class, 'rt');
    }

    public function relasiPejabatKeErwe()
    {
        return $this->belongsTo(Erwe::class, 'rw');
    }

    public function relasiPejabatKeKelurahan()
    {
        return $this->belongsTo(Kelurahan::class, 'kelurahan');
    }

    public function relasiPejabatKeKecamatan()
    {
        return $this->belongsTo(Kecamatan::class, 'kecamatan');
    }

    public function relasiPejabatKeKotakab()
    {
        return $this->belongsTo(Kota::class, 'kota_kab');
    }

    public function relasiPejabatKeProvinsi()
    {
        return $this->belongsTo(Kota::class, 'provinsi');
    }
}
