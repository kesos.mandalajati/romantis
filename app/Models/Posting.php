<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

use Illuminate\Support\Carbon;

class Posting extends Model
{

    use HasFactory,  Sluggable;
    protected $fillable = [
        'user',
        'title',
        'category',
        'image',
        'excerpt',
        'body',
        'publish_at',
        'slug',
        'penulis',
        'quote',
        'tipe',
        'type_berita',
    ];

    public function getCreatedAttribute()
    {
        return Carbon::parse($this->attributes['publish_at'])
        ->translatedFormat('l, d F Y');
    }

    // public function getRouteKeyName()
    // {
    //     return 'slug';
    // }

    public function relasiPostingKeUser()
    {
        return $this->belongsTo(User::class, 'user');
    }


    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}


