<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dtks extends Model
{
    use HasFactory;

    protected $primaryKey = 'nik';

    public $incrementing = false;
    protected $keyType = 'string';


    protected $fillable = [
        'jenis_bantuan',
        'bulan_bantuan',
        'id_dtks',
        'id_art_dtks',
    ];

    public function relasiDtksKeKeluarga()
    {
        return $this->belongsTo(Keluarga::class)->withTimestamps();
    }

    public function relasiDtksKeWarga()
    {
        return $this->belongsTo(warga::class)->withTimestamps();
    }
}

