<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    use HasFactory;
    protected $fillable = [
        'nama_kelurahan',
        'kecamatan_id',        
        'kode_pos',
    ];

    public function relasiKelurahanKeWarga ()
    {
        return $this->hasMany(warga::class, 'kelurahan');
    }

    public function relasiKelurahanKeKecamatan ()
    {
        return $this->belongsTo(Kecamatan::class, 'Kecamatan_id');
    }

    public function relasiKelurahanKeErwe ()
    {
        return $this->hasMany(Erwe::class, 'kelurahan_id');
    }

    public function relasiKelurahanKePejabat()
    {
        return $this->hasMany(Pejabat::class, 'kelurahan');
    }
}
