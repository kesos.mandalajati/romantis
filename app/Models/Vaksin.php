<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vaksin extends Model
{
    use HasFactory;
    protected $fillable = [

        'jenis_vaksin',
        'keterangan',
    ];

    public function relasiVaksinKeWarga()
    {
        return $this->belongsMany(warga::class)->withTimestamps();
    }
}
