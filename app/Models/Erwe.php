<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Erwe extends Model
{
    use HasFactory;
    protected $fillable = [
        'nama_erwe',
        'kelurahan_id'
    ];

    public function relasiErweKeKeluarga ()
    {
        return $this->hasMany(Keluarga::class, 'alamat_kk_rw');
    }

    public function relasiErweKeWarga ()
    {
        return $this->hasMany(warga::class, 'alamat_domisili_rw');
    }

    public function relasiErweKeKelurahan()
    {
        return $this->belongsTo(Kelurahan::class, 'kelurahan_id');
    }

    public function relasiErweKeErte()
    {
        return $this->hasMany(Erte::class, 'rw_id');
    }
}
