<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class warga extends Model
{
    use HasFactory;

    protected $fillable = [

        'keluarga_id',
        'nik',
        'nama',
        'jenis_kelamin',
        'tempat_lahir',
        'tanggal_lahir',
        'sdhrt',
        'pendidikan',
        'pekerjaan',
        'agama',
        'status_perkawinan',
        'kewarganegaraan',
        'golongan_darah',
        'status_kematian',
        'alamat_domisili_provinsi',
        'alamat_domisili_kota_kab',
        'alamat_domisili_kecamatan',
        'alamat_domisili_kelurahan',
        'alamat_domisili_rw',
        'alamat_domisili_rt',
        'alamat_domisili',
        'status_verifikasi',
        'status_verifikasi_berkas',
        'status_verifikasi_ppks',
        'status_pindah_keluar',
        'status_bantuan',
        'status_proses',
        'vaksin',
        'verifikasiWarga',
        'verifikasiPenduduk',
        'verifikasiBerkas',
        'image',
        'status_pindah_datang',
        'verifikator',
        'verifikator_kependudukan',
        'verifikator_berkas',
        'validasi_warga',
        'validasi_kependudukan',
        'validasi_berkas',
        'usia',
        'verifikasiWargaWpa',
        'verifikasiWargaSecret',

    ];

    public function getCreatedAttribute()
    {
        return Carbon::parse($this->attributes['tanggal_lahir'])
        ->translatedFormat('l, d F Y');
    }

    public function relasiWargaKeKeluarga()
    {
        return $this->belongsTo(Keluarga::class, 'keluarga_id');
    }

    public function relasiWargaKeSdhrt()
    {
        return $this->belongsTo(Sdhrt::class, 'sdhrt');
    }

    public function relasiWargaKePendidikan()
    {
        return $this->belongsTo(Pendidikan::class, 'pendidikan');
    }

    public function relasiWargaKePekerjaan()
    {
        return $this->belongsTo(Pekerjaan::class, 'pekerjaan');
    }

    public function relasiWargaKePmks()
    {
        return $this->belongsToMany(Pmks::class)->withPivot('verifikasiWarga', 'status_verifikasi_ppks', 'verifikator', 'validasi_warga')->withTimestamps();
    }

    public function relasiWargaKeAgama()
    {
        return $this->belongsTo(Agama::class, 'agama');
    }

    public function relasiWargaKeProvinsi()
    {
        return $this->belongsTo(Provinsi::class, 'alamat_domisili_provinsi');
    }

    public function relasiWargaKeKotaKab()
    {
        return $this->belongsTo(Kota::class, 'alamat_domisili_kota_kab');
    }

    public function relasiWargaKeKecamatan()
    {
        return $this->belongsTo(Kecamatan::class, 'alamat_domisili_kecamatan');
    }

    public function relasiWargaKeKelurahan()
    {
        return $this->belongsTo(Kelurahan::class, 'alamat_domisili_kelurahan');
    }

    public function relasiWargaKeErwe()
    {
        return $this->belongsTo(Erwe::class, 'alamat_domisili_rw');
    }

    public function relasiWargaKeErte()
    {
        return $this->belongsTo(Erte::class, 'alamat_domisili_rt');
    }

    public function relasiWargaKeDisability()
    {
        return $this->belongsToMany(Disability::class)->withTimestamps();
    }

    public function relasiWargaKeAlamat()
    {
        return $this->belongsTo(Alamat::class, 'alamat_domisili')->orderBy('no_bangunan', 'ASC');
    }


    public function relasiWargaKeBantuan()
    {
        return $this->belongsToMany(Bantuan::class)->withTimestamps();
    }
    public function relasiWargaKeVaksin()
    {
        return $this->belongsToMany(Vaksin::class)->withTimestamps();
    }
    public function relasiWargaKeUser()
    {
        return $this->belongsTo(User::class, 'verifikator');
    }
    public function relasiWargaKeUserr()
    {
        return $this->belongsTo(User::class, 'verifikator_kependudukan');
    }
    public function relasiWargaKeUserrr()
    {
        return $this->belongsTo(User::class, 'verifikator_berkas');
    }

    public function relasiWargaKeUmkm()
    {
        return $this->hasMany(umkm::class, 'id_warga');
    }

    public function relasiWargaKeSuket()
    {
        return $this->hasMany(Suket::class, 'warga_id');
    }

}
