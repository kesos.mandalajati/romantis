<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Suket extends Model
{
    use HasFactory;

    protected $fillable = [

        'warga_id',
        'jenis_suket',
        'no_reg_rt',
        'tgl_reg_rt',
        'no_reg_rw',
        'tgl_reg_rw',
        'no_reg_kel',
        'tgl_reg_kel',
        'no_reg_kec',
        'tgl_reg_kec',
        'catatan',
        'maksud_suket',
        'status_verifikasi_suket',
        'no_tlp',
        'rt_id',
        'rw_id',
        'kelurahan_id',
        'kecamatan_id',
        'kota_kab_id',
        'rt_ttd',
        'rw_ttd',
        'kelurahan_ttd',
        'kecamatan_ttd',
        'kota_kab_ttd',
        'pemohon',
        'hubungan',
        'tempat',
        'sebab',
        'waktu_lahir',
        'nama_ibu',
        'nik_ibu',
        'no_kk_ibu',
        'ttl_ibu',
        'pekerjaan_ibu',
        'alamat_ibu',
        'nama_ayah',
        'nik_ayah',
        'no_kk_ayah',
        'ttl_ayah',
        'pekerjaan_ayah',
        'alamat_ayah',
        'tempat_nikah',
        'tgl_nikah',
        'buku_nikah',

        'tempat_lahir_ayah',
        'tempat_lahir_ibu',
        'kewarganegaraan_ayah',
        'kewarganegaraan_ibu',
        'agama_ayah',
        'agama_ibu',
        'nama_pasangan',
        'status_perkawinan_pasangan',

        'keluarga_id',

    ];

    public function getCreatedAttribute()
    {
        return Carbon::parse($this->attributes['ttl_ibu'])
        ->translatedFormat('l, d F Y');
    }

    public function relasiSuketKeWarga()
    {
        return $this->belongsTo(warga::class, 'warga_id');
    }

    public function relasiSuketKeWargaIbu()
    {
        return $this->belongsTo(warga::class, 'nik_ibu');
    }

    public function relasiSuketKeWargaAyah()
    {
        return $this->belongsTo(warga::class, 'nik_ayah');
    }

    public function relasiSuketKeErte()
    {
        return $this->belongsTo(Erte::class, 'rt_id');
    }


    public function relasiSuketKeErwe()
    {
        return $this->belongsTo(Erwe::class, 'rw_id');
    }


    public function relasiSuketKeKelurahan()
    {
        return $this->belongsTo(Kelurahan::class, 'kelurahan_id');
    }

    public function relasiSuketKeKecamatan()
    {
        return $this->belongsTo(Kecamatan::class, 'kecamatan_id');
    }

    public function relasiSuketKeKotakab()
    {
        return $this->belongsTo(Kota::class, 'kota_kab_id');
    }

    public function relasiSuketKePejabat()
    {
        return $this->belongsTo(Pejabat::class, 'kelurahan_ttd');
    }


}
