<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'kelurahan',
        'kecamatan',
        'kota_kab',
        'provinsi',
        'rt',
        'rw',
        'role',
        'no_tlp',
        'image',
        'alamat_sekretariat',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function hasRole($role)
    {
        if ($this->role == $role) {
            return $this;
        } else {
            return null;
        }
    }

    public function relasiUserKeErte ()
    {
        return $this->hasOne(Erte::class);
    }

    public function relasiUserKeErwe ()
    {
        return $this->hasOne(Erwe::class);
    }

    public function relasiUserKeKelurahan ()
    {
        return $this->hasOne(Kelurahan::class);
    }

    public function relasiUserKeKecamatan ()
    {
        return $this->hasOne(Kecamatan::class);
    }

    public function relasiUserKeKotaKab ()
    {
        return $this->hasOne(Kota::class);
    }

    public function relasiUserKeProvinsi ()
    {
        return $this->hasOne(Provinsi::class);
    }

    public function relasiUserKeWarga()
    {
        return $this->hasMany(warga::class, 'status_verifikasi');
    }
}
