<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Alamat extends Model
{
    use HasFactory;
    protected $fillable = [
        'nama_alamat',
        'rt_id',
        'kelurahan_id',
        'rw_id',
        'kecamatan_id',
        'kota_kab_id',
        'provinsi_id',
        'image',
        'no_bangunan',
        'alamat_kk_latitude',
        'alamat_kk_longitude',
        'sumber_air',
        'fasilitas_listrik',
        'kepemilikan_septictank',
        'kepemilikan_jamban',
        'nop',
        'alamat_yang_baru',
        'verifikasiAlamat',
        'status_verifikasi_alamat',
        'validasi_alamat',
        'verifikator_alamat',


    ];

    public function relasiAlamatKeKeluarga()
    {
        return $this->hasMany(Keluarga::class, 'alamat_kk');
    }

    public function relasiAlamatKeWarga()
    {
        return $this->hasMany(warga::class, 'alamat_domisili');
    }

    public function relasiAlamatKeErte()
    {
        return $this->belongsTo(Erte::class, 'rt_id');
    }

    public function relasiAlamatKeErwe()
    {
        return $this->belongsTo(Erwe::class, 'rw_id');
    }

    public function relasiAlamatKeKelurahan()
    {
        return $this->belongsTo(Kelurahan::class, 'kelurahan_id');
    }

    public function relasiAlamatKeBangunan()
    {
        return $this->belongsToMany(Bangunan::class)->withTimestamps();
    }

}
