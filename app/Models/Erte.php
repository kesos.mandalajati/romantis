<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Erte extends Model
{
    use HasFactory;

    public function relasiErteKeKeluarga ()
    {
        return $this->hasMany(Keluarga::class, 'erte');
    }

    public function relasiErteKeWarga ()
    {
        return $this->hasMany(warga::class, 'erte');
    }

    public function relasiErteKeErwe()
    {
        return $this->belongsTo(Erwe::class, 'rw_id');
    }

    public function relasiErteKeAlamat()
    {
        return $this->hasMany(Alamat::class, 'rt_id');
    }

    public function relasiErteKeUser()
    {
        return $this->belongsTo(User::class);
    }
}
