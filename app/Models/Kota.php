<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kota extends Model
{
    use HasFactory;
    protected $fillable = [
        'nama_kota_kab',
        'provinsi_id',
    ];

    public function relasiKotaKabKeWarga()
    {
        return $this->hasMany(warga::class, 'kota');
    }

    public function relasiKotaKabKeProvinsi()
    {
        return $this->belongsTo(Provinsi::class, 'provinsi_id');
    }

    public function relasiKotaKabKeKecamatan()
    {
        return $this->hasMany(Kecamatan::class, 'kota_kab_id');
    }

    
}
