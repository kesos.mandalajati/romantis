<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    use HasFactory;
    protected $fillable = [
        'nama_kecamatan',
        'kota_kab_id',
    ];

    public function relasiKecamatanKeWarga() {
        return $this->hasMany(warga::class, 'kecamatan');
    }

    public function relasiKecamatanKeKotaKab()
    {
        return $this->belongsTo(Kota::class, 'kota_kab_id');
    }

    public function relasiKecamatanKeKelurahan() {
        return $this->hasMany(Kelurahan::class, 'kecamatan_id');
    }

}
