<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bangunan extends Model
{
    use HasFactory;
    protected $fillable = [
        'jenis_bangunan',
        'nama_bangunan',
        'umkm',
    ];

    public function relasiBangunanKeAlamat()
    {
        return $this->belongsToMany(Alamat::class)->withTimestamps();
    }
}


