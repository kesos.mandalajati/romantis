<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $table = 'videos';
    protected $fillable = [
        'title', 'video', 'publish_at', 'user', 'category', 'body', 'excerpt'
    ];

    public function relasiVideoKeUser()
    {
        return $this->belongsTo(User::class, 'user');
    }
}
