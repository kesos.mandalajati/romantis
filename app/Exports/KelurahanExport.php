<?php

namespace App\Exports;

use App\Models\Erte;
use App\Models\Alamat;
use App\Models\Erwe;
use App\Models\Kelurahan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class KelurahanExport implements FromView
{
    public function view(): View
    {
        return view('exports.kelurahan', [
            'kelurahan' => Kelurahan::all()
        ]);
    }
}
