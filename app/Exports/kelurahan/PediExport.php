<?php

namespace App\Exports\kelurahan;

use App\Models\warga;
use App\Models\Vaksin;
use App\Models\Erte;
use App\Models\Alamat;
use App\Models\Erwe;
use App\Models\Keluarga;
use App\Models\Kelurahan;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromView;

class WargaExport implements FromView
{
    public function view(): View
    {

        return view('exports.kelurahan.pedi', [
            'warga' => warga::where('alamat_domisili_kelurahan', '=', Auth::user()->kelurahan)->whereHas("relasiWargaKePmks", function ($q) {
                $q->where('pmks.id', '=', 8);
            })->get(),

            'keluarga' => Keluarga::all()
        ]);
    }


}
