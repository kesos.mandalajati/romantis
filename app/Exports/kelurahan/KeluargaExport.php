<?php

namespace App\Exports\kelurahan;

use App\Models\Keluarga;
use App\Models\Erte;
use App\Models\Alamat;
use App\Models\Erwe;
use App\Models\Kelurahan;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromView;

class KeluargaExport implements FromView
{
    public function view(): View
    {
        return view('exports.keluarga', [
            'keluarga' => Keluarga::where('alamat_kk_kelurahan', '=', Auth::user()->kelurahan)->with(["relasiKeluargaKeWarga" => function ($q) {
                $q->where('wargas.sdhrt', '=', 1);
            }])->get()
        ]);
    }
}
