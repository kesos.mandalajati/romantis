<?php

namespace App\Exports\kelurahan;

use App\Models\warga;
use App\Models\Vaksin;
use App\Models\Erte;
use App\Models\Alamat;
use App\Models\Erwe;
use App\Models\Keluarga;
use App\Models\Kelurahan;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromView;

class KematianExport implements FromView
{
    public function view(): View
    {

        return view('exports.kelurahan.kematian', [
            'warga' => warga::where('alamat_domisili_kelurahan', '=', Auth::user()->kelurahan)->where('status_kematian', '!=', NULL)->get(),

            'keluarga' => Keluarga::all()
        ]);
    }


}
