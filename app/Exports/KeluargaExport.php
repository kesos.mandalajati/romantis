<?php

namespace App\Exports;

use App\Models\Keluarga;
use App\Models\Erte;
use App\Models\Alamat;
use App\Models\Erwe;
use App\Models\Kelurahan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class KeluargaExport implements FromView
{
    public function view(): View
    {
        return view('exports.keluarga', [
            'keluarga' => Keluarga::with(["relasiKeluargaKeWarga" => function ($q) {
                $q->where('wargas.sdhrt', '=', 1);
            }])->get()
        ]);
    }
}
