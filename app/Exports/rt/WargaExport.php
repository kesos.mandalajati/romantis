<?php

namespace App\Exports\rt;

use App\Models\warga;
use App\Models\Vaksin;
use App\Models\Erte;
use App\Models\Alamat;
use App\Models\Erwe;
use App\Models\Keluarga;
use App\Models\Kelurahan;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromView;

class WargaExport implements FromView
{
    public function view(): View
    {

        return view('exports.rt.warga', [
            'warga' => warga::where('alamat_domisili_rt', '=', Auth::user()->rt)->orderBy('alamat_domisili')->orderBy('keluarga_id')->orderBy('sdhrt')->orderBy('tanggal_lahir')->get(),

            // 'warga' => warga::with(["relasiWargaKeKeluarga" => function ($q) {
            //     $q->where('keluargas.alamat_kk_rt', '=', Auth::user()->rt);
            // }])->orderBy('keluarga_id')->orderBy('sdhrt')->orderBy('tanggal_lahir')->get(),

            'keluarga' => Keluarga::all()
        ]);

    }


}
