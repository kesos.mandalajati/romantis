<?php

namespace App\Exports;

use App\Models\warga;
use App\Models\Vaksin;
use App\Models\Erte;
use App\Models\Alamat;
use App\Models\Erwe;
use App\Models\Kelurahan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class WargaExport implements FromView
{
    public function view(): View
    {
        // return view('exports.warga', [
        //     'warga' => warga::with(["relasiWargaKeVaksin" => function ($q) {
        //         $q->where('vaksins.id', '=', 4);
        //     }])->get()
        // ]);

        return view('exports.warga', [
            'warga' => warga::all()
        ]);
    }


}
