<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pmks;
use App\Models\User;
use App\Models\Kelurahan;
use App\Models\Disability;
use App\Models\Vaksin;
use App\Models\Pekerjaan;
use App\Models\Pendidikan;
use App\Models\Agama;
use App\Models\Alamat;
use App\Models\warga;
use App\Models\Keluarga;
use App\Models\Posting;
use App\Models\Kota;
use App\Models\Bangunan;
use App\Models\Sdhrt;
use App\Models\Provinsi;
use App\Models\Suket;
use App\Models\Erte;
use App\Models\Kecamatan;
use App\Models\Erwe;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class RomantisController extends Controller
{

    //POSTING
    public function romantis()
    {
        $posting = Posting::all();

        $headlines = Posting::where('category', 12880)->where('type_berita', '=', 'headlines')->orderBy('publish_at', 'desc')->get();

        $blogpost = Posting::where('category', 12880)->where('type_berita', '=', 'blogpost')->orderBy('publish_at', 'desc')->get();

        $quotes = Posting::where('category', 12880)->where('type_berita', '=', 'quotes')->orderBy('publish_at', 'desc')->get();

        $giat = Posting::where('category', 12880)->where('type_berita', '=', 'giat')->orderBy('publish_at', 'desc')->paginate(10);

        $about = Posting::where('category', 12880)->where('type_berita', '=', 'about')->orderBy('publish_at', 'desc')->paginate(20);

        $medsos = Posting::where('category', 12880)->where('type_berita', '=', 'medsos')->orderBy('publish_at', 'desc')->paginate(3);

        $galeri = Posting::where('image', '!=', null)->where('category', 12880)->orderBy('publish_at', 'desc')->get();

        $pkk = Posting::where('category', 12880)->where('type_berita', '=', 'about')->orderBy('publish_at', 'desc')->paginate(3);

        $lpm = Posting::where('category', 12880)->where('type_berita', '=', 'aboutlpm')->orderBy('publish_at', 'desc')->paginate(3);

        $rw = Posting::where('category', 12880)->where('type_berita', '=', 'aboutrw')->orderBy('publish_at', 'desc')->paginate(3);

        $karta = Posting::where('category', 12880)->where('type_berita', '=', 'aboutkarta')->orderBy('publish_at', 'desc')->paginate(3);

        return view ('romantis.romantis', compact('about', 'blogpost', 'posting', 'headlines',  'quotes',  'giat', 'medsos', 'galeri', 'pkk', 'karta', 'rw', 'lpm'));

    }
    public function allnews(Request $request)
    {
        $posting = Posting::find($request->id);
        return view ('romantis.posting.allnews', compact('posting'));
    }
    public function allpemberdayaan(Request $request)
    {
        $posting = Posting::find($request->id);
        return view ('romantis.posting.allpemberdayaan', compact('posting'));
    }
    public function headlines(Request $request)
    {
        $posting = Posting::where('type_berita', '=', 'headlines')->where('category', 12880)->orderBy('publish_at', 'desc')->get();
        return view ('romantis.posting.index', compact('posting'));
    }
    public function medsos(Request $request)
    {
        $posting = Posting::where('type_berita', '=', 'medsos')->where('category', 12880)->orderBy('publish_at', 'desc')->get();
        return view ('romantis.posting.index', compact('posting'));
    }
    public function quotes(Request $request)
    {
        $posting = Posting::where('type_berita', '=', 'quotes')->where('category', 12880)->orderBy('publish_at', 'desc')->get();
        return view ('romantis.posting.index', compact('posting'));
    }
    public function galeri(Request $request)
    {
        $posting = Posting::where('type_berita', '=', 'galeri')->where('category', 12880)->orderBy('publish_at', 'desc')->get();
        return view ('romantis.posting.index', compact('posting'));
    }
    public function giat(Request $request)
    {
        $posting = Posting::where('type_berita', 'giat')->where('category', 12880)->get();

        return view ('romantis.posting.indexGiat', compact('posting'));
    }


    //PROFIL
    public function profilkelurahanpengelolaansampah (Request $request)
    {
        $quotes = Posting::where('category', 12880)->where('type_berita', '=', 'quotes')->orderBy('publish_at', 'desc')->get();

        return view ('romantis.posting.pengelolaansampah', compact('quotes'));
    }

    public function profilkelurahanpemberdayaanmasyarakat (Request $request)
    {
        $about = Posting::where('category', 12880)->where('type_berita', '=', 'about')->orderBy('publish_at', 'desc')->get();
        $aboutlpm = Posting::where('category', 12880)->where('type_berita', '=', 'aboutlpm')->orderBy('publish_at', 'desc')->get();
        $aboutkarta = Posting::where('category', 12880)->where('type_berita', '=', 'aboutkarta')->orderBy('publish_at', 'desc')->get();
        $aboutrw = Posting::where('category', 12880)->where('type_berita', '=', 'aboutrw')->orderBy('publish_at', 'desc')->get();

        return view ('romantis.posting.pemberdayaanmasyarakat', compact('about', 'aboutlpm', 'aboutkarta', 'aboutrw'));
    }


    public function profilkelurahanperekonomian (Request $request)
    {
        $kopishop = Alamat::where('kelurahan_id', 12880)->whereHas("relasiAlamatKeBangunan", function ($q) {
            $q->where('bangunan_id', '=', 39);
        })->get();

        $barbershop = Alamat::where('kelurahan_id', 12880)->whereHas("relasiAlamatKeBangunan", function ($q) {
            $q->where('bangunan_id', '=', 40);
        })->get();

        $bengkel = Alamat::where('kelurahan_id', 12880)->whereHas("relasiAlamatKeBangunan", function ($q) {
            $q->where('bangunan_id', '=', 38);
        })->get();

        $warung = Alamat::where('kelurahan_id', 12880)->whereHas("relasiAlamatKeBangunan", function ($q) {
            $q->where('bangunan_id', '=', 10);
        })->get();

        return view ('romantis.posting.umkm', compact('kopishop', 'barbershop', 'bengkel', 'warung'));
    }
    public function lihatumkm(Request $request)
    {
        $umkm = Alamat::find($request->id);
        return view ('romantis.posting.lihatumkm', compact('umkm'));
    }

    public function profilkelurahan(Request $request)
    {
        $kk = Keluarga::where('alamat_kk_kelurahan', '=', 12880)->count();
        $penduduk = warga::where('alamat_domisili_kelurahan', '=', 12880)->where('status_kematian', null)->where('status_pindah_datang', null)->count();

          // jumlah penduduk berdasarkan umur
          $warga = warga::where('alamat_domisili_kelurahan', '=', 12880)->where('status_kematian', null)->get();
          $warga_0_4 = $this->getAgeCount(0, 4, $warga);
          $warga_5_9 = $this->getAgeCount(5, 9, $warga);
          $warga_10_14 = $this->getAgeCount(10, 14, $warga);
          $warga_15_19 = $this->getAgeCount(15, 19, $warga);
          $warga_20_24 = $this->getAgeCount(20, 24, $warga);
          $warga_25_29 = $this->getAgeCount(25, 29, $warga);
          $warga_30_34 = $this->getAgeCount(30, 34, $warga);
          $warga_35_39 = $this->getAgeCount(35, 39, $warga);
          $warga_40_44 = $this->getAgeCount(40, 44, $warga);
          $warga_45_49 = $this->getAgeCount(45, 49, $warga);
          $warga_50_54 = $this->getAgeCount(50, 54, $warga);
          $warga_55_59 = $this->getAgeCount(55, 59, $warga);
          $warga_60 = $this->getAgeCount(60, 200, $warga);
          $chart_umur_label = ["0-4", "5-9", "10-14", "15-19", "20-24", "25-29", "30-34", "35-39", "40-44", "45-49", "50-54", "55-59", ">60"];
          $chart_umur_data = [$warga_0_4, $warga_5_9, $warga_10_14, $warga_15_19, $warga_20_24, $warga_25_29, $warga_30_34, $warga_35_39, $warga_40_44, $warga_45_49, $warga_50_54, $warga_55_59, $warga_60];



           // jumlah penduduk berdasarkan jenis kelamin
        $laki = warga::where('alamat_domisili_kelurahan', '=', "12880")->where('status_kematian', null)->where('status_pindah_datang', null)->where('jenis_kelamin', '=', 'laki-laki')->count();
        $perempuan = warga::where('alamat_domisili_kelurahan', '=', "12880")->where('status_kematian', null)->where('status_pindah_datang', null)->where('jenis_kelamin', '=', 'perempuan')->count();
        $chart_jenis_kelamin_label = ["Laki-Laki", "Perempuan"];
        $chart_jenis_kelamin_data = [$laki, $perempuan];

        // jumlah penduduk berdasarkan kewarganegaraan
        $wna = warga::where('alamat_domisili_kelurahan', '=', "12880")->where('kewarganegaraan', '=', 'wna')->where('status_kematian', null)->where('status_pindah_datang', null)->count();
        $wni = warga::where('alamat_domisili_kelurahan', '=', "12880")->where('kewarganegaraan', '=', 'wni')->where('status_kematian', null)->where('status_pindah_datang', null)->count();
        $warganegara = ['WNI', 'WNA'];
        $jumlahWargaNegara = [$wni, $wna];

        // jumlah penduduk berdasarkan status perkawinan
        $kawin = warga::where('alamat_domisili_kelurahan', '=', "12880")->where('status_perkawinan', '=', 'kawin')->where('status_kematian', null)->where('status_pindah_datang', null)->count();
        $belumKawin = warga::where('alamat_domisili_kelurahan', '=', "12880")->where('status_perkawinan', '=', 'belum kawin')->where('status_kematian', null)->where('status_pindah_datang', null)->count();
        $ceraiMati = warga::where('alamat_domisili_kelurahan', '=', "12880")->where('status_perkawinan', '=', 'cerai mati')->where('status_kematian', null)->where('status_pindah_datang', null)->count();
        $ceraiHidup = warga::where('alamat_domisili_kelurahan', '=', "12880")->where('status_perkawinan', '=', 'cerai hidup')->where('status_kematian', null)->where('status_pindah_datang', null)->count();
        $chart_status_perkawinan_label = ["kawin", "belum kawin", "cerai hidup", "cerai mati"];
        $chart_status_perkawinan_data = [$kawin, $belumKawin, $ceraiHidup, $ceraiMati];


        // jumlah penduduk berdasarkan agama
        $tittleAgama = warga::where('alamat_domisili_kelurahan', '=', "12880")->where('status_kematian', null)->where('status_pindah_datang', null)->select('agamas.nama_agama', DB::raw('count(*) as jumlah'))->join('agamas', 'wargas.agama', '=', 'agamas.id')->groupBy('agamas.nama_agama')->orderBy('agamas.nama_agama', 'ASC')->pluck('nama_agama');
        $jumlahWargaAgama = warga::where('alamat_domisili_kelurahan', '=', "12880")->where('status_kematian', null)->where('status_pindah_datang', null)->select('agamas.nama_agama', DB::raw('count(*) as jumlah'))->join('agamas', 'wargas.agama', '=', 'agamas.id')->groupBy('agamas.nama_agama')->orderBy('agamas.nama_agama', 'ASC')->pluck('jumlah');


        // jumlah penduduk berdasarkan pekerjaan
        $tittlePekerjaan = warga::where('alamat_domisili_kelurahan', '=', "12880")->where('status_kematian', null)->where('status_pindah_datang', null)->select('pekerjaans.nama_pekerjaan', DB::raw('count(*) as jumlah'))->join('pekerjaans', 'wargas.pekerjaan', '=', 'pekerjaans.id')->groupBy('pekerjaans.nama_pekerjaan')->orderBy('pekerjaans.nama_pekerjaan', 'ASC')->pluck('nama_pekerjaan');
        $jumlahWargaPekerjaan = warga::where('alamat_domisili_kelurahan', '=', "12880")->where('status_kematian', null)->where('status_pindah_datang', null)->select('pekerjaans.nama_pekerjaan', DB::raw('count(*) as jumlah'))->join('pekerjaans', 'wargas.pekerjaan', '=', 'pekerjaans.id')->groupBy('pekerjaans.nama_pekerjaan')->orderBy('pekerjaans.nama_pekerjaan', 'ASC')->pluck('jumlah');


        // jumlah penduduk berdasarkan pendidikan
        $tittlePendidikan = warga::where('alamat_domisili_kelurahan', '=', "12880")->where('status_kematian', null)->where('status_pindah_datang', null)->select('pendidikans.nama_pendidikan', DB::raw('count(*) as jumlah'))->join('pendidikans', 'wargas.pendidikan', '=', 'pendidikans.id')->groupBy('pendidikans.nama_pendidikan')->orderBy('pendidikans.nama_pendidikan', 'ASC')->pluck('nama_pendidikan');
        $jumlahWargaPendidikan = warga::where('alamat_domisili_kelurahan', '=', "12880")->where('status_kematian', null)->where('status_pindah_datang', null)->select('pendidikans.nama_pendidikan', DB::raw('count(*) as jumlah'))->join('pendidikans', 'wargas.pendidikan', '=', 'pendidikans.id')->groupBy('pendidikans.nama_pendidikan')->orderBy('pendidikans.nama_pendidikan', 'ASC')->pluck('jumlah');


        // jumlah penduduk permanen dan non permanen
        $permanen = DB::select('SELECT COUNT(*) AS "permanen" FROM wargas JOIN keluargas ON wargas.keluarga_id = keluargas.id WHERE wargas.status_kematian IS NULL AND wargas.alamat_domisili_kelurahan = ' . "12880" . ' AND keluargas.alamat_kk_kelurahan = ' . "12880");
        $nonpermanen = DB::select('SELECT COUNT(*) AS "nonpermanen" FROM wargas JOIN keluargas ON wargas.keluarga_id = keluargas.id WHERE wargas.status_kematian IS NULL AND wargas.alamat_domisili_kelurahan = ' . "12880" . ' AND keluargas.alamat_kk_kelurahan <> ' . "12880");
        $chart_domisili_label = ["Permanen", "Non-Permanen"];
        $chart_domisili_data = [$permanen[0]->permanen, $nonpermanen[0]->nonpermanen];


        $rtod = Alamat::where('kepemilikan_septictank', 'TIDAK ADA')->where('kelurahan_id', '=', 12880)->count();

        $umpi = Alamat::whereHas('relasiAlamatKeBangunan')->where('kelurahan_id', '=', 12880)->count();

        $umkm = Alamat::whereHas("relasiAlamatKeBangunan", function ($q) {
            $q->where('bangunans.umkm', '=', 'ya');
        })->where('kelurahan_id', '=', 12880)->count();

        $nop_erte = Alamat::whereNotNull('nop')->where('kelurahan_id', '=', 12880)->count();


        return view ('romantis.profil.kelurahan', compact('kk', 'penduduk', 'chart_umur_label', 'chart_jenis_kelamin_label', 'chart_jenis_kelamin_data', 'tittleAgama', 'jumlahWargaAgama', 'chart_status_perkawinan_label', 'chart_status_perkawinan_data', 'tittlePekerjaan', 'jumlahWargaPekerjaan', 'tittlePendidikan', 'jumlahWargaPendidikan', 'jumlahWargaNegara', 'warganegara', 'chart_domisili_label', 'chart_domisili_data', 'warga', 'warga_0_4', 'warga_5_9', 'warga_10_14', 'warga_15_19', 'warga_20_24', 'warga_25_29',  'warga_30_34', 'warga_35_39', 'warga_40_44', 'warga_45_49', 'warga_50_54', 'warga_55_59',  'warga_60', 'chart_umur_data', 'rtod', 'nop_erte', 'umpi', 'umkm'));
    }
    public function getAgeCount($min, $max, $data)
    {
        $count = 0;
        foreach ($data as $value) {
            $age = Carbon::parse($value->tanggal_lahir)->age;
            if ($age >= $min && $age <= $max) {
                $count++;
            }
        }
        return $count;
    }



    public function profillembaga(Request $request)
    {

        return view ('romantis.profil.lembaga');
    }

    public function cetar(Request $request)
    {

        return view ('romantis.profil.cetar');
    }
    public function simpelonline(Request $request)
    {
        if ($request->submit == 'searchNik') {
            $searchNik = $request->searchText;
            $warga = warga::where('nik', '=', $request->searchText)->orWhere('nama', '=', $request->searchText)->get();
            $provinsis = Provinsi::where('id', 12)->get();
            $erte = Erte::all();
            $erwe = Erwe::all();
            $bangunan = Bangunan::all();
            $keluarahan = Kelurahan::where('id', 12880)->get();
            $keluarahans = Kelurahan::where('id', 12880)->get();
            $kecamatans = Kecamatan::where('id', 1252)->get();
            $kota_kabs = Kota::where('id', 12880)->get();
            $suket = Suket::all();

            return view ('romantis.profil.simpelonline', compact(  'bangunan',  'keluarahans', 'kecamatans', 'kota_kabs', 'provinsis', 'erte', 'erwe', 'searchNik', 'warga', 'keluarahan', 'suket'));
        }

        if ($request->submit == 'search') {
            $search = $request->searchText;
            $keluarga = Keluarga::where('no_kk', '=', $request->searchText)->get();
            $provinsis = Provinsi::where('id', 12)->get();
            $erte = Erte::all();
            $erwe = Erwe::all();
            $bangunan = Bangunan::all();
            $keluarahan = Kelurahan::where('id', 12880)->get();
            $keluarahans = Kelurahan::where('id', 12880)->get();
            $kecamatans = Kecamatan::where('id', 1252)->get();
            $kota_kabs = Kota::where('id', 12880)->get();
            $suket = Suket::all();
            return view ('romantis.profil.simpelonline', compact(  'bangunan',  'keluarahans', 'kecamatans', 'kota_kabs', 'provinsis', 'erte', 'erwe', 'search', 'keluarga', 'keluarahan', 'suket'));
        }

        $provinsis = Provinsi::where('id', 12)->get();
        $erte = Erte::all();
        $erwe = Erwe::all();
        $bangunan = Bangunan::all();
        $keluarahans = Kelurahan::where('id', 12880)->get();
        $keluarahan = Kelurahan::where('id', 12880)->get();
        $kecamatans = Kecamatan::where('id', 1252)->get();
        $kota_kabs = Kota::where('id', 12880)->get();
        $suket = Suket::all();
        return view ('romantis.profil.simpelonline', compact(  'bangunan',  'keluarahans', 'kecamatans', 'kota_kabs', 'provinsis', 'erte', 'erwe', 'keluarahan', 'suket'));
    }

    public function simpelikes(Request $request)
    {
        if ($request->submit == 'searchNik') {
            $searchNik = $request->searchText;
            $warga = warga::where('nik', '=', $request->searchText)->orWhere('nama', '=', $request->searchText)->get();
            $provinsis = Provinsi::where('id', 12)->get();
            $erte = Erte::all();
            $erwe = Erwe::all();
            $bangunan = Bangunan::all();
            $keluarahan = Kelurahan::where('id', 12880)->get();
            $keluarahans = Kelurahan::where('id', 12880)->get();
            $kecamatans = Kecamatan::where('id', 1252)->get();
            $kota_kabs = Kota::where('id', 12880)->get();

            return view ('romantis.profil.simpelikes', compact(  'bangunan',  'keluarahans', 'kecamatans', 'kota_kabs', 'provinsis', 'erte', 'erwe', 'searchNik', 'warga', 'keluarahan'));
        }

        if ($request->submit == 'search') {
            $search = $request->searchText;
            $keluarga = Keluarga::where('no_kk', '=', $request->searchText)->get();
            $provinsis = Provinsi::where('id', 12)->get();
            $erte = Erte::all();
            $erwe = Erwe::all();
            $bangunan = Bangunan::all();
            $keluarahan = Kelurahan::where('id', 12880)->get();
            $keluarahans = Kelurahan::where('id', 12880)->get();
            $kecamatans = Kecamatan::where('id', 1252)->get();
            $kota_kabs = Kota::where('id', 12880)->get();
            return view ('romantis.profil.simpelikes', compact(  'bangunan',  'keluarahans', 'kecamatans', 'kota_kabs', 'provinsis', 'erte', 'erwe', 'search', 'keluarga', 'keluarahan'));
        }

        $provinsis = Provinsi::where('id', 12)->get();
        $erte = Erte::all();
        $erwe = Erwe::all();
        $bangunan = Bangunan::all();
        $keluarahans = Kelurahan::where('id', 12880)->get();
        $keluarahan = Kelurahan::where('id', 12880)->get();
        $kecamatans = Kecamatan::where('id', 1252)->get();
        $kota_kabs = Kota::where('id', 12880)->get();
        return view ('romantis.profil.simpelikes', compact(  'bangunan',  'keluarahans', 'kecamatans', 'kota_kabs', 'provinsis', 'erte', 'erwe', 'keluarahan'));
    }


    //tambah surat keterangan baru
    public function tambahsuketsku(Request $request)
    {
        $warga = warga::find($request->id);
        $suket = Suket::all();

        return view('romantis.suket.tambahsuketsku', compact('warga', 'suket'));
    }
    public function tambahsuketkematian(Request $request)
    {
        $warga = warga::find($request->id);
        $suket = Suket::all();

        return view('romantis.suket.tambahsuketkematianbaru', compact('warga', 'suket'));
    }
    public function tambahsuketkematianbaru(Request $request)
    {
        $warga = warga::find($request->id);
        $suket = Suket::all();

        return view('romantis.suket.tambahsuketkematianbaru', compact('warga', 'suket'));
    }
    public function tambahsuketkelahiran(Request $request)
    {
        $warga = warga::find($request->id);
        $suket = Suket::all();

        return view('romantis.suket.tambahsuketkelahiran', compact('warga', 'suket'));
    }
    public function tambahsuketkelahiranbaru(Request $request)
    {
        $warga = warga::find($request->id);
        $suket = Suket::all();

        return view('romantis.suket.tambahsuketkelahiranbaru', compact('warga', 'suket'));
    }
    public function tambahsuketna(Request $request)
    {
        $warga = warga::find($request->id);
        $suket = Suket::all();

        return view('romantis.suket.tambahsuketna', compact('warga', 'suket'));
    }
    public function tambahsuketserbaguna(Request $request)
    {
        $warga = warga::find($request->id);
        $suket = Suket::all();

        return view('romantis.suket.tambahsuketserbaguna', compact('warga', 'suket'));
    }
    public function tambahsukettidakmampu(Request $request)
    {
        $warga = warga::find($request->id);
        $suket = Suket::all();

        return view('romantis.suket.tambahsukettidakmampu', compact('warga', 'suket'));
    }
    public function tambahsuketorangyangsama(Request $request)
    {
        $warga = warga::find($request->id);
        $suket = Suket::all();

        return view('romantis.suket.tambahsuketorangyangsama', compact('warga', 'suket'));
    }
    public function simpansuket(Request $request)
    {
        $validated = $request->validate([
            'warga_id' => 'required',
            'jenis_suket' => 'required',
            'no_tlp' => 'required',
            'no_reg_rw' => 'required|unique:sukets',
            'tgl_reg_rw' => 'required',
            // 'no_reg_kel' => 'unique:sukets',
            // 'tgl_reg_kel' => 'required',
        ]);

        $suket = Suket::create($request->all());

        return redirect()->route('romantis.ubahsuket', $suket->id);
    }

    public function ubahsuket(Request $request)
    {
        $suket = Suket::find($request->id);
        $agama = Agama::all();


        return view('romantis.profil.ubahsuket', compact('suket',  'agama'));
    }

    public function perbaharuisuket(Request $request, Suket $suket)
    {
        $validated = $request->validate([
            'warga_id' => 'required',
            'jenis_suket' => 'required',
            'no_tlp' => 'required',
            'no_reg_rw' => 'unique:sukets',
        ]);

        DB::beginTransaction();
        try {
            $suket->warga_id = $request->warga_id;
            $suket->jenis_suket = $request->jenis_suket;
            $suket->no_tlp = $request->no_tlp;
            $suket->no_kk_ayah = $request->no_kk_ayah;
            $suket->no_kk_ibu = $request->no_kk_ibu;
            $suket->save();

        } catch (\Exception $ex) {
            DB::rollback();
        }
        // DB::rollBack();
        DB::commit();



        return redirect()->back()->with('success', 'Silahkan diisi form yang masih kosong! Simpan! Silahkan kirim ke berkas RT/RW/Kelurahan!');
    }



    //print suket
    public function printkematian(Request $request){
        //menampilkan halaman laporan
        $suket = Suket::find($request->id);
        $user = User::all();
        $rts = Erte::all();
        $rws = Erwe::all();
        $kelurahan = Kelurahan::all();
        $kecamatan = Kecamatan::all();
        $kotakab = Kota::all();

        return view('romantis.suket.printkematian', compact('kotakab', 'kecamatan', 'suket', 'kelurahan', 'user', 'rts', 'rws'));
    }
    public function printsku(Request $request){
        //menampilkan halaman laporan
        $suket = Suket::find($request->id);
        $user = User::all();
        $rts = Erte::all();
        $rws = Erwe::all();
        $kelurahan = Kelurahan::all();
        $kecamatan = Kecamatan::all();
        $kotakab = Kota::all();

        return view('romantis.suket.printsku', compact('kotakab', 'kecamatan', 'suket', 'kelurahan', 'user', 'rts', 'rws'));
    }
    public function printna(Request $request){
        //menampilkan halaman laporan
        $suket = Suket::find($request->id);
        $user = User::all();
        $rts = Erte::all();
        $rws = Erwe::all();
        $kelurahan = Kelurahan::all();
        $kecamatan = Kecamatan::all();
        $kotakab = Kota::all();

        return view('romantis.suket.printna', compact('kotakab', 'kecamatan', 'suket', 'kelurahan', 'user', 'rts', 'rws'));
    }
    public function printorangyangsama(Request $request){
        //menampilkan halaman laporan
        $suket = Suket::find($request->id);
        $user = User::all();
        $rts = Erte::all();
        $rws = Erwe::all();
        $kelurahan = Kelurahan::all();
        $kecamatan = Kecamatan::all();
        $kotakab = Kota::all();

        return view('romantis.suket.printorangyangsama', compact('kotakab', 'kecamatan', 'suket', 'kelurahan', 'user', 'rts', 'rws'));
    }
    public function printkematianbaru(Request $request){
        //menampilkan halaman laporan
        $suket = Suket::find($request->id);
        $user = User::all();
        $rts = Erte::all();
        $rws = Erwe::all();
        $kelurahan = Kelurahan::all();
        $kecamatan = Kecamatan::all();
        $kotakab = Kota::all();

        return view('romantis.suket.printkematianbaru', compact('kotakab', 'kecamatan', 'suket', 'kelurahan', 'user', 'rts', 'rws'));
    }
    public function printkelahiran(Request $request){
        //menampilkan halaman laporan
        $suket = Suket::find($request->id);
        $user = User::all();
        $rts = Erte::all();
        $rws = Erwe::all();
        $kelurahan = Kelurahan::all();
        $kecamatan = Kecamatan::all();
        $kotakab = Kota::all();

        return view('romantis.suket.printkelahiran', compact('kotakab', 'kecamatan', 'suket', 'kelurahan', 'user', 'rts', 'rws'));
    }
    public function printkelahiranbaru(Request $request){
        //menampilkan halaman laporan
        $suket = Suket::find($request->id);
        $user = User::all();
        $rts = Erte::all();
        $rws = Erwe::all();
        $kelurahan = Kelurahan::all();
        $kecamatan = Kecamatan::all();
        $kotakab = Kota::all();

        return view('romantis.suket.printkelahiranbaru', compact('kotakab', 'kecamatan', 'suket', 'kelurahan', 'user', 'rts', 'rws'));
    }
    public function printtidakmampu(Request $request){
        //menampilkan halaman laporan
        $suket = Suket::find($request->id);
        $user = User::all();
        $rts = Erte::all();
        $rws = Erwe::all();
        $kelurahan = Kelurahan::all();
        $kecamatan = Kecamatan::all();
        $kotakab = Kota::all();

        return view('romantis.suket.printtidakmampu', compact('kotakab', 'kecamatan', 'suket', 'kelurahan', 'user', 'rts', 'rws'));
    }public function printserbaguna(Request $request){
        //menampilkan halaman laporan
        $suket = Suket::find($request->id);
        $user = User::all();
        $rts = Erte::all();
        $rws = Erwe::all();
        $kelurahan = Kelurahan::all();
        $kecamatan = Kecamatan::all();
        $kotakab = Kota::all();

        return view('romantis.suket.printserbaguna', compact('kotakab', 'kecamatan', 'suket', 'kelurahan', 'user', 'rts', 'rws'));
    }

    public function tambahalamat(Request $request)
    {
        $provinsis = Provinsi::where('id', 12)->get();
        $erte = Erte::all();
        $erwe = Erwe::all();
        $bangunan = Bangunan::all();
        $keluarahan = Kelurahan::where('id', 12880)->get();
        $kecamatans = Kecamatan::where('id', 1252)->get();
        $kota_kabs = Kota::where('id', 12880)->get();
        return view('romantis.profil.tambahalamat', compact(['bangunan',  'kecamatans', 'kota_kabs', 'provinsis', 'erte', 'erwe', 'keluarahan']));

    }

    public function simpanalamat(Request $request)
    {
        $validate =$request->validate([
            'alamat_kk_rw' => 'required',
            'alamat_kk_rt' => 'required',
            'nama_alamat' => 'required',
        ]);

        DB::beginTransaction();
        try {
            // save alamat
            $alamat = new Alamat();
            $alamat->kelurahan_id = 12880;
            $alamat->provinsi_id = 12;
            $alamat->kecamatan_id = 1252;
            $alamat->kota_kab_id = 1018;
            $alamat->rw_id = $request->alamat_kk_rw;
            $alamat->rt_id = $request->alamat_kk_rt;
            $alamat->nama_alamat = $request->nama_alamat;

            $alamat->save();

        }catch (\Exception $ex) {
            DB::rollBack();
        }
        DB::commit();

        return redirect()->route('romantis.profil.simpelonline')->with('success', 'Surat Keterangan berhasil dibuat');

    }

    public function tambahkeluarga(Request $request)
    {
        $validated = $request->validate([
            'no_kk' => 'required|unique:keluargas|min:16|max:16',
            'alamat_kk' => 'required',
            // 'alamat_kk_provinsi' => 'required',
            // 'alamat_kk_kota_kab' => 'required',
            // 'alamat_kk_kecamatan' => 'required',
            // 'alamat_kk_kelurahan' => 'required',
            'alamat_kk_rw' => 'required',
            'alamat_kk_rt' => 'required',
        ]);
        // Keluarga::create($request->all());

        DB::beginTransaction();
        try {
            // save keluarga
            $keluarga = new Keluarga();
            $keluarga->no_kk = $request->no_kk;
            $keluarga->alamat_kk_provinsi = 12;
            $keluarga->alamat_kk_kota_kab = 1018;
            $keluarga->alamat_kk_kecamatan = 1252;
            $keluarga->alamat_kk_kelurahan = 12880;
            $keluarga->alamat_kk_rw = $request->alamat_kk_rw;
            $keluarga->alamat_kk_rt = $request->alamat_kk_rt;
            $keluarga->alamat_kk = $request->alamat_kk;
            $keluarga->save();


        } catch (\Exception $ex) {
            DB::rollBack();
        }
        DB::commit();

        return redirect()->back()->with('success', 'No KK anda sudah berhasil ditambahkan');
    }

    public function tambahwarga(Request $request)
    {
        $keluarga = Keluarga::find($request->id);
        $sdhrts = Sdhrt::all();
        $pendidikans = Pendidikan::all();
        $pekerjaans = Pekerjaan::all();
        $agamas = Agama::all();
        $disabilities = Disability::all();
        $provinsis= Provinsi::all();
        $warga = warga::all();

        return view('romantis.profil.tambahwarga', compact(['keluarga', 'sdhrts', 'pendidikans', 'pekerjaans',  'agamas', 'provinsis', 'disabilities']));
    }

    public function simpanwarga(Request $request)
    {
        $validated = $request->validate([
            'nik' => 'unique:wargas|min:16|max:16',
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'kewarganegaraan' => 'required',
            'pendidikan' => 'required',
            'pekerjaan' => 'required',
            'status_perkawinan' => 'required',
            'sdhrt' => 'required',

        ]);


        try {


        if ($request->sdhrt == '1') {
            $cek_kepalakeluarga = warga::where('keluarga_id', $request->keluarga_id)->where('sdhrt', '1')->get();

            if ($cek_kepalakeluarga->isNotEmpty()) {
                return redirect()->back()->withInput()->with('error', 'Kepala Keluarga sudah ada');
            } else {

                $warga = warga::create($request->all());


            }
        } else if ($request->sdhrt == '3') {
            $cek_istri = warga::where('keluarga_id', $request->keluarga_id)->where('sdhrt', '3')->get();

            if ($cek_istri->isNotEmpty()) {
                return redirect()->back()->withInput()->with('error', 'Istri/Suami sudah ada');
            } else {
                $warga = warga::create($request->all());


            }
        } else {


        $warga = warga::create($request->all());


        }

        } catch (\Exception $ex) {
            // DB::rollback();
        }


        // DB::commit();

        return redirect()->back()->with('success', 'Data warga berhasil ditambahkan');
    }

    public function persyaratan(Request $request)
    {

        return view ('romantis.profil.persyaratan');
    }
    public function skm(Request $request)
    {

        return view ('romantis.profil.skm');
    }

    public function searchLokasi(Request $request)
    {
        if ($request->ajax()) {
            if ($request->search == 'provinsi') {
                $kota_kab = Kota::where('provinsi_id', $request->provinsi_id)->get();
                return $kota_kab;
            }

            if ($request->search == 'kota_kab') {
                $kecamatan = Kecamatan::where('kota_kab_id', $request->kota_kab_id)->get();
                return $kecamatan;
            }

            if ($request->search == 'kecamatan') {
                $keluarahan = Kelurahan::where('Kecamatan_id', $request->kecamatan_id)->get();
                return $keluarahan;
            }

            if ($request->search == 'kelurahan') {
                $rw = Erwe::where('Kelurahan_id', $request->kelurahan_id)->get();
                return $rw;
            }

            if ($request->search == 'rw') {
                $rt = Erte::where('rw_id', $request->rw_id)->get();
                return $rt;
            }

            if ($request->search == 'rt') {
                $alamat = Alamat::where('rt_id', $request->rt_id)->get();
                return $alamat;
            }
        }
    }


    public function vaksin (Request $request)
    {
        if ($request->submit == 'searchNik') {
            $warga = warga::where('nik', '=', $request->searchText)->get();
            if($warga->count() > 0) {
                $warganya = warga::all();
                $vaksin = Vaksin::all();

                return view('romantis.vaksin', compact('warga', 'vaksin'));
            }else {
                return back()->with('error', 'NIK anda tidak terdaftar, Silahkan Hubungi RT setempat supaya dapat didaftarkan ke database kami');
            }
        }

        return view('romantis.vaksin');
    }


    public function updatevaksin(Request $request)
    {
        $warga = warga::find($request->id);
        $warga->relasiWargaKeVaksin()->sync($request->vaksin);

        // return view('romantis.vaksin')->with('success', 'Data Vaksin telah berhasil di update!');
        return redirect()->route('insertvaksin')->with('success', 'Data Vaksin telah berhasil di update!');

    }

    public function odf (Request $request)
    {
        if ($request->submit == 'searchNik') {
            $warga = warga::where('nik', '=', $request->searchText)->get();
            if($warga->count() > 0) {
                $warganya = warga::all();
                $alamat = Alamat::where('kecamatan_id', '=', '1018');

                return view('romantis.odf', compact('warga', 'alamat'));
            }else {
                return back()->with('error', 'NIK anda tidak terdaftar, Silahkan Hubungi RT setempat supaya dapat didaftarkan ke database kami');
            }
        }

        return view('romantis.odf');
    }

    public function updateodf(Request $request, Alamat $alamat)
    {
        $alamat = Alamat::find($request->id);
        $alamat->kepemilikan_septictank = $request->kepemilikan_septictank;
        $alamat->save();

        return redirect()->route('insertodf')->with('success', 'Data Rumah Tinggal Anda telah berhasil di update, Terima Kasih Telah Berpartisipasi Dalam Pendataan Ini');

    }




}
